import Reactotron from 'reactotron-react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {reactotronRedux} from 'reactotron-redux';

const reactotronConfig = Reactotron.setAsyncStorageHandler(AsyncStorage)
  .configure({
    name: 'BabyCrib',
    host: '192.168.1.53',
  })
  .useReactNative()
  .use(reactotronRedux())
  .connect();

export default reactotronConfig;
