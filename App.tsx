import AsyncStorage from '@react-native-async-storage/async-storage';
import React, {Suspense, useEffect} from 'react';
import {DevSettings, View} from 'react-native';
import OneSignal from 'react-native-onesignal';
import Toast from 'react-native-toast-message';
import {Provider} from 'react-redux';
import reactotron from 'reactotron-react-native';
import {PersistGate} from 'redux-persist/integration/react';
import TabNavigator from './src/navigations/index';
import {toastConfig} from './src/utils/toast-config';
import store, {persistor} from '/redux/configStore';

const App = () => {
  const addMenuClearAsyncStorage = () => {
    if (__DEV__) {
      DevSettings.addMenuItem('Clear AsyncStorage', () => {
        AsyncStorage.clear();
        DevSettings.reload();
      });
    }
  };
  useEffect(() => {
    OneSignal.setLogLevel(6, 0);
    OneSignal.setAppId('b3b0af9f-acce-4783-b769-c98bf8776ae7');
    OneSignal.setNotificationOpenedHandler(notification => {
      reactotron.log!('OneSignal: notification opened:', notification);
    });
    addMenuClearAsyncStorage();
  }, []);

  return (
    <Provider store={store}>
      <PersistGate persistor={persistor} loading={null}>
        <Suspense fallback={<View />} />
        <TabNavigator />
        <Toast config={toastConfig} position="bottom" bottomOffset={30} />
      </PersistGate>
    </Provider>
  );
};

export default App;
