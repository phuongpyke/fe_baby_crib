import {number} from 'prop-types';
import {EGender} from '/utils/enum';

export interface TResponse {
  success: boolean;
  message: string;
}

export interface IBabyInfo {
  background_disease: any;
  breast_milk: number;
  created_at: string | Date;
  disease: any;
  dob: any;
  gender: EGender;
  height: number;
  id: string | number;
  month_by_week: any;
  name: string;
  updated_at: string | Date;
  user_id: string | number;
  weight: number;
}
