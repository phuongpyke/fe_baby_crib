import reactotron from 'reactotron-react-native';
/* eslint-disable prefer-promise-reject-errors */
import _ from 'lodash';
import APISauce from 'apisauce';
import Toast from 'react-native-toast-message';
import {TIME_OUT_REQUEST_API, STATUS_HTTPS} from '../config';
import store from '/redux/configStore';

let HEADERS: any = {
  // 'Accept': "application/json",
  // "Cache-Control": "no-cache",
  'Content-Type': 'application/json',
};

let HEADERS_MULTIPLE_PART: any = {
  Accept: 'application/json',
  // "Cache-Control": "no-cache",
  'Content-Type': 'multipart/form-data',
};

const setToken = (_token: string) => {
  if (_token) {
    HEADERS = {
      ...HEADERS,
      Authorization: `Bearer ${_token}`,
    };
    HEADERS_MULTIPLE_PART = {
      ...HEADERS_MULTIPLE_PART,
      Authorization: `Bearer ${_token}`,
    };
  }
};
const getBaseURLWithMode = 'https://babyai.lamwebnhanh.net/api/v2';

export const apiGlobal = APISauce.create({
  timeout: TIME_OUT_REQUEST_API,
  headers: HEADERS,
  baseURL: getBaseURLWithMode,
});

apiGlobal.addAsyncRequestTransform(async (config: any) => {
  // Do something before API is sent
  const token = store.getState()?.auth?.token;
  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }
});

const throttledResetToLogin = async (response: any) => {
  Toast.show({
    type: 'error',
    text1: 'Thất bại',
    text2: `${response?.data?.message}`,
  });
};

const showToastError = () => {
  Toast.show({
    type: 'error',
    props: {
      message: '',
      onClose: () => Toast.hide(),
    },
  });
};

const handlingResponse = (response: any) =>
  new Promise((resolve, reject) => {
    // if (response.status === STATUS_HTTPS.UNAUTHENTICATED) {
    //     return throttledResetToLogin(response);
    // }
    if (
      response.status >= STATUS_HTTPS.GREATER_RANGE_SUCCESS &&
      response.status < STATUS_HTTPS.SMALLER_RANGE_SUCCESS
    ) {
      return resolve(response.data);
    }
    switch (response.problem) {
      // case TIMEOUT_ERROR:
      //     reject({});
      //     return showToastError();
      // case NETWORK_ERROR:
      //     reject({});
      //     return showToastError();
      default: {
        const error = {
          errorMessage: response.data.message,
        };
        return reject(error);
      }
    }
  });

const baseApi = {
  post: async (endpoint: string, params: any) => {
    const baseURL = getBaseURLWithMode;
    // console.log('*****POST', `${baseURL}${endpoint}`, params);
    apiGlobal.setBaseURL(baseURL);
    apiGlobal.setHeaders(HEADERS);
    return apiGlobal
      .post(endpoint, params)
      .then(response => handlingResponse(response));
  },
  get: async (endpoint: string, params?: any) => {
    const baseURL = getBaseURLWithMode;
    // console.log('*****GET', `${baseURL}${endpoint}`, params);
    apiGlobal.setBaseURL(baseURL);
    apiGlobal.setHeaders(HEADERS);
    return apiGlobal
      .get(endpoint, params)
      .then(response => handlingResponse(response));
  },
  put: (endpoint: string, params: any) => {
    const baseURL = getBaseURLWithMode;
    // console.log('*****PUT', `${baseURL}${endpoint}`, params);
    apiGlobal.setBaseURL(baseURL);
    apiGlobal.setHeaders(HEADERS);
    return apiGlobal
      .put(endpoint, params)
      .then(response => handlingResponse(response));
  },
  postFormData: async (endpoint: string, params: any) => {
    const baseURL = getBaseURLWithMode;
    // console.log('******POST_FORM_DATA', `${baseURL}${endpoint}`, params);
    apiGlobal.setBaseURL(baseURL);
    apiGlobal.setHeaders(HEADERS_MULTIPLE_PART);
    const formData = new FormData();
    _.forIn(params, (value, key) => {
      if (value) {
        formData.append(key, value);
      }
    });
    return apiGlobal
      .post(endpoint, formData)
      .then(response => handlingResponse(response));
  },
  delete: async (endpoint: string, params: any) => {
    const baseURL = getBaseURLWithMode;
    // console.log('******DELETE', `${baseURL}${endpoint}`, params);
    apiGlobal.setBaseURL(baseURL);
    apiGlobal.setHeaders(HEADERS);
    return apiGlobal
      .delete(endpoint, {}, {data: params})
      .then(response => handlingResponse(response));
  },
};

export {baseApi, setToken, getBaseURLWithMode};

export function loginAppAPI({phone, password}: any) {
  return baseApi.post('/auth/login', {
    phone,
    password,
  });
}

export function getBabyListAPI() {
  return baseApi.get('/babies');
}

export function getMeAPI() {
  return baseApi.get('/auth/me');
}

export function forgotPasswordAPI({phone, new_password}: any) {
  return baseApi.post('change_password', {
    phone,
    new_password,
  });
}

export function registerAPI({phone, password}: any) {
  return baseApi.post('/auth/signup', {
    phone,
    password,
    password_confirmation: password,
  });
}

export function updatePasswordAPI({
  oldPassword,
  newPassword,
  reNewPassword,
}: any) {
  return baseApi.post(`/changepass`, {
    oldPassword,
    newPassword,
    reNewPassword,
  });
}

export function createBaByAPI({
  name,
  dob,
  weight,
  height,
  breast_milk,
  gender,
  background_disease,
}: any) {
  return baseApi.post(`/babies`, {
    name,
    dob,
    weight: parseFloat(weight),
    height: parseFloat(height),
    breast_milk,
    gender,
    background_disease,
  });
}

export function updateBaByAPI({
  name,
  dob,
  weight,
  height,
  breast_milk,
  gender,
  background_disease,
  id,
}: any) {
  return baseApi.post(`/babies/${id}`, {
    name,
    dob,
    weight: parseFloat(weight),
    height: parseFloat(height),
    breast_milk,
    gender,
    background_disease,
  });
}

// Schedule
export function getScheduleTodayWithBaby({babyId}: {babyId: number}) {
  return baseApi.get(`/schedule/today/${babyId}`);
}
