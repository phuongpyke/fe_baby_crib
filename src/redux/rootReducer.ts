import authReducer from '/modules/auth/slice';
import userInfoReducer from '/modules/user/slice';
import scheduleReducer from '/modules/schedule/slice';

const rootReducer = {
  auth: authReducer,
  user: userInfoReducer,
  schedule: scheduleReducer,
};

export default rootReducer;
