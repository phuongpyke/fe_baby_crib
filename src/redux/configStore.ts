import {configureStore} from '@reduxjs/toolkit';
import logger from 'redux-logger';
import {persistStore} from 'redux-persist';
import createSagaMiddleware from 'redux-saga';
import reactotronConfig from '../../ReactotronConfig';
import rootReducer from './rootReducer';
import rootSaga from './rootSaga';

const sagaMiddleware = createSagaMiddleware();

const store = configureStore({
  reducer: rootReducer,
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({
      immutableCheck: false,
      serializableCheck: false,
    })
      .prepend(sagaMiddleware)
      .concat(logger),
  devTools: __DEV__,
  enhancers: [reactotronConfig.createEnhancer!()],
});

sagaMiddleware.run(rootSaga);

export const persistor = persistStore(store);

export default store;
