import {all} from 'redux-saga/effects';
import authSaga from '../modules/auth/saga';
import userSaga from '/modules/user/saga';

function* rootSaga() {
  yield all([authSaga(), userSaga()]);
}
export default rootSaga;
