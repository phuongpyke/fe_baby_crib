import React from 'react';
import {Switch} from 'react-native-switch';
import {Color} from '../../utils';
import {ColorsDefault} from '/assets';

const CustomSwitch = props => {
  return (
    <Switch
      value={props.value}
      onValueChange={props.onValueChange}
      disabled={false}
      activeText="|"
      inActiveText=""
      circleBorderWidth={0}
      circleSize={24}
      barHeight={28}
      backgroundActive={ColorsDefault.blue1}
      backgroundInactive={'gray'}
      circleActiveColor={ColorsDefault.white}
      circleInActiveColor={ColorsDefault.white}
      changeValueImmediately={true}
      switchRightPx={6}
      switchLeftPx={6}
    />
  );
};

export default CustomSwitch;
