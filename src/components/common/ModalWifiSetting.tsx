import React, {
  forwardRef,
  useEffect,
  useImperativeHandle,
  useState,
} from 'react';
import {
  Keyboard,
  Text,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import {s, ScaledSheet} from 'react-native-size-matters';
import {ColorsDefault, FontFamilyDefault, FontSizeDefault} from '/assets';
import {FULL_WIDTH_DEVICE} from '/common';
import Modal from 'react-native-modal';

interface Props {
  onSubmit: (name: string, pass: string) => void;
}

const ModalWifiSetting = (props: Props, ref: any) => {
  const {onSubmit} = props;

  const [isVisible, setIsVisible] = useState<boolean>(false);
  const [nameWifi, setNameWifi] = useState<any>({});
  const [valueInput, setValueInput] = useState<string>('');
  const [disable, setDisable] = useState<boolean>(true);

  useEffect(() => {
    setDisable(valueInput.length ? false : true);
  }, [valueInput]);

  useImperativeHandle(ref, () => ({
    show: ({data}: any) => {
      setNameWifi(data);
      setIsVisible(true);
    },
    hide: () => {
      setIsVisible(false);
    },
  }));

  const handleSubmit = () => {
    setIsVisible(false);
    onSubmit?.(nameWifi, valueInput);
  };

  const onBackdropPress = () => {
    setIsVisible(!isVisible);
  };

  const onChangeText = (text: string) => {
    setValueInput(text);
  };

  return (
    <Modal
      isVisible={isVisible}
      coverScreen={true}
      onBackdropPress={onBackdropPress}>
      <TouchableWithoutFeedback
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={styles.childStyle}>
          <Text style={styles.textTitle}>{nameWifi}</Text>
          <Text style={styles.textSubTitle}>{'Nhập mật khẩu'}</Text>
          <TextInput
            value={valueInput}
            style={styles.input}
            underlineColorAndroid={'transparent'}
            autoComplete={'off'}
            textContentType={'none'}
            importantForAutofill="yes"
            autoCorrect={false}
            secureTextEntry={true}
            returnKeyType={'done'}
            onChangeText={onChangeText}
          />
          <TouchableOpacity
            disabled={disable}
            style={[styles.btnConfirm, disable && styles.btnDisable]}
            activeOpacity={0.8}
            onPress={handleSubmit}>
            <Text style={styles.textConfirm}>{'Xác nhận'}</Text>
          </TouchableOpacity>
        </View>
      </TouchableWithoutFeedback>
    </Modal>
  );
};

const styles = ScaledSheet.create({
  childStyle: {
    borderRadius: 16,
    backgroundColor: ColorsDefault.white,
    width: FULL_WIDTH_DEVICE - s(32),
    alignItems: 'center',
    paddingVertical: '24@s',
    paddingHorizontal: '32@s',
  },
  textTitle: {
    fontSize: FontSizeDefault.FONT_18,
    fontFamily: FontFamilyDefault.primarySemiBold,
    color: ColorsDefault.black3,
  },
  textSubTitle: {
    fontSize: FontSizeDefault.FONT_16,
    fontFamily: FontFamilyDefault.primaryMedium,
    color: ColorsDefault.gray3,
    marginTop: '5@s',
  },
  btnConfirm: {
    height: '48@s',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: '48@s',
    backgroundColor: ColorsDefault.blue1,
    width: FULL_WIDTH_DEVICE - s(160),
    marginTop: '16@s',
  },
  btnDisable: {
    backgroundColor: ColorsDefault.gray2,
  },
  textConfirm: {
    fontSize: FontSizeDefault.FONT_18,
    fontFamily: FontFamilyDefault.primaryMedium,
    color: ColorsDefault.white,
  },
  input: {
    height: '48@s',
    paddingVertical: 0,
    paddingHorizontal: '10@s',
    width: FULL_WIDTH_DEVICE - s(128),
    borderRadius: 10,
    borderColor: ColorsDefault.gray1,
    borderWidth: 1,
    fontSize: FontSizeDefault.FONT_16,
    fontFamily: FontFamilyDefault.primaryMedium,
    color: ColorsDefault.black3,
    marginTop: '16@s',
  },
  textMl: {
    fontSize: FontSizeDefault.FONT_16,
    fontFamily: FontFamilyDefault.primaryMedium,
    color: ColorsDefault.black3,
    marginLeft: '8@s',
  },
});

export default forwardRef(ModalWifiSetting);
