import React, {useEffect, useState} from 'react';
import isEqual from 'react-fast-compare';
import {StyleProp, TouchableOpacity, View, ViewStyle} from 'react-native';
import Animated, {
  useAnimatedStyle,
  useSharedValue,
  withSpring,
} from 'react-native-reanimated';
import {s, scale, ScaledSheet} from 'react-native-size-matters';
import {ColorsDefault} from '/assets';

const SIZE_DEFAULT = {
  trackBarHeight: scale(24),
  trackBarRadius: scale(12),
  trackBarWidth: scale(38),
  thumbBtnHeight: scale(20),
  thumbBtnRadius: scale(10),
  thumbBtnWidth: scale(20),
};

interface ToggleSwitchProps {
  containerStyle?: StyleProp<ViewStyle>;
  value: boolean;
  onPress?(value: boolean): void;
}

const ToggleSwitch = (props: ToggleSwitchProps) => {
  const {value, containerStyle, onPress} = props;

  const [toggleValue, setToggleValue] = useState<boolean>(false);
  const translateX = useSharedValue(0);

  useEffect(() => {
    if (value) {
      setToggleValue(value);
    }
  }, [value]);

  useEffect(() => {
    const {thumbBtnWidth, trackBarWidth} = SIZE_DEFAULT;
    const distance = trackBarWidth - thumbBtnWidth - s(4);
    const toValue = toggleValue ? distance : 0;
    translateX.value = withSpring(toValue, {
      overshootClamping: true,
    });
  }, [toggleValue]);

  const onToggle = () => {
    const isToggle = !toggleValue;
    setToggleValue(isToggle);
    onPress?.(isToggle);
  };

  const animatedCircleStyle = useAnimatedStyle(() => {
    return {
      transform: [
        {
          translateX: translateX.value,
        },
      ],
    };
  });

  return (
    <View style={[styles.container, containerStyle]}>
      <TouchableOpacity
        activeOpacity={1}
        onPress={onToggle}
        onLongPress={onToggle}>
        <Animated.View
          style={[
            styles.bar,
            {
              backgroundColor: toggleValue ? ColorsDefault.blue1 : '#E3E3E3',
            },
          ]}>
          <Animated.View style={[styles.button, animatedCircleStyle]} />
          <View style={styles.line}></View>
        </Animated.View>
      </TouchableOpacity>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  bar: {
    width: SIZE_DEFAULT.trackBarWidth,
    height: SIZE_DEFAULT.trackBarHeight,
    borderRadius: SIZE_DEFAULT.trackBarRadius,
    justifyContent: 'center',
  },
  button: {
    backgroundColor: ColorsDefault.white,
    width: SIZE_DEFAULT.thumbBtnWidth,
    height: SIZE_DEFAULT.thumbBtnHeight,
    borderRadius: SIZE_DEFAULT.thumbBtnRadius,
    zIndex: -1,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    right: s(16),
  },
  line: {
    height: s(10),
    width: s(2),
    backgroundColor: ColorsDefault.white,
    borderRadius: 2,
    position: 'absolute',
    left: s(8),
  },
});

export default React.memo(ToggleSwitch, isEqual);
