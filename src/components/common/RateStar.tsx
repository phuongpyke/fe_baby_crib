import React, {useState} from 'react';
import {
  Image,
  StyleProp,
  TouchableOpacity,
  View,
  ViewStyle,
} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {ColorsDefault} from '/assets';
import images from '/assets/images';

interface Props {
  customStyle?: StyleProp<ViewStyle>;
  onPress?: (value: number) => void;
  defaultValue?: number;
}

const RateStar = (props: Props) => {
  const {customStyle, onPress, defaultValue = -1} = props;
  const [value, setValue] = useState<number>(defaultValue);

  const onStarChange = (index: number) => {
    setValue(index);
    onPress?.(index);
  };

  return (
    <View style={[styles.container, customStyle]}>
      {Array(5)
        .fill(null)
        .map((_, i) => (
          <TouchableOpacity
            key={i}
            style={styles.btn}
            onPress={() => onStarChange(i)}>
            <Image
              source={images.rateStar}
              style={[
                styles.icon,
                i <= value && {tintColor: ColorsDefault.yellow3},
              ]}
            />
          </TouchableOpacity>
        ))}
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  btn: {
    marginLeft: '4@s',
  },
  icon: {
    height: '24@s',
    width: '24@s',
  },
});

export default RateStar;
