import ActionSheet from '@alessiocancian/react-native-actionsheet';
import React, {forwardRef} from 'react';
import {FontFamilyDefault, FontSizeDefault} from '/assets';

interface Props {
  onSelect?(index: number): void;
  data?: string[];
  title?: string;
}

const ModalActionSheet = (props: Props, ref: any) => {
  const {onSelect, data, title} = props;

  const options = ['Hủy', 'Chọn ảnh', 'Chụp ảnh'];

  return (
    <ActionSheet
      ref={ref}
      options={data || options}
      cancelButtonIndex={0}
      onPress={index => onSelect?.(index)}
      theme="ios"
      userInterfaceStyle="light"
      title={title}
      styles={{
        buttonText: {
          fontFamily: FontFamilyDefault.primaryRegular,
          fontSize: FontSizeDefault.FONT_16,
        },
      }}
    />
  );
};

export default forwardRef(ModalActionSheet);
