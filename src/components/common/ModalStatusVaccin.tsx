import React, {forwardRef, useImperativeHandle, useState} from 'react';
import {Image, Text, TextInput, TouchableOpacity, View} from 'react-native';
import {s, ScaledSheet, vs} from 'react-native-size-matters';
import {ColorsDefault, FontFamilyDefault, FontSizeDefault} from '/assets';
import {FULL_WIDTH_DEVICE} from '/common';
import Modal from 'react-native-modal';
import StaticSafeAreaInsets from 'react-native-static-safe-area-insets';
import images from '/assets/images';

interface Props {
  options: any[];
  onChangeValue: (value: any) => void;
}

const ModalStatusVaccin = (props: Props, ref: any) => {
  const {options = [], onChangeValue} = props;

  const [isVisible, setIsVisible] = useState<boolean>(false);

  useImperativeHandle(ref, () => ({
    show: () => {
      setIsVisible(true);
    },
    hide: () => {
      setIsVisible(false);
    },
  }));

  const onSubmit = (item: any) => {
    setIsVisible(false);
    onChangeValue?.(item);
  };

  const onBackdropPress = () => {
    setIsVisible(!isVisible);
  };

  return (
    <Modal
      isVisible={isVisible}
      style={styles.modal}
      coverScreen={true}
      onBackdropPress={onBackdropPress}>
      <View style={styles.childStyle}>
        <Text style={styles.textTitle}>{'Trạng thái'}</Text>
        <TouchableOpacity style={styles.btnClose} onPress={onBackdropPress}>
          <Image source={images.close} style={styles.icClose} />
        </TouchableOpacity>
        {options.map((item, index) => (
          <TouchableOpacity
            key={index}
            style={styles.btn}
            activeOpacity={0.8}
            onPress={() => onSubmit(item)}>
            <Image
              source={item.icon}
              style={[styles.icItem, {tintColor: item.color}]}
            />
            <Text style={[styles.textBtn, {color: item.color}]}>
              {item.label}
            </Text>
          </TouchableOpacity>
        ))}
      </View>
    </Modal>
  );
};

const styles = ScaledSheet.create({
  modal: {
    margin: 0,
    justifyContent: 'flex-end',
  },
  childStyle: {
    borderRadius: 16,
    backgroundColor: ColorsDefault.white,
    width: FULL_WIDTH_DEVICE,
    paddingTop: '16@vs',
    paddingBottom: StaticSafeAreaInsets.safeAreaInsetsBottom + vs(32),
    paddingHorizontal: '32@s',
  },
  textTitle: {
    fontSize: FontSizeDefault.FONT_20,
    fontFamily: FontFamilyDefault.primaryMedium,
    color: ColorsDefault.black3,
    textAlign: 'center',
  },
  btn: {
    backgroundColor: ColorsDefault.white,
    marginTop: '16@vs',
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: '10@vs',
  },
  textBtn: {
    fontSize: FontSizeDefault.FONT_16,
    fontFamily: FontFamilyDefault.primaryRegular,
    marginLeft: '6@s',
  },
  icItem: {
    height: '24@s',
    width: '24@s',
  },
  icClose: {
    height: '24@s',
    width: '24@s',
  },
  btnClose: {
    position: 'absolute',
    right: '16@s',
    top: '16@vs',
  },
});

export default forwardRef(ModalStatusVaccin);
