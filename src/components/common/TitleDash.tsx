import React from 'react';
import {StyleProp, Text, View, ViewStyle} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {ColorsDefault, FontFamilyDefault, FontSizeDefault} from '/assets';

interface Props {
  customStyle?: StyleProp<ViewStyle>;
  title: string;
}

const TitleDash = (props: Props) => {
  const {customStyle, title} = props;

  return (
    <View style={[styles.container, customStyle]}>
      <Text style={styles.textTitle}>{title}</Text>
      <View style={styles.dash} />
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    alignSelf: 'center',
    alignItems: 'center',
  },
  textTitle: {
    color: ColorsDefault.orange2,
    fontSize: FontSizeDefault.FONT_20,
    fontFamily: FontFamilyDefault.primaryBold,
  },
  dash: {
    height: 0,
    borderColor: ColorsDefault.orange2,
    borderWidth: 1,
    borderRadius: 1,
    borderStyle: 'dashed',
    width: '120@s',
  },
});

export default TitleDash;
