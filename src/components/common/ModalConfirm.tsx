import React, {forwardRef, useImperativeHandle, useState} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import Modal from 'react-native-modal';
import {s, ScaledSheet} from 'react-native-size-matters';
import {ColorsDefault, FontFamilyDefault, FontSizeDefault} from '/assets';
import {FULL_WIDTH_DEVICE} from '/common';

interface Props {
  title: string;
  subTitle?: string;
  textConfirm?: string;
  textCancel?: string;
  onCancel?: () => void;
  onSubmit?: () => void;
}

const ModalConfirm = (props: Props, ref: any) => {
  const {title, subTitle, textConfirm, textCancel, onCancel, onSubmit} = props;

  const [isVisible, setIsVisible] = useState<boolean>(false);

  useImperativeHandle(ref, () => ({
    show: () => {
      setIsVisible(true);
    },
    hide: () => {
      setIsVisible(false);
    },
  }));

  const handleSubmit = () => {
    setIsVisible(false);
    onSubmit?.();
  };

  const handleCancel = () => {
    setIsVisible(false);
    onCancel?.();
  };

  const onBackdropPress = () => {
    setIsVisible(!isVisible);
  };

  return (
    <Modal
      isVisible={isVisible}
      coverScreen={true}
      onBackdropPress={onBackdropPress}>
      <View style={styles.childStyle}>
        <Text style={styles.textTitle}>{title}</Text>
        {!!subTitle && <Text style={styles.textSubTitle}>{subTitle}</Text>}
        <View style={styles.wrapButton}>
          <TouchableOpacity
            style={styles.btn}
            activeOpacity={0.8}
            onPress={handleSubmit}>
            <Text style={styles.textBtn}>{textConfirm || 'Xác nhận'}</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.btn}
            activeOpacity={0.8}
            onPress={handleCancel}>
            <Text style={styles.textBtn}>{textCancel || 'Hủy'}</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

const styles = ScaledSheet.create({
  childStyle: {
    borderRadius: 16,
    backgroundColor: ColorsDefault.white,
    width: FULL_WIDTH_DEVICE - s(32),
    alignItems: 'center',
    paddingTop: '24@s',
    paddingBottom: '48@s',
    paddingHorizontal: '32@s',
  },
  textTitle: {
    fontSize: FontSizeDefault.FONT_16,
    fontFamily: FontFamilyDefault.primaryMedium,
    color: ColorsDefault.black3,
  },
  textSubTitle: {
    fontSize: FontSizeDefault.FONT_14,
    fontFamily: FontFamilyDefault.primaryRegular,
    color: ColorsDefault.gray3,
    marginTop: '5@s',
  },
  wrapButton: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
  },
  btn: {
    height: '52@s',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: '52@s',
    backgroundColor: ColorsDefault.gray1,
    width: (FULL_WIDTH_DEVICE - s(116)) / 2,
    marginTop: '42@s',
  },
  textBtn: {
    fontSize: FontSizeDefault.FONT_16,
    fontFamily: FontFamilyDefault.primaryRegular,
    color: ColorsDefault.black3,
  },
});

export default forwardRef(ModalConfirm);
