import React, {useCallback, useState} from 'react';
import {StyleProp, View, ViewStyle} from 'react-native';
import {s, ScaledSheet} from 'react-native-size-matters';
import YoutubePlayer from 'react-native-youtube-iframe';
import {Text} from '../base';
import {ColorsDefault} from '/assets';
import {WIDTH_DEVICE} from '/common';
interface Props {
  customStyle?: StyleProp<ViewStyle>;
  title: string;
  videoId: string;
}

const WIDTH_PLAYER = WIDTH_DEVICE - s(32);
const HEIGHT_PLAYER = (WIDTH_PLAYER * 3) / 4;

const WatchVideo = (props: Props) => {
  const {customStyle, title, videoId} = props;

  const [playing, setPlaying] = useState<boolean>(false);

  const onStateChange = useCallback((state: string) => {
    if (state === 'ended') {
      setPlaying(false);
    }
  }, []);

  return (
    <View style={[styles.container, customStyle]}>
      <YoutubePlayer
        height={HEIGHT_PLAYER}
        width={WIDTH_PLAYER}
        play={playing}
        videoId={videoId}
        onChangeState={onStateChange}
      />
      <Text
        type="h4"
        fontWeight={'medium'}
        color={ColorsDefault.black3}
        mt={-(HEIGHT_PLAYER / 5)}>
        {title}
      </Text>
      <View style={styles.viewTime}>
        <Text type="h4" fontWeight={'medium'} color={ColorsDefault.white}>
          06:15
        </Text>
      </View>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    marginTop: '20@vs',
    marginHorizontal: '16@s',
  },
  viewPlayer: {
    overflow: 'hidden',
    alignSelf: 'center',
  },
  viewTime: {
    borderRadius: 6,
    backgroundColor: ColorsDefault.black3,
    height: '26@vs',
    paddingHorizontal: '20@s',
    opacity: 0.25,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    right: '12@s',
    top: '12@s',
  },
});

export default WatchVideo;
