import React, {useCallback, useState} from 'react';
import {
  StyleProp,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ViewStyle,
} from 'react-native';
import {ColorsDefault, FontFamilyDefault} from '/assets';
import {hitSlop} from '/utils/helper';

interface Props {
  containerStyle?: StyleProp<ViewStyle>;
  content: string;
  defaultShow?: boolean;
  defaultNumberOfLines?: number;
  onPress?(): void;
}

const TextSeeMore = (props: Props) => {
  const {content, defaultNumberOfLines = 5, containerStyle, onPress} = props;
  const [showMore, setShowMore] = useState<boolean>(false);
  const [numOfLines, setNumOfLines] = useState<number>(0);

  const onTextLayout = useCallback((event: any) => {
    if (numOfLines === 0) {
      setNumOfLines(event.nativeEvent.lines.length);
    }
  }, []);

  const onToggle = () => {
    setShowMore(!showMore);
    setNumOfLines(0);
  };

  const getNumberOfLines = useCallback(() => {
    if (showMore) {
      return numOfLines;
    }
    return defaultNumberOfLines;
  }, [numOfLines, defaultNumberOfLines, showMore]);

  return (
    <View style={containerStyle}>
      {content ? (
        <TouchableOpacity onPress={onPress} disabled={!onPress}>
          <Text
            style={styles.content}
            numberOfLines={getNumberOfLines()}
            onTextLayout={onTextLayout}>
            {content}
          </Text>
        </TouchableOpacity>
      ) : (
        <View />
      )}
      {numOfLines >= defaultNumberOfLines && (
        <TouchableOpacity
          hitSlop={hitSlop(10)}
          style={styles.btnSee}
          onPress={onToggle}>
          <Text style={styles.textSee}>{showMore ? 'Ẩn bớt' : 'Xem thêm'}</Text>
        </TouchableOpacity>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  content: {
    fontSize: 14,
    lineHeight: 24,
    color: ColorsDefault.black3,
    fontFamily: FontFamilyDefault.primaryRegular,
  },
  textSee: {
    fontSize: 14,
    color: ColorsDefault.blue1,
    fontFamily: FontFamilyDefault.primaryBold,
  },
  btnSee: {},
});

export default TextSeeMore;
