import React, {forwardRef, useImperativeHandle, useState} from 'react';
import {Text, TextInput, TouchableOpacity, View} from 'react-native';
import {s, ScaledSheet} from 'react-native-size-matters';
import {ColorsDefault, FontFamilyDefault, FontSizeDefault} from '/assets';
import {FULL_WIDTH_DEVICE} from '/common';
import Modal from 'react-native-modal';

interface Props {}

const ModalAmountMilk = (props: Props, ref: any) => {
  const {} = props;

  const [isVisible, setIsVisible] = useState<boolean>(false);

  useImperativeHandle(ref, () => ({
    show: () => {
      setIsVisible(true);
    },
    hide: () => {
      setIsVisible(false);
    },
  }));

  const onSubmit = () => {
    setIsVisible(false);
  };

  const onBackdropPress = () => {
    setIsVisible(!isVisible);
  };

  return (
    <Modal
      isVisible={isVisible}
      coverScreen={true}
      onBackdropPress={onBackdropPress}>
      <View style={styles.childStyle}>
        <Text style={styles.textTitle}>{'Bé đã uống bao nhiêu ml sữa?'}</Text>
        <Text style={styles.textSubTitle}>
          {'Tiêu chuẩn cho bé: 120 - 180ml'}
        </Text>
        <View style={styles.wrapInput}>
          <TextInput
            style={styles.input}
            textAlign={'center'}
            keyboardType={'decimal-pad'}
            maxLength={4}
          />
          <Text style={styles.textMl}>{'ml'}</Text>
        </View>
        <TouchableOpacity
          style={styles.btnConfirm}
          activeOpacity={0.8}
          onPress={onSubmit}>
          <Text style={styles.textConfirm}>{'Xác nhận'}</Text>
        </TouchableOpacity>
      </View>
    </Modal>
  );
};

const styles = ScaledSheet.create({
  childStyle: {
    borderRadius: 16,
    backgroundColor: ColorsDefault.white,
    width: FULL_WIDTH_DEVICE - s(32),
    alignItems: 'center',
    paddingVertical: '24@s',
    paddingHorizontal: '32@s',
  },
  textTitle: {
    fontSize: FontSizeDefault.FONT_16,
    fontFamily: FontFamilyDefault.primaryMedium,
    color: ColorsDefault.black3,
  },
  textSubTitle: {
    fontSize: FontSizeDefault.FONT_14,
    fontFamily: FontFamilyDefault.primaryRegular,
    color: ColorsDefault.gray3,
    marginTop: '5@s',
  },
  wrapInput: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: '24@s',
  },
  btnConfirm: {
    height: '52@s',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: '52@s',
    backgroundColor: ColorsDefault.blue1,
    width: FULL_WIDTH_DEVICE - s(128),
    marginTop: '42@s',
  },
  textConfirm: {
    fontSize: FontSizeDefault.FONT_18,
    fontFamily: FontFamilyDefault.primaryMedium,
    color: ColorsDefault.white,
  },
  input: {
    height: '64@s',
    width: '64@s',
    borderRadius: 16,
    borderColor: ColorsDefault.gray1,
    borderWidth: 1,
    fontSize: FontSizeDefault.FONT_20,
    fontFamily: FontFamilyDefault.primaryMedium,
    color: ColorsDefault.black3,
  },
  textMl: {
    fontSize: FontSizeDefault.FONT_16,
    fontFamily: FontFamilyDefault.primaryMedium,
    color: ColorsDefault.black3,
    marginLeft: '8@s',
  },
});

export default forwardRef(ModalAmountMilk);
