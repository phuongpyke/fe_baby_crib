import moment from 'moment';
import React, {
  forwardRef,
  useEffect,
  useImperativeHandle,
  useState,
} from 'react';
import {
  Modal,
  StyleProp,
  Text,
  TouchableOpacity,
  View,
  ViewStyle,
} from 'react-native';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import {s, ScaledSheet} from 'react-native-size-matters';
import {ColorsDefault, FontFamilyDefault, FontSizeDefault} from '/assets';
import {FULL_WIDTH_DEVICE} from '/common';

interface Props {
  customStyle?: StyleProp<ViewStyle>;
  boxStyle?: StyleProp<ViewStyle>;
  onChangeValue?: (value: string) => void;
}

const SIZE_BOX = (FULL_WIDTH_DEVICE - s(118)) / 4;

const SelectTimeSchedule = (props: Props, ref: any) => {
  const {customStyle, boxStyle, onChangeValue} = props;

  const [timer, setTimer] = useState('06:15');
  const [showModalTime, setShowModalTime] = useState(false);

  const onClickModalTime = () => setShowModalTime(!showModalTime);

  const handleConfirm = (date: any) => {
    const _time = moment(date).format('HH:mm');
    setTimer(_time);
    onClickModalTime();
  };

  useImperativeHandle(ref, () => ({
    add: (amount: number) => {
      const _time = moment(timer, 'HH:mm')
        .add(amount, 'minute')
        .format('HH:mm');
      setTimer(_time);
    },
    subtract: (amount: number) => {
      const _time = moment(timer, 'HH:mm')
        .subtract(amount, 'minute')
        .format('HH:mm');
      setTimer(_time);
    },
  }));

  useEffect(() => {
    if (onChangeValue) {
      onChangeValue(timer);
    }
  }, [timer]);

  return (
    <>
      <TouchableOpacity
        activeOpacity={0.8}
        style={[styles.container, customStyle]}
        onPress={onClickModalTime}>
        <View style={[styles.viewTime, boxStyle]}>
          <Text style={styles.textTime}>{timer.slice(0, 1)}</Text>
        </View>
        <View style={[styles.viewTime, boxStyle]}>
          <Text style={styles.textTime}>{timer.slice(1, 2)}</Text>
        </View>
        <View style={styles.viewDot}>
          <Text style={styles.textDot}>:</Text>
        </View>
        <View style={[styles.viewTime, boxStyle]}>
          <Text style={styles.textTime}>{timer.slice(3, 4)}</Text>
        </View>
        <View style={[styles.viewTime, boxStyle]}>
          <Text style={styles.textTime}>{timer.slice(4, 5)}</Text>
        </View>
      </TouchableOpacity>
      <Modal transparent={true} visible={showModalTime}>
        <DateTimePickerModal
          date={moment(timer, 'HH:mm').toDate()}
          isVisible={showModalTime}
          mode="time"
          locale="en_GB"
          onConfirm={handleConfirm}
          onCancel={onClickModalTime}
          confirmTextIOS={'Xác nhận'}
          cancelTextIOS={'Hủy'}
        />
      </Modal>
    </>
  );
};

const styles = ScaledSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: '34@s',
  },
  viewTime: {
    height: SIZE_BOX,
    width: SIZE_BOX,
    borderRadius: 6,
    backgroundColor: ColorsDefault.gray1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textTime: {
    fontSize: FontSizeDefault.FONT_24,
    fontFamily: FontFamilyDefault.primaryBold,
    color: ColorsDefault.black2,
  },
  textDot: {
    fontSize: FontSizeDefault.FONT_16,
    fontFamily: FontFamilyDefault.primaryMedium,
    color: ColorsDefault.black,
  },
  viewDot: {
    width: '20@s',
    alignItems: 'center',
  },
});

export default forwardRef(SelectTimeSchedule);
