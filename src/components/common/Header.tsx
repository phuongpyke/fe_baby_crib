import {useNavigation} from '@react-navigation/native';
import React, {ReactNode} from 'react';
import isEqual from 'react-fast-compare';
import {
  Image,
  StyleProp,
  Text,
  TextStyle,
  TouchableOpacity,
  View,
  ViewProps,
  ViewStyle,
} from 'react-native';
import {s, ScaledSheet} from 'react-native-size-matters';
import {ColorsDefault, FontFamilyDefault, FontSizeDefault} from '/assets';
import images from '/assets/images';
import {FULL_WIDTH_DEVICE} from '/common';
import {goBack} from '/navigations';

interface HeaderProps extends ViewProps {
  isBack?: boolean;
  title?: string;
  customStyle?: StyleProp<ViewStyle>;
  actionStyle?: StyleProp<ViewStyle>;
  titleStyle?: StyleProp<TextStyle>;
  onPressAction?(): void;
  customHandleBackPress?(): void;
  renderAction?: any;
}

const Header = (props: HeaderProps) => {
  const {
    isBack = true,
    title = '',
    customStyle,
    actionStyle,
    titleStyle,
    onPressAction,
    customHandleBackPress,
    renderAction,
  } = props;

  const navigation = useNavigation();

  const onBack = () => {
    if (customHandleBackPress) {
      customHandleBackPress();
      return;
    }
    navigation.goBack();
  };

  return (
    <View style={[styles.container, customStyle]}>
      <View style={[styles.viewHeader]}>
        {isBack ? (
          <TouchableOpacity onPress={onBack} style={styles.buttonBack}>
            <Image source={images.backLeft} style={{height: 20, width: 20}} />
          </TouchableOpacity>
        ) : (
          <View style={styles.buttonBack} />
        )}
        <View style={styles.viewCenter}>
          <Text style={[styles.title, titleStyle]} numberOfLines={1}>
            {title}
          </Text>
        </View>
        {renderAction && (
          <TouchableOpacity
            onPress={onPressAction}
            style={[styles.buttonAction, actionStyle]}>
            {renderAction?.()}
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    overflow: 'hidden',
  },
  viewHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: '50@s',
    width: '100%',
    paddingHorizontal: '16@s',
    backgroundColor: ColorsDefault.transparent,
  },
  buttonBack: {
    width: '40@s',
    justifyContent: 'center',
  },
  title: {
    fontSize: FontSizeDefault.FONT_18,
    fontFamily: FontFamilyDefault.primaryMedium,
    color: ColorsDefault.black3,
  },
  buttonAction: {
    width: '40@s',
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewCenter: {
    width: FULL_WIDTH_DEVICE,
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: -10,
    alignSelf: 'center',
  },
});

export default React.memo(Header, isEqual);
