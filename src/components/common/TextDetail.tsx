import React from 'react';
import {
  StyleProp,
  Text,
  TouchableWithoutFeedback,
  View,
  ViewStyle,
} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {ColorsDefault, FontFamilyDefault, FontSizeDefault} from '/assets';

interface Props {
  customStyle?: StyleProp<ViewStyle>;
  title: string;
  onPress?: () => void;
}

const TextDetail = (props: Props) => {
  const {customStyle, title, onPress} = props;

  return (
    <View style={[styles.container, customStyle]}>
      <Text style={styles.textTitle}>
        {title}
        <TouchableWithoutFeedback onPress={onPress}>
          <Text style={styles.textBtn}>{'Chi tiết'}</Text>
        </TouchableWithoutFeedback>
      </Text>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    backgroundColor: ColorsDefault.gray1,
    paddingHorizontal: '16@s',
    paddingVertical: '8@vs',
  },
  textTitle: {
    color: ColorsDefault.black3,
    fontSize: FontSizeDefault.FONT_14,
    fontFamily: FontFamilyDefault.primaryRegular,
    lineHeight: FontSizeDefault.FONT_22,
  },
  textBtn: {
    color: ColorsDefault.blue1,
    fontSize: FontSizeDefault.FONT_14,
    fontFamily: FontFamilyDefault.primarySemiBold,
    lineHeight: FontSizeDefault.FONT_22,
    textDecorationLine: 'underline',
  },
});

export default TextDetail;
