import React from 'react';
import Modal from 'react-native-modal';

import Loading from './Loading';
import Text from './Text';

type PropsType = {
  title?: string;
  loading: boolean;
};

const LoadingOverlay = ({title, loading, ...rest}: PropsType) => (
  <Modal
    isVisible={loading}
    style={{margin: 0, justifyContent: 'center', alignItems: 'center'}}
    backdropColor={'rgba(0, 0, 0, 0.8)'}
    {...rest}>
    <Loading />
    <Text>{title}</Text>
  </Modal>
);

export default LoadingOverlay;
