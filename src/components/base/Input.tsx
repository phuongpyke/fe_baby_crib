/* eslint-disable @typescript-eslint/no-use-before-define */
/* eslint-disable no-use-before-define */
/* eslint-disable @typescript-eslint/naming-convention */
import React, {forwardRef, Ref, RefObject} from 'react';
import {
  StyleProp,
  StyleSheet,
  TextInput,
  ViewStyle,
  TextInputProps,
} from 'react-native';
import {ColorsDefault, FontFamilyDefault} from '@assets';
import {moderateScale, SpaceInStyleType, verticalScale} from '@common';
import {Block, Text, IconSvg} from '@components/index';
import Image from './Image';
import Touchable from './Touchable';

export type PropsType = TextInputProps &
  SpaceInStyleType & {
    label?: string;
    required?: boolean;
    description?: string;
    disabled?: boolean;
    loading?: boolean;
    iconLeft?: string;
    iconRight?: string;
    iconRightPress?: () => void;
    error?: boolean | undefined | any;
    errorMessage?: string | any;
    multiline?: boolean;
    height?: number;
    fontSize?: number;
    success?: boolean;
    name?: string;
    control?: any | undefined;
    style?: Omit<StyleProp<ViewStyle>, 'height'>;
  };

const Input = (
  {
    m,
    mt,
    mr,
    mb,
    ml,
    mh,
    mv,
    label,
    required,
    description,
    disabled,
    loading,
    iconLeft,
    iconRight,
    iconRightPress,
    error,
    errorMessage,
    height,
    width,
    multiline,
    fontSize,
    success,
    name,
    control,
    style,
    ...rest
  }: PropsType,
  ref: Ref<TextInput> | RefObject<TextInput>,
) => {
  const _renderLabel = () =>
    label && (
      <Text type="c1" fontWeight="medium" mb={10} ml={4}>
        {label}
        {required && <Text color={ColorsDefault.textPink}>*</Text>}
      </Text>
    );

  const _renderIconLeft = () => iconLeft && <IconSvg xml={iconLeft} mr={10} />;

  const _renderIconRight = () =>
    iconRight && (
      <Touchable ml={5} onPress={iconRightPress && iconRightPress}>
        <Image source={iconRight} width={24} height={24} />
      </Touchable>
    );

  const _renderTextInput = () => {
    const textInputProps = {
      ref,
      editable: !disabled,
      placeholderTextColor: ColorsDefault.textPlaceHolder,
      multiline,
      ...rest,
    };

    const styledTextInput = [
      styles.textInput,
      disabled && styles.disabled,
      fontSize ? moderateScale(fontSize) : {fontSize: moderateScale(14)},
      height
        ? {height: verticalScale(height as number)}
        : {height: verticalScale(44)},
      multiline && {paddingTop: 15, textAlignVertical: 'top'},
    ];

    return (
      <TextInput
        autoCapitalize="none"
        style={styledTextInput as StyleProp<ViewStyle>}
        {...textInputProps}
      />
    );
  };

  const _renderErrorOrDescription = () => {
    if (error) {
      return (
        <Text
          type="c1"
          color={ColorsDefault.textRed}
          fontWeight="medium"
          mt={8}>
          {errorMessage}
        </Text>
      );
    }

    if (description) {
      return (
        <Text type="c2" mt={5} ml={4}>
          {description}
        </Text>
      );
    }
    return null;
  };

  const styledWrapperContainer: SpaceInStyleType = {
    m,
    mt,
    mr,
    mb,
    ml,
    mh,
    mv,
  };

  const styledWrapperInput = [
    style || styles.container,
    width,
    height
      ? {height: verticalScale(height as number)}
      : {height: verticalScale(40)},
    error && styles.error,
  ];

  return (
    <Block {...styledWrapperContainer}>
      {_renderLabel()}
      <Block
        row
        middle
        style={[
          styledWrapperInput,
          {
            paddingHorizontal: 16,
            backgroundColor: disabled ? '#EAEAEA' : ColorsDefault.gray1,
          },
        ]}>
        {_renderIconLeft()}
        {_renderTextInput()}
        {_renderIconRight()}
      </Block>
      {_renderErrorOrDescription()}
    </Block>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    backgroundColor: ColorsDefault.white,
    borderRadius: 2,
    borderWidth: 1,
    borderColor: ColorsDefault.textGrayDark,
  },
  error: {
    borderColor: ColorsDefault.textRed,
    borderWidth: 1,
  },
  disabled: {
    color: ColorsDefault.textPrimary,
  },

  textInput: {
    margin: 0,
    padding: 0,
    flex: 1,
    color: ColorsDefault.textPrimary,
    fontSize: 18,
    fontFamily: FontFamilyDefault.primaryRegular,
  },
});

export default forwardRef(Input);
