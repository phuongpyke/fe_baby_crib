import React from 'react';
import { SvgXml } from 'react-native-svg';
import Block from './Block';
import Touchable, { Propstype as TouchableType } from './Touchable';

type PropsType = TouchableType & {
  xml: string;
  touchable?: boolean;
  iconWidth?: number;
  iconHeight?: number;
  hitSlop?: Record<string, number>;
  size?: number;
};

const IconSvg = ({ xml, touchable, iconWidth, iconHeight, hitSlop, ...rest }: PropsType) => {
  const Wrapper = touchable ? Touchable : Block;
  if (iconWidth || iconWidth) {
    return (
      <Wrapper {...rest}>
        <SvgXml xml={xml} width={iconWidth} height={iconHeight} hitSlop={hitSlop} />
      </Wrapper>
    );
  }
  return (
    <Wrapper {...rest}>
      <SvgXml xml={xml} hitSlop={hitSlop} />
    </Wrapper>
  );
};

export default IconSvg;
