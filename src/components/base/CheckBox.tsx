import React from "react";
import { Image, Touchable } from "@components/base";
import images from "/assets/images";
import { ColorsDefault } from '@assets'
type PropsType = {
    isChecked: boolean,
    onPress: () => void,
    color?: string
}
const CheckBox = ({ isChecked, onPress, color }: PropsType) => {
    return (
        <Touchable onPress={() => {
            onPress && onPress()
        }}>
            <Image
                source={isChecked ? images.checkedBoxSquare : images.uncheckedBoxSquare}
                width={22}
                height={22}
                tintColor={isChecked ? ColorsDefault.btnPrimary : color}
            />
        </Touchable>
    )
}

export default CheckBox