import React from "react";
import { Image, Touchable, Block, Text } from "@components/base";
import images from "/assets/images";
import { ColorsDefault } from '@assets'

type PropsType = {
    item: any,
    onChange: (item: any) => void,
    selectListValue?: any,
    color?: string,
    isReplace?: boolean
}
const CheckBoxList = ({ item, onChange, selectListValue, isReplace = false }: PropsType) => {
    return (
        <Block row>
            <Touchable row alignItems="center"
                onPress={() => {
                    if (isReplace) {
                        if (item.value !== selectListValue) {
                            onChange(item.value);
                        } else {
                            onChange(null)
                        }
                    } else {
                        let index = selectListValue ? selectListValue?.findIndex((i: any) => i == item.value) : -1;
                        if (index === -1) {
                            onChange([...selectListValue, item.value]);
                        } else {
                            onChange(selectListValue?.filter((i: any) => i != item.value));
                        }
                    }
                }}>
                <Block>
                    {
                        isReplace ?
                            <Image
                                source={selectListValue === item.value ? images.checkedBoxSquare : images.uncheckedBoxSquare}
                                width={22}
                                height={22}
                                tintColor={ColorsDefault.bgPrimary}
                            />
                            :
                            <Image
                                source={selectListValue && (selectListValue?.indexOf(item.value.toString()) > -1 || selectListValue?.indexOf(item.value) > -1) ? images.checkedBoxSquare : images.uncheckedBoxSquare}
                                width={22}
                                height={22}
                                tintColor={ColorsDefault.bgPrimary}
                            />
                    }
                </Block>
                <Text type='c1' color={ColorsDefault.textPrimary} ml={10}>{item.label}</Text>
            </Touchable>
            <Block></Block>
        </Block>
    )
}

export default CheckBoxList;