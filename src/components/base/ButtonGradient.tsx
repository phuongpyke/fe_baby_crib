import React, { memo, ReactNode } from 'react';
import { Gradient, Button } from '@components/index';

interface Props {
  title?: string;
  onPress?: () => void;
}
const ButtonGradient = (props: Props) => (
    <Gradient
      colors={['#8AD4EC', '#EF96FF', '#FF56A9', '#FFAA6C']}
      start={{ x: 0, y: 0 }}
      end={{ x: 1, y: 1 }}
    >
      <Button
        variant='primary'
        children={props.title}
        onPress={props.onPress}
        gradient bg='transparent'/>
    </Gradient>
);

export default memo(ButtonGradient);
