import React, { forwardRef, Ref } from 'react';
import { RefreshControl, FlatList, FlatListProps } from 'react-native';
import { SpaceInStyleType } from '@common';
import { Block } from '@components/index';

type PropsType = FlatListProps<any> &
  SpaceInStyleType & {
    freshLoading?: boolean;
    onRefresh?: (params: any) => void;
    keyExtractor?: (item: Record<string, any>, index: number) => number | string;
  };

const List = (
  {
    m,
    mt,
    mr,
    mb,
    ml,
    mv,
    mh,
    p,
    pt,
    pr,
    pb,
    pl,
    pv,
    ph,
    freshLoading,
    onRefresh,
    keyExtractor,
    ...rest
  }: PropsType,
  ref: Ref<any>,
) => {
  const StyledWrap = {
    m,
    mt,
    mr,
    mb,
    ml,
    mv,
    mh,
    p,
    pt,
    pr,
    pb,
    pl,
    pv,
    ph,
  };

  return (
    <Block {...StyledWrap}>
      <FlatList
        ref={ref}
        refreshControl={
          onRefresh && <RefreshControl refreshing={freshLoading as boolean} onRefresh={onRefresh} />
        }
        keyExtractor={keyExtractor || ((item, index) => `${item.id || index}`)}
        {...rest}
      />
    </Block>
  );
};

export default forwardRef(List);
