import React, {memo, ReactNode} from 'react';
import {StyleProp, View, ViewStyle} from 'react-native';

import LinearGradient from 'react-native-linear-gradient';
import {
  moderateScale,
  SpaceInStyleType,
  LayoutInStyleType,
  BackgroudInStyleType,
  BorderInStyleType,
  verticalScale,
  scale,
} from '@common';
import {ColorsDefault} from '/assets/colors';

type Propstype = SpaceInStyleType &
  LayoutInStyleType &
  BorderInStyleType &
  BackgroudInStyleType & {
    shadowColor?: string;
    children?: ReactNode;
    style?: StyleProp<ViewStyle>;
    gradient?: boolean;
  };

const Block = ({
  gradient,
  flex,
  m,
  mt,
  mr,
  mb,
  ml,
  mv,
  mh,
  p,
  pt,
  pr,
  pb,
  pl,
  pv,
  ph,
  width,
  height,
  wrap,
  flexGrow,
  row,
  column,
  direction,
  center,
  justifyStart,
  justifyEnd,
  justifyBetween,
  justifyAround,
  justifyEvenly,
  justify,
  middle,
  alignItemsStart,
  alignItemsEnd,
  alignItems,
  alignSelfCenter,
  borderRadius,
  borderWidth,
  borderBottomWidth,
  borderTopWidth,
  borderTRRadius,
  borderTLRadius,
  borderBRRadius,
  borderBLRadius,
  borderColor,
  borderBottom,
  borderTop,
  borderAll,
  borderLeft,
  borderRight,
  borderStyle,
  shadowColor,
  absolute,
  relative,
  top,
  left,
  bottom,
  right,
  zIndex,
  circle,
  bg,
  opacity,
  shadow,
  children,
  style,
  ...rest
}: Propstype) => {
  const styledComponent = [
    flex && {flex},
    width && {width: typeof width === 'number' ? width : width},
    height && {
      height: typeof height === 'number' ? height : height,
    },

    m && {margin: moderateScale(m)},
    mt && {marginTop: verticalScale(mt)},
    mr && {marginRight: scale(mr)},
    mb && {marginBottom: verticalScale(mb)},
    ml && {marginLeft: scale(ml)},
    mh && {marginHorizontal: mh},
    mv && {marginVertical: verticalScale(mv)},
    p && {padding: moderateScale(p)},
    pt && {paddingTop: verticalScale(pt)},
    pr && {paddingRight: scale(pr)},
    pb && {paddingBottom: verticalScale(pb)},
    pl && {paddingLeft: scale(pl)},
    ph && {paddingHorizontal: scale(ph)},
    pv && {paddingVertical: verticalScale(pv)},
    circle && {
      width: circle,
      height: circle,
      borderRadius: circle / 2,
    },
    borderStyle && {borderStyle: borderStyle},
    row && {flexDirection: 'row'},
    column && {flexDirection: 'column'},
    direction && {flexDirection: direction},
    wrap && {flexWrap: 'wrap'},
    flexGrow && {flexGrow},
    center && {justifyContent: 'center'},
    justifyStart && {justifyContent: 'flex-start'},
    justifyEnd && {justifyContent: 'flex-end'},
    justifyAround && {justifyContent: 'space-around'},
    justifyBetween && {justifyContent: 'space-between'},
    justifyEvenly && {justifyContent: 'space-evenly'},
    justify && {justifyContent: justify},
    middle && {alignItems: 'center'},
    alignItemsStart && {alignItems: 'flex-start'},
    alignItemsEnd && {alignItems: 'flex-end'},
    alignItems && {alignItems},
    alignSelfCenter && {alignSelf: 'center'},

    borderTRRadius && {borderTopRightRadius: borderTRRadius},
    borderTLRadius && {borderTopLeftRadius: borderTLRadius},
    borderBRRadius && {borderBottomLeftRadius: borderBRRadius},
    borderBLRadius && {borderBottomRightRadius: borderBLRadius},

    bg && {backgroundColor: bg},
    borderRadius && {borderRadius},
    borderWidth && {borderWidth},
    borderColor && {borderColor},
    opacity && {opacity},
    borderBottom && {
      borderBottomWidth: borderBottomWidth || 1,
      borderBottomColor: borderColor || ColorsDefault.gray,
    },
    borderAll && {
      borderWidth: borderWidth || 1,
      borderColor: borderColor || ColorsDefault.gray,
    },
    borderTop && {
      borderTopWidth: borderTopWidth || 1,
      borderTopColor: borderColor || ColorsDefault.gray,
    },
    borderLeft && {
      borderLeftWidth: borderWidth || 1,
      borderLeftColor: borderColor || ColorsDefault.gray,
    },
    borderRight && {
      borderRightWidth: borderWidth || 1,
      borderRightColor: borderColor || ColorsDefault.gray,
    },
    absolute && {position: 'absolute'},
    relative && {position: 'relative'},
    (top || top === 0) && {top},
    (left || left === 0) && {left},
    (bottom || bottom === 0) && {bottom},
    (right || right === 0) && {right},
    zIndex && {zIndex},
    shadow && {
      shadowColor: ColorsDefault.black || shadowColor,
      shadowOffset: {
        width: 0,
        height: 1,
      },
      shadowOpacity: 0.22,
      shadowRadius: 2.22,
      elevation: 3,
    },
    style && style,
  ];

  if (gradient) {
    return (
      <LinearGradient
        start={{x: 0, y: 0.5}}
        end={{x: 1, y: 0.5}}
        colors={['#1D4170', '#881776']}
        style={styledComponent as StyleProp<ViewStyle>}
        {...rest}>
        {children}
      </LinearGradient>
    );
  }

  return (
    <View style={styledComponent as StyleProp<ViewStyle>} {...rest}>
      {children}
    </View>
  );
};

export default memo(Block);
