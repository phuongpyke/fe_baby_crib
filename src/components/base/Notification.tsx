import React from 'react';
import {Touchable, Text, Block} from '@components/base';
import {ColorsDefault} from '/assets';
import {INotificationContent} from '/services/type';

interface Props {
  notiContent: INotificationContent;
  disable?: boolean;
  isCarDetail?: boolean;
  isCenter?: boolean;
}

const Notification = ({
  notiContent,
  disable = false,
  isCarDetail = false,
  isCenter = false,
}: Props) => {
  return (
    <Block width="100%" alignItems="center" height={39}>
      <Touchable
        disabled={disable}
        bg={ColorsDefault.bgPrimary}
        width="100%"
        height={39}
        justify={
          isCenter
            ? 'center'
            : notiContent?.numberOfSearchResult !== undefined || !isCarDetail
            ? 'flex-start'
            : 'center'
        }
        alignItems={'center'}
        row={
          notiContent?.numberOfSearchResult !== undefined || !isCarDetail
            ? true
            : false
        }>
        {notiContent?.numberOfSearchResult !== undefined ? (
          <Text color={ColorsDefault.white} ml={10} fontWeight="bold" type="h4">
            {notiContent?.numberOfSearchResult}台
          </Text>
        ) : (
          <></>
        )}
        <Text
          maxWidth={'80%'}
          ml={notiContent?.numberOfSearchResult ? 0 : 10}
          numberOfLines={1}
          color={ColorsDefault.white}
          fontWeight={
            notiContent?.numberOfSearchResult !== undefined || !isCarDetail
              ? 'medium'
              : 'bold'
          }
          type="c1">
          {notiContent?.text}
        </Text>
      </Touchable>
    </Block>
  );
};
export default Notification;
