import React from 'react';
import RNPickerSelect, {PickerSelectProps} from 'react-native-picker-select';
import {Block, Image, Text} from '@components/base';
import {ColorsDefault} from '/assets';
import images from '/assets/images';
import {Platform} from 'react-native';

type DropwDownProps = PickerSelectProps & {
  items: any;
  placeholder?: any;
  styles?: any;
  heightInput?: number;
  stylesPlaceholder?: any;
  style?: any;
};

const PickerSelect = ({
  items,
  heightInput,
  placeholder,
  ...rest
}: DropwDownProps) => {
  return (
    <Block
      borderWidth={1}
      borderColor={ColorsDefault.textGrayDark}
      borderRadius={2}>
      <RNPickerSelect
        {...rest}
        items={items}
        useNativeAndroidPickerStyle={false}
        placeholder={{label: placeholder ? placeholder : ''}}
        style={{
          viewContainer: {
            justifyContent: 'center',
            backgroundColor: ColorsDefault.gray2,
            height: 54,
            borderRadius: 10,
            ...(rest.style && rest.style),
          },
          placeholder: {
            color: ColorsDefault.gray1,
            opacity: 0.8,
            fontSize: 20,
            textAlign: 'center',
            ...(rest.stylesPlaceholder && rest.stylesPlaceholder),
          },
          inputAndroid: {
            color: ColorsDefault.gray1,
            fontSize: 20,
            fontWeight: 'bold',
            textAlign: 'center',
            height: 40,
            ...(rest.styles && rest.styles),
          },
          inputIOS: {
            color: ColorsDefault.gray1,
            fontSize: 20,
            textAlign: 'center',
            fontWeight: 'bold',
            height: heightInput ? heightInput : 50,
            paddingHorizontal: 16,
            ...(rest.styles && rest.styles),
          },
        }}
        Icon={() => (
          <Block center middle>
            <Image
              style={{
                width: 32,
                height: 22,
              }}
              mr={16}
              mt={4}
              source={images.arrowDown}
            />
          </Block>
        )}
      />
    </Block>
  );
};

export default PickerSelect;
