import {
  BottomTabBarProps,
  createBottomTabNavigator,
} from '@react-navigation/bottom-tabs';
import React from 'react';
import Dashboard from '../screens/dashboard';
import Notification from '../screens/notifications';
import Settings from '../screens/settings';
import TabBar from './components/TabBar';
import images from '/assets/images';
import BabyCrib from '/screens/BabyCrib';

const Tab = createBottomTabNavigator();

const TabScenes = () => {
  const ArrayTabs = [
    {
      name: 'Home',
      title: '',
      component: Dashboard,
      icon: images.tab.home,
    },
    {
      name: 'Crib',
      title: '',
      component: BabyCrib,
      icon: images.tab.crib,
    },
    {
      name: 'Notification',
      title: '',
      component: Notification,
      icon: images.tab.notify,
    },
    {
      name: 'Settings',
      title: '',
      component: Settings,
      icon: images.tab.setting,
    },
  ];

  return (
    <Tab.Navigator
      screenOptions={{headerShown: false}}
      tabBar={(props: BottomTabBarProps) => <TabBar {...props} />}>
      {ArrayTabs.map((item: any, index: number) => (
        <Tab.Screen key={`${index}`} options={{...item}} {...item} />
      ))}
    </Tab.Navigator>
  );
};

export default TabScenes;
