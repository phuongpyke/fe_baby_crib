import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React, {useEffect} from 'react';
import reactotron from 'reactotron-react-native';
import EditProfile from '../screens/EditProfile';
import Patients from '../screens/patients';
import ScanDevices from '../screens/ScanDevices';
import Settings from '../screens/settings';
import Timer from '../screens/Timer';
import {Constants} from '../utils';
import {NavigiationOption} from './NavigationOption';
import {Routes} from './Routes';
import TabScenes from './TabScenes';
import {getAllLabels} from '/api/modules/news';
import {ColorsDefault} from '/assets';
import {scheduleActions} from '/modules/schedule/slice';
import {useAppDispatch} from '/redux/hooks';
import AddCrib from '/screens/AddCrib';
import AddPatient from '/screens/AddPatient';
import AddSchedule from '/screens/AddSchedule';
import BabyCare from '/screens/BabyCare';
import BabySpending from '/screens/BabySpending';
import CreateSchedule from '/screens/CreateSchedule';
import DiagnosisByAI from '/screens/DiagnosisByAI';
import Diapers from '/screens/diapers';
import DiapersDetail from '/screens/diapersDetail';
import EasyDetail from '/screens/EasyDetail';
import EditSchedule from '/screens/EditSchedule';
import ListBluetooth from '/screens/ListBluetooth';
import ListVaccination from '/screens/ListVaccination';
import ListWifi from '/screens/ListWifi';
import LoadingWifi from '/screens/loadingWifi';
import MilkDetail from '/screens/MilkDetail';
import Milks from '/screens/Milks';
import PatientDetail from '/screens/PatientDetail';
import QrScreen from '/screens/Qrcode';
import Repeat from '/screens/repeat';
import ScanWifi from '/screens/ScanWifi';
import StoreEasy from '/screens/StoreEasy';
import TimerDetail from '/screens/TimerDetail';
import UpdateVaccinSchedule from '/screens/UpdateVaccinSchedule';
import VaccinDetail from '/screens/VaccinDetail';
import VaccinSchedule from '/screens/VaccinSchedule';

const Stack = createNativeStackNavigator();

const MainScenes = () => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    const getData = async () => {
      try {
        const res: any = await Promise.all([getAllLabels()]);
        if (res[0]?.success) {
          dispatch(scheduleActions.setLabels(res[0]?.data));
        }
      } catch (err) {
        reactotron.log!(err);
      }
    };
    getData();
  }, []);

  return (
    <Stack.Navigator
      screenOptions={{
        headerTitleAlign: 'center',
        headerTintColor: ColorsDefault.black,
        headerTitleStyle: {
          fontSize: Constants.txtSize,
        },
        headerStyle: {
          backgroundColor: '#828282',
        },
        headerBackTitleVisible: false,
      }}>
      <Stack.Screen
        name={Routes.Dashboard}
        component={TabScenes}
        options={{headerShown: false}}
      />
      {/*
        Active Crib
      */}
      <Stack.Screen
        name={Routes.ScanDevices}
        component={ScanDevices}
        options={p => NavigiationOption({...p, title: 'Bluetooth'})}
      />
      <Stack.Screen
        name={Routes.Wifi}
        component={ScanWifi}
        options={p => NavigiationOption({...p, title: 'Wifi'})}
      />
      <Stack.Screen
        name={Routes.AddCrib}
        component={AddCrib}
        options={p => NavigiationOption({...p, title: 'Thêm nôi'})}
      />
      <Stack.Screen
        name={Routes.ListBluetooth}
        component={ListBluetooth}
        options={p => NavigiationOption({...p, title: 'Bluetooth'})}
      />
      <Stack.Screen
        name={Routes.ListWifi}
        component={ListWifi}
        options={p => NavigiationOption({...p, title: 'Wifi'})}
      />
      {/*
       */}
      <Stack.Screen
        name="Setting"
        component={Settings}
        options={{headerShown: false}}
      />
      <Stack.Screen
        options={p =>
          NavigiationOption({
            ...p,
            title: 'Cập nhật thông tin bé',
            isPreventSwipeBack: false,
            headerShadowVisible: false,
          })
        }
        name={Routes.EditProfile}
        component={EditProfile}
      />
      <Stack.Screen
        name="TimerScreen"
        component={Timer}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={Routes.QrScreen}
        component={QrScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        options={p => NavigiationOption({...p, title: 'Bỉm'})}
        name={Routes.Diapers}
        component={Diapers}
      />
      <Stack.Screen
        options={p => NavigiationOption({...p, title: 'Bỉm'})}
        name={Routes.DiapersDetail}
        component={DiapersDetail}
      />
      <Stack.Screen
        options={p => NavigiationOption({...p, title: 'Sữa'})}
        name={Routes.Milks}
        component={Milks}
      />
      <Stack.Screen
        options={p => NavigiationOption({...p, title: 'Sữa'})}
        name={Routes.MilkDetail}
        component={MilkDetail}
      />

      <Stack.Screen
        options={{headerShown: false}}
        name={Routes.Repeat}
        component={Repeat}
      />
      <Stack.Screen
        options={p => NavigiationOption({...p, title: 'Bệnh án'})}
        name={Routes.Patients}
        component={Patients}
      />
      <Stack.Screen
        options={p => NavigiationOption({...p, title: 'Chi tiết'})}
        name={Routes.PatientDetail}
        component={PatientDetail}
      />
      <Stack.Screen
        options={p => NavigiationOption({...p, title: 'Thêm bệnh án'})}
        name={Routes.AddPatient}
        component={AddPatient}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name={Routes.LoadingWifi}
        component={LoadingWifi}
      />
      <Stack.Screen
        options={p => NavigiationOption({...p, title: 'Chăm sóc bé'})}
        name={Routes.BabyCare}
        component={BabyCare}
      />
      <Stack.Screen
        // options={p => NavigiationOption({...p, title: 'Tổng hợp lịch chăm bé'})}
        options={{
          headerShown: false,
        }}
        name={Routes.Schedule}
        component={Timer}
      />
      <Stack.Screen
        options={p => NavigiationOption({...p, title: 'Đoán bệnh bé bằng AI'})}
        name={Routes.DiagnosisByAI}
        component={DiagnosisByAI}
      />
      <Stack.Screen
        options={p =>
          NavigiationOption({
            ...p,
            title: 'Chi tiêu',
            headerShadowVisible: false,
          })
        }
        name={Routes.BabySpending}
        component={BabySpending}
      />
      <Stack.Screen
        options={p =>
          NavigiationOption({
            ...p,
            title: 'Lịch tiêm phòng',
            headerShadowVisible: false,
          })
        }
        name={Routes.VaccinSchedule}
        component={VaccinSchedule}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name={Routes.ListVaccination}
        component={ListVaccination}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name={Routes.VaccinDetail}
        component={VaccinDetail}
      />
      <Stack.Screen
        options={p =>
          NavigiationOption({
            ...p,
            title: 'Cập nhật lịch tiêm',
          })
        }
        name={Routes.UpdateVaccinSchedule}
        component={UpdateVaccinSchedule}
      />
      {/* Easy */}
      <Stack.Screen
        options={{headerShown: false}}
        name={Routes.StoreEasy}
        component={StoreEasy}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name={Routes.EasyDetail}
        component={EasyDetail}
      />
      {/* Schedule */}
      <Stack.Group
        screenOptions={{
          presentation: 'modal',
          headerBackVisible: false,
          headerShadowVisible: false,
        }}>
        <Stack.Screen
          options={p =>
            NavigiationOption({
              ...p,
              title: 'Chi tiết lịch',
              headerLeft: undefined,
            })
          }
          name={Routes.TimerDetail}
          component={TimerDetail}
        />
        <Stack.Screen
          options={p =>
            NavigiationOption({
              ...p,
              title: 'Sửa lịch',
            })
          }
          name={Routes.EditSchedule}
          component={EditSchedule}
        />
        <Stack.Screen
          options={p =>
            NavigiationOption({
              ...p,
              title: 'Cài đặt khung giờ cho bé',
            })
          }
          name={Routes.AddSchedule}
          component={AddSchedule}
        />
        <Stack.Screen
          options={p =>
            NavigiationOption({
              ...p,
              title: 'Thêm lịch',
            })
          }
          name={Routes.CreateSchedule}
          component={CreateSchedule}
        />
      </Stack.Group>
    </Stack.Navigator>
  );
};

export default MainScenes;
