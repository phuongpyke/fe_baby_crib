import {NativeStackNavigationOptions} from '@react-navigation/native-stack';
import React from 'react';
import {ColorsDefault} from '/assets';
import images from '/assets/images';
import {Block, Image, Text, Touchable} from '/components';

interface Props extends NativeStackNavigationOptions {
  navigation: any;
  root?: boolean;
  isPreventSwipeBack?: boolean;
  title: string;
}

export const NavigiationOption = (
  props: Props,
): NativeStackNavigationOptions => ({
  gestureEnabled: props.isPreventSwipeBack ? false : true,
  headerStyle: {backgroundColor: ColorsDefault.white},
  headerTitleAlign: 'center',
  headerLeft: () => (
    <Touchable onPress={() => props?.navigation.goBack()}>
      <Block width={40} height={40} alignItemsStart center>
        <Image width={20} height={20} source={images.backLeft} />
      </Block>
    </Touchable>
  ),
  headerTitle: () => (
    <Text fontWeight={'medium'} fontSize={16} lineHeight={22}>
      {props.title}
    </Text>
  ),
  ...props,
});
