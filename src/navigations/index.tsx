import {
  CommonActions,
  NavigationContainer,
  StackActions,
} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {useSelector} from 'react-redux';
import AuthScenes from './AuthScenes';
import MainScenes from './MainScenes';
import {Routes} from './Routes';
import {tokenSelector} from '/modules/auth/selectors';

const Stack = createNativeStackNavigator();

const navigationRef = React.createRef<any>();

export function navigate(name: string, params?: any) {
  navigationRef.current?.navigate(name, params);
}

export function goBack() {
  navigationRef.current?.goBack();
}

export function replace(name: string, params: any) {
  navigationRef.current?.dispatch(StackActions.replace(name, params));
}

export function reset(routes: any[]): void {
  navigationRef.current.dispatch({
    ...CommonActions.reset({
      index: 1,
      routes,
    }),
  });
}

const TabNavigator = () => {
  const token = useSelector(tokenSelector);

  return (
    <SafeAreaProvider>
      <NavigationContainer ref={navigationRef}>
        <Stack.Navigator
          initialRouteName={!token ? Routes.AuthStack : Routes.MainStack}>
          <Stack.Screen
            name={Routes.AuthStack}
            options={{headerShown: false}}
            component={AuthScenes}
          />
          <Stack.Screen
            options={{headerShown: false}}
            name={Routes.MainStack}
            component={MainScenes}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </SafeAreaProvider>
  );
};

export default TabNavigator;
