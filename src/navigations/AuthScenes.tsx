import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React from 'react';
import Login from '../screens/Login';
import Otp from '../screens/Otp';
import Register from '../screens/register';
import {Routes} from './Routes';
import {ForgotPassword} from '/screens/forgotPassword';
import {ForgotPasswordComplete} from '/screens/ForgotPasswordComplete';

const Stack = createNativeStackNavigator();

const AuthScenes = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        options={{
          headerShown: false,
        }}
        name={Routes.Login}
        component={Login}
      />
      <Stack.Screen
        options={{
          headerShown: false,
        }}
        name={Routes.Register}
        component={Register}
      />
      <Stack.Screen
        options={{
          headerShown: false,
        }}
        name={Routes.Otp}
        component={Otp}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name={Routes.ForgotPassword}
        component={ForgotPassword}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name={Routes.ForgotPasswordComplete}
        component={ForgotPasswordComplete}
      />
    </Stack.Navigator>
  );
};

export default AuthScenes;
