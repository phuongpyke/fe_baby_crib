import React, {useMemo} from 'react';
import isEqual from 'react-fast-compare';
import {View} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {ScaledSheet} from 'react-native-size-matters';
import ItemTabBar from './ItemTabBar';
import {ColorsDefault} from '/assets';
import {FULL_WIDTH_DEVICE} from '/common';
import {useAppSelector} from '/redux/hooks';

const TabBar = ({state, descriptors, navigation}: any) => {
  const insets = useSafeAreaInsets();
  const {bleConnect} = useAppSelector(
    ({user: {bleConnect}}) => ({bleConnect}),
    isEqual,
  );

  const bottom = useMemo(() => {
    if (insets.bottom) {
      return insets.bottom;
    }
    return 10;
  }, [insets]);

  return (
    <View style={styles.container}>
      <View style={[styles.viewGroupTabs, {marginBottom: bottom}]}>
        {state.routes.map((route: any, index: number) => {
          const {options} = descriptors[route.key];
          const isFocused = state.index === index;

          const onPress = () => {
            const event = navigation.emit({
              type: 'tabPress',
              target: route.key,
              canPreventDefault: true,
            });
            if (!isFocused && !event.defaultPrevented) {
              navigation.navigate({name: route.name, merge: true});
            }
          };

          const onLongPress = () => {
            navigation.emit({
              type: 'tabLongPress',
              target: route.key,
            });
          };
          if (route.name === 'Crib' && bleConnect.length === 0) {
            return null;
          }
          return (
            <ItemTabBar
              key={route.key}
              isFocused={isFocused}
              options={options}
              onPress={onPress}
              onLongPress={onLongPress}
            />
          );
        })}
      </View>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    backgroundColor: ColorsDefault.white,
  },
  viewGroupTabs: {
    flexDirection: 'row',
    width: FULL_WIDTH_DEVICE - 32,
    backgroundColor: ColorsDefault.white,
    justifyContent: 'center',
    alignSelf: 'center',
    borderRadius: 100,
    paddingVertical: 6,
    shadowColor: ColorsDefault.black,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.17,
    shadowRadius: 4,
    elevation: 2,
  },
});

export default TabBar;
