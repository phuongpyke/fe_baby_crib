import React from 'react';
import {TouchableOpacity} from 'react-native';
import Animated, {
  Easing,
  interpolate,
  useAnimatedStyle,
  useDerivedValue,
  withTiming,
} from 'react-native-reanimated';
import {ScaledSheet} from 'react-native-size-matters';
import {ColorsDefault} from '/assets';

interface Props {
  isFocused: boolean;
  options: any;
  onPress(): void;
  onLongPress(): void;
}

const ItemTabBar = ({isFocused, options, onPress, onLongPress}: Props) => {
  const progress = useDerivedValue(() => {
    const val = Number(!isFocused);
    return withTiming(val, {duration: 300, easing: Easing.ease});
  }, [isFocused]);

  const animatedStyles = useAnimatedStyle(() => {
    const scale = interpolate(progress.value, [0, 1], [0.9, 1]);
    return {
      transform: [{scale}],
    };
  });

  return (
    <TouchableOpacity
      activeOpacity={0.8}
      accessibilityRole="button"
      accessibilityState={isFocused ? {selected: true} : {}}
      onPress={onPress}
      onLongPress={onLongPress}
      style={styles.tabButton}>
      <Animated.Image
        source={options.icon}
        style={[
          styles.tabIcon,
          {tintColor: isFocused ? ColorsDefault.blue1 : ColorsDefault.gray3},
          animatedStyles,
        ]}
        resizeMode={'contain'}
      />
    </TouchableOpacity>
  );
};

const styles = ScaledSheet.create({
  tabButton: {
    height: 48,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  tabIcon: {
    height: 30,
    width: 30,
  },
});

export default ItemTabBar;
