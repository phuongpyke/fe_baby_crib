import * as Constants from './constants/constant';
import moment from 'moment';

export {Constants};

export const getAge = (start: string) => {
  const _start = moment(start, 'YYYY-MM-DD');
  const end = moment(new Date(), 'YYYY-MM-DD');

  return end.diff(_start, 'days');
};
