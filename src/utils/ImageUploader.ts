import ImagePicker from 'react-native-image-crop-picker';

const MAX_WIDTH = 800;
const MAX_HEIGHT = 800;

const ImageUploaded = {
  chooseImageFromCamera: () =>
    ImagePicker.openCamera({
      // width: MAX_WIDTH,
      // height: MAX_HEIGHT,
      compressImageMaxWidth: MAX_WIDTH,
      compressImageMaxHeight: MAX_HEIGHT,
      waitAnimationEnd: true,
      // includeBase64: true,
      // forceJpg: true,
      cropping: true,
    }),

  chooseImageFromGallery: () =>
    ImagePicker.openPicker({
      // width: MAX_WIDTH,
      // height: MAX_HEIGHT,
      compressImageMaxWidth: MAX_WIDTH,
      compressImageMaxHeight: MAX_HEIGHT,
      // compressImageQuality: 100,
      waitAnimationEnd: true,
      // includeBase64: true,
      // forceJpg: true,
      cropping: true,
    }),

  uploader: async (localPath: any, name: string) => {
    const timeStamp = new Date().getTime();
    const formatImage: any = {
      uri: localPath.path,
      name: `${timeStamp}.${'image/jpeg'}`,
      type: 'image/jpeg',
    };
    const formData = new FormData();
    formData.append(name, formatImage);
    return formData;
  },
};

export default ImageUploaded;
