export enum ELabel {
  TYPE_1 = '1',
  TYPE_2 = '2',
  TYPE_3 = '3',
}

export enum EStatusInject {
  INJECTED = 0,
  NOT_INJECTED = 1,
}

export enum EGender {
  MALE = 0,
  FEMALE = 1,
}
