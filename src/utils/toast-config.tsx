import React from 'react';
import {Text, View} from 'react-native';

export const toastConfig = {
  custom: ({props, text1}: any) => (
    <View
      style={{
        height: 45,
        width: 300,
        backgroundColor: props.type === 'success' ? '#DAFFE0' : '#EB5757',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 8,
      }}>
      <Text>{text1}</Text>
    </View>
  ),
};
