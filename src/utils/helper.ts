import AsyncStorage from '@react-native-async-storage/async-storage';
import {Insets} from 'react-native';
import Toast from 'react-native-toast-message';
import {labelSchedule} from './constants/constant';
import {ELabel} from './enum';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';

export const generatePersistConfig = (key: string, whitelist: string[]) => {
  return {
    key,
    whitelist,
    version: 1,
    debug: __DEV__,
    storage: AsyncStorage,
    stateReconciler: autoMergeLevel2,
  };
};

export const findLabelSchedule = (type: ELabel) => {
  const findItem = labelSchedule.find(item => item.type === type);
  if (findItem) {
    return findItem?.title;
  }
  return '';
};

export const hitSlop = (value: number): Insets => {
  const insets: Insets = {top: value, bottom: value, left: value, right: value};
  return insets;
};

export const showToastSuccess = (message: string) => {
  if (message) {
    Toast.show({
      type: 'custom',
      text1: message,
      props: {
        type: 'success',
      },
    });
  }
};

export const showToastError = (message: string) => {
  if (message) {
    Toast.show({
      type: 'custom',
      text1: message,
      props: {
        type: 'error',
      },
    });
  }
};

export const getOptionSelected = (
  data: any[],
  value: any,
  schema = {
    label: 'label',
    value: 'value',
  },
) => {
  const itemSelected = data?.find(
    (item: any) => item?.[schema.value] === value,
  );
  return itemSelected?.[schema.label] || data?.[0]?.[schema.label];
};

export const tagsStyles: any = {};
