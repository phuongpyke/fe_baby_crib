import {ELabel} from '../enum';

export const txtSize = 16;
export const titleSize = 20;
export const title = 24;
export const borderRadius = 6;
export const fontWeight = '600';
export const fontWeightNormal = '400';

// Login screen
export const Hello = 'Xin Chào,';
export const RequireLogin = 'Vui lòng đăng nhập để sử dụng';
export const ForgotPassword = 'Quên mật khẩu';
export const Login = 'Đăng nhập';
export const ScanQR = 'Quét QR Code để đăng ký';
export const ScanQRText = 'Quét mã';
export const InstructionScanQR = 'Hướng khung camera vào mã QR để quét';
export const FlashOn = 'Bật đèn pin';
export const FlashOff = 'Tắt đèn pin';
export const ScanGallery = 'Hình có sẵn';
export const ErrPhone = 'Hãy nhập số điện thoại của bạn';
export const ErrPass = 'Hãy nhập mật khẩu của bạn';
export const HaventAccount = 'Bạn chưa có tài khoản?';

// Register screen
export const Register = 'Đăng ký';
export const RegisterText1 = 'Hãy đăng ký tài khoản';
export const RegisterText2 = 'để theo dõi sức khoẻ của bé nhé';
export const HaveAccount = 'Bạn đã có tài khoản?';
export const ErrRePass = 'Hãy nhập lại mật khẩu của bạn';

// Settings screen
export const Settings = 'Cài đặt';
export const EditProfile = 'Chỉnh sửa thông tin bé';
export const ChangePass = 'Đổi mật khẩu';
export const Language = 'Ngôn ngữ';
export const DarkMode = 'Chế độ tối';
export const LogOut = 'Đăng xuất';

// Edit Info screen
export const Name = 'Tên bé';
export const DOB = 'Ngày tháng năm sinh';
export const Weight = 'Cân nặng';
export const HealthCondition = 'Bé đang bị bệnh';
export const Cough = 'Ho';
export const Done = 'Hoàn thành';
export const Cancel = 'Huỷ';
export const Confirm = 'Xác nhận';

// Dashboard
export const InfoBaby = 'Thông tin bé';
export const CribInfo = 'Thông tin từ nôi';
export const Height = 'Chiều cao';
export const TemperatureCrib = 'Nhiệt độ từ nôi';
export const HealthConditionDashboard = 'Bệnh lý';
export const CryStatus = 'Tình trạng khóc';
export const Drink = 'Sữa';
export const OverallCover = 'Chăm sóc toàn diện bé';
export const Sleep = 'Ngủ';
export const Diapers = 'Bỉm';
export const BabyTime = 'Thời gian cho bé';
export const Doctor = 'Khám bệnh';
export const DoctorAI = 'Đoán bệnh bé bằng AI';
export const CareSuggestion = 'Gợi ý chăm sóc bé';
export const AiConclude = 'AI tổng kết tình trạng bé';
export const BabyTricks = 'Các tricks cho bé';

// Crib screen
export const CribTitle = 'Nôi của bé';
export const CribSettings = 'Cài đặt nôi';
export const FollowInDay = 'Theo dõi trong ngày';
export const StatusConnectCrib = 'Tình trạng kết nối nôi:';
export const Crying = 'Bé đang khóc nhè';
export const Connected = 'Đã kết nối';
export const AutoTurnOn = 'Tự động bật';
export const AutoTurnOn1 = 'Tự động bật rung và âm nhạc bé khóc';
export const AutoTurnOn2 = 'Tự tắt sau khi bé nín khóc 30 phút.';
export const Vibrate = 'Rung';
export const Music = 'Âm nhạc';
export const Light = 'Đèn';
export const Temperature = 'Nhiệt độ';
export const Cry = 'Khóc';
export const NoDevices = 'Chưa có nôi';
export const NoDevices2 = 'Bạn chưa có nôi nào được kết nối';
export const AddCrib = 'Thêm nôi mới';

// Timer screen
export const Timer = 'Hẹn giờ';
export const TimerFor = 'Hẹn giờ cho';
export const Time = 'Thời gian';
export const Repeat = 'Lặp lại';
export const Create = 'Tạo';
export const RecentEvents = 'Các sự kiện gần đây';
export const ViewAll = 'Xem tất cả';

// Notification screen
export const Notification = 'Thông báo';

// Data
export const repeatMode = [
  {label: 'Lặp lại theo ngày', key: 'days'},
  {label: 'Lặp lại theo giờ', key: 'hours'},
];

export const repeatDays = [
  {label: 'Mọi thứ Hai', key: 'mon'},
  {label: 'Mọi thứ Ba', key: 'tue'},
  {label: 'Mọi thứ Tư', key: 'wed'},
  {label: 'Mọi thứ Năm', key: 'thu'},
  {label: 'Mọi thứ Sáu', key: 'fri'},
  {label: 'Mọi thứ Bảy', key: 'sat'},
  {label: 'Mọi Chủ Nhật', key: 'sun'},
];

export const labelSchedule = [
  {title: 'Dậy & Ăn', type: ELabel.TYPE_1},
  {title: 'Chơi', type: ELabel.TYPE_2},
  {title: 'Bé ti', type: ELabel.TYPE_3},
];
