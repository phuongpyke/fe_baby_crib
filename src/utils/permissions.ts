import {Platform} from 'react-native';
import {check, PERMISSIONS, request, RESULTS} from 'react-native-permissions';

export const checkCamera = async (callback?: () => void) => {
  try {
    const checkPermission = await check(
      Platform.OS === 'ios'
        ? PERMISSIONS.IOS.CAMERA
        : PERMISSIONS.ANDROID.CAMERA,
    );
    if (checkPermission === RESULTS.BLOCKED) {
      callback?.();
      return false;
    }
    if (checkPermission === RESULTS.DENIED) {
      const result = await request(
        Platform.OS === 'ios'
          ? PERMISSIONS.IOS.CAMERA
          : PERMISSIONS.ANDROID.CAMERA,
      );
      return result === RESULTS.GRANTED;
    }
    if (checkPermission === RESULTS.UNAVAILABLE) {
      return false;
    }
    return true;
  } catch (err) {
    return false;
  }
};

export const checkPhoto = async (callback?: () => void) => {
  try {
    const checkPermission = await check(
      Platform.OS === 'ios'
        ? PERMISSIONS.IOS.PHOTO_LIBRARY
        : PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE,
    );
    if (checkPermission === RESULTS.BLOCKED) {
      callback?.();
      return false;
    }
    if (checkPermission === RESULTS.DENIED) {
      const result = await request(
        Platform.OS === 'ios'
          ? PERMISSIONS.IOS.PHOTO_LIBRARY
          : PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE,
      );
      return result === RESULTS.GRANTED;
    }
    if (checkPermission === RESULTS.UNAVAILABLE) {
      return false;
    }
    return true;
  } catch (err) {
    return false;
  }
};

export const checkAudio = async () => {
  try {
    const checkPermission = await check(
      Platform.OS === 'ios'
        ? PERMISSIONS.IOS.MICROPHONE
        : PERMISSIONS.ANDROID.RECORD_AUDIO,
    );
    if (checkPermission === RESULTS.BLOCKED) {
      return false;
    }
    if (checkPermission === RESULTS.DENIED) {
      const result = await request(
        Platform.OS === 'ios'
          ? PERMISSIONS.IOS.MICROPHONE
          : PERMISSIONS.ANDROID.RECORD_AUDIO,
      );
      return result === RESULTS.GRANTED;
    }
    if (checkPermission === RESULTS.UNAVAILABLE) {
      return false;
    }
    return true;
  } catch (err) {
    return false;
  }
};

export const checkLocation = async () => {
  try {
    const checkPermission = await check(
      Platform.OS === 'ios'
        ? PERMISSIONS.IOS.LOCATION_WHEN_IN_USE
        : PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
    );
    if (checkPermission === RESULTS.BLOCKED) {
      return false;
    }
    if (checkPermission === RESULTS.DENIED) {
      const result = await request(
        Platform.OS === 'ios'
          ? PERMISSIONS.IOS.LOCATION_WHEN_IN_USE
          : PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
      );
      return result === RESULTS.GRANTED;
    }
    if (checkPermission === RESULTS.UNAVAILABLE) {
      return false;
    }
    return true;
  } catch (err) {
    return false;
  }
};
