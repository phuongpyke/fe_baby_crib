import BleManager from 'react-native-ble-manager';
import {Buffer} from 'buffer';
import {bytesToString} from 'convert-string';
import reactotron from 'reactotron-react-native';

// Read
export const read = async (peripheral, service, characteristic) => {
  let status = await BleManager.read(peripheral, service, characteristic)
    .then(readData => {
      // console.log('read DATA', readData);
      const data = Buffer.from(readData).toString();
      return data;
    })
    .catch(error => {
      reactotron.log('Err read: ', error);
      return false;
    });
  return status;
};

// Write
export const write = async (peripheral, service, characteristic, data) => {
  let status = BleManager.write(peripheral, service, characteristic, data)
    .then(() => {
      reactotron.log('Write: ' + data);
      return true;
    })
    .catch(error => {
      reactotron.log('Err write: ', error);
      return false;
    });
  return status;
};

export const writeWithoutResponse = async (
  peripheral,
  service,
  characteristic,
  data,
) => {
  let status = BleManager.writeWithoutResponse(
    peripheral,
    service,
    characteristic,
    data,
  )
    .then(() => {
      reactotron.log('Write: ' + data);
      return true;
    })
    .catch(error => {
      reactotron.log('Err write: ', error);
      return false;
    });
  return status;
};

// Notify
export const notify = async (peripheral, service, characteristic) => {
  // Connect to device
  await BleManager.connect(peripheral);
  // Before startNotification you need to call retrieveServices
  await BleManager.retrieveServices(peripheral);
  // To enable BleManagerDidUpdateValueForCharacteristic listener
  await BleManager.startNotification(peripheral, service, characteristic);
  // Add event listener
  bleManagerEmitter.addListener(
    'BleManagerDidUpdateValueForCharacteristic',
    ({value, peripheral, characteristic, service}) => {
      // Convert bytes array to string
      const data = bytesToString(value);
      console.log(`Recieved ${data} for characteristic ${characteristic}`);
    },
  );
  // Actions triggereng BleManagerDidUpdateValueForCharacteristic event
};
