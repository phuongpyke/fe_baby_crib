const require = "必要な情報をすべて記入してください。"
const passwordNotMatch = "確認パスワードが一致しません。"
const agreeWithPolicy = "以下の条件を読み、同意します。"
const invalidEmail = "入力したメールアドレスが正しい形式ではありません。"
const noData = "表示するデータがありません。"
const noNetwork = "インターネットの接続がありません。"
const requireLogin = 'この機能を実行するには、ログインしてください。'
const requireMessage = 'メッセージのテキストを入力してください。'
const updateSuccess = '更新に成功。'
const osMessage = 'お使いの携帯電話の正しいオペレーティングシステムを選択してください。'
const lengthPassword = "※半角英数字 8文字以上で入力してください。"
export const message = {
    require,
    passwordNotMatch,
    agreeWithPolicy,
    invalidEmail,
    noData,
    noNetwork,
    requireLogin,
    requireMessage,
    updateSuccess,
    osMessage,
    lengthPassword
} 