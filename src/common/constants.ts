import {Dimensions, Platform} from 'react-native';
import {vs} from 'react-native-size-matters';
import StaticSafeAreaInsets from 'react-native-static-safe-area-insets';

export const CODE_SUCCESS = 200;

export const WIDTH_DEVICE = Dimensions.get('screen').width;
export const HEIGHT_DEVICE = Dimensions.get('screen').height;

export const FULL_WIDTH_DEVICE = Dimensions.get('window').width;
export const FULL_HEIGHT_DEVICE = Dimensions.get('window').height;

export const BOTTOM_PADDING =
  StaticSafeAreaInsets.safeAreaInsetsBottom > 30 && Platform.OS === 'ios'
    ? StaticSafeAreaInsets.safeAreaInsetsBottom
    : vs(20);

export const TOP_PADDING =
  StaticSafeAreaInsets.safeAreaInsetsTop > 30
    ? StaticSafeAreaInsets.safeAreaInsetsTop
    : vs(20);

export const GOOGLE_API_KEY: string = 'AIzaSyAussOeCXc8pP0I5tSEvGq93HTQFIlR3Js';

export const ONE_SIGNAL_STG: string = 'eaba189f-ad20-4f23-a009-771259767629';
export const ONE_SIGNAL_PROD: string = '03144e2a-3fc0-489a-af3b-4ca0342c1b5a';
