export type FontsFamilyType = {
  primaryRegular: string;
  primaryMedium: string;
  primarySemiBold: string;
  primaryBold: string;
  primaryExtraBold: string;
};

export const FontFamilyDefault: FontsFamilyType = {
  primaryRegular: 'Baloo2-Regular',
  primaryMedium: 'Baloo2-Medium',
  primarySemiBold: 'Baloo2-SemiBold',
  primaryBold: 'Baloo2-Bold',
  primaryExtraBold: 'Baloo2-ExtraBold',
};
