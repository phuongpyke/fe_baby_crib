export type ColorsType = {
  bgBody: string;
  bgPrimary: string;

  black: string;
  black2: string;
  white: string;
  white1: string;
  gray: string;
  grayDark: string;
  purple: string;
  yellow: string;
  green: string;
  green2: string;
  orange: string;
  red: string;
  pink: string;
  pink2: string;
  pink3: string;

  card1GradientStart: string;
  card1GradientEnd: string;
  card2GradientStart: string;
  card2GradientEnd: string;
  card3GradientStart: string;
  card3GradientEnd: string;

  btnPrimary: string;
  btnSecondary1: string;
  btnSecondary2: string;
  btnSmall1: string;
  btnSmall2: string;
  btnGradientStart: string;
  btnGradientEnd: string;

  textPrimary: string;
  textWhite: string;
  textGrayLight: string;
  textGrayDark: string;
  textGreen: string;
  textRed: string;
  textOrange: string;
  textPink: string;
  textPlaceHolder: string;

  border: string;
  gray1: string;
  gray2: string;
  gray3: string;
  gray4: string;
  blue1: string;
  yellow1: string;
  yellow2: string;
  yellow3: string;
  blue2: string;
  blue3: string;
  blue4: string;
  blue7: string;
  gray5: string;
  red1: string;
  blue5: string;
  blue6: string;
  gray6: string;
  yellow4: string;
  yellow5: string;
  gray7: string;
  gray8: string;
  black3: string;
  purple1: string;
  orange1: string;
  orange2: string;
  transparent: string;
};

export const ColorsDefault: ColorsType = {
  bgBody: '#F1F2F7',
  bgPrimary: '#062759',
  black: '#000',
  black2: '#404040',
  black3: '#222222',
  white: '#fff',
  white1: '#FCFCFD',
  gray: '#CDCEE0',
  grayDark: '#777E91',
  purple: '#7666FF',
  yellow: '#FFDE00',
  green: '#039E2F',
  green2: '#02BD63',

  orange: '#FD601D',
  transparent: 'transparent',

  //red
  red: '#E10000',
  red1: '#EA4A4A',

  pink: '#FC3271',
  pink2: '#FFF3F3',
  pink3: '#EFD5FF',

  card1GradientStart: '#292946',
  card1GradientEnd: '#39395F',
  card2GradientStart: '#4A23EB',
  card2GradientEnd: '#7666FF',
  card3GradientStart: '#FFDE00',
  card3GradientEnd: '#FD5900',

  btnPrimary: '#1370E3',
  btnSecondary1: 'rgba(118, 102, 255, 0.2)',
  btnSecondary2: 'rgba(255, 222, 0, 0.2)',
  btnSmall1: 'rgba(3, 158, 47, 0.2)',
  btnSmall2: 'rgba(253, 89, 0, 0.2)',
  btnGradientStart: '#FFDE00',
  btnGradientEnd: '#FD5900',

  textPrimary: '#000',
  textWhite: '#FCFCFD',
  textGrayLight: '#E6E8EC',
  textGrayDark: '#F1F2F7',
  textGreen: '#039E2F',
  textRed: '#FC3232',
  textOrange: '#FD5900',
  textPink: '#FC3271',
  textPlaceHolder: '#AAAAAA',

  border: '#DBDBDB',

  //gray
  gray1: '#F5F5F5',
  gray2: '#CCCCCC',
  gray3: '#828282',
  gray4: '#ABB3BB',
  gray5: '#BDBDBD',
  gray6: '#F0EDED',
  gray7: '#EFEDED',
  gray8: '#C4C4C4',
  //blue
  blue1: '#3E64FF',
  blue2: '#EAF1FF',
  blue3: '#0788FE',
  blue4: '#0F2F4450',
  blue5: '#EAF1FF',
  blue6: '#EEEFD1',
  blue7: '#EAFAFF',
  //yellow
  yellow1: '#FFC700',
  yellow2: '#FFCD00',
  yellow3: '#FFCC04',
  yellow4: '#F4954F',
  yellow5: '#F7B500',
  //red
  purple1: '#E8ECFF',

  //orange
  orange1: '#FD601D',
  orange2: '#FD6B18',
};
