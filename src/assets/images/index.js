const images = {
  logo: require('./source/logo.png'),
  hide: require('./source/hidden.png'),
  show: require('./source/show.png'),
  backLeft: require('./source/left-arrow2.png'),
  checkbox: require('./source/checkbox.png'),
  checkboxActive: require('./source/checkbox-active.png'),
  checkboxInactive: require('./source/checkbox-inactive.png'),
  injection: require('./source/injection.png'),
  patient: require('./source/patient.png'),
  lock: require('./source/lock.png'),
  wifi: require('./source/wifi.png'),
  heightBaby: require('./source/height-baby.png'),
  weightBaby: require('./source/weight-baby.png'),
  calendar: require('./source/calendar.png'),
  calendar2: require('./source/calendar2.png'),
  calendar3: require('./source/ic_calendar.png'),
  edit: require('./source/edit.png'),
  diapers1: require('./source/diapers1.png'),
  comment: require('./source/comment.png'),
  favorite: require('./source/favorite.png'),
  avatarCommet: require('./source/avatar-comment.png'),
  milk: require('./source/milk.png'),
  rightArrow: require('./source/right-arrow.png'),
  bottomArrow: require('./source/bottom-arrow.png'),
  babyNotification: require('./source/baby-notification.png'),
  avatar2: require('./source/avatar2.png'),
  milkYellow: require('./source/milk-yellow.png'),
  temperature: require('./source/temperature.png'),
  thermometer: require('./source/thermometer.png'),
  connect: require('./source/connect.png'),
  cough: require('./source/cough.png'),
  robot: require('./source/robot.png'),
  babyTime: require('./source/baby_time.png'),
  overallCover: require('./source/overall_cover.png'),
  cry_status: require('./source/cry_status.png'),
  doctorAi: require('./source/doctor_ai.png'),
  crying: require('./source/crying.png'),
  babyTricks: require('./source/baby_tricks.png'),
  clock: require('./source/clock.png'),
  diapers: require('./source/diapers.png'),
  medical: require('./source/medical.png'),
  babyBoy1: require('./source/baby-boy-1.png'),
  babyBoy2: require('./source/baby-boy-2.png'),
  babyBoy3: require('./source/baby-boy-3.png'),
  headerLogin: require('./source/header-login.png'),
  headerRegister: require('./source/header-register.png'),
  footerRegister: require('./source/footer-register.png'),
  headerForgotPassword: require('./source/header-forgot-password.png'),
  backgroundDiapers: require('./source/background-diapers.png'),
  diaper: require('./source/diaper.png'),
  headerDashBoard: require('./source/header-dashboard.png'),
  imageDate: require('./source/image-date.png'),
  imageName: require('./source/image-name.png'),
  imageStage: require('./source/image-stage.png'),
  poster: require('./source/poster.png'),
  headerTakeCare: require('./source/header-take-care.png'),
  dashBoard1: require('./source/dashBoard1.png'),
  dashBoard2: require('./source/dashBoard2.png'),
  dashBoard3: require('./source/dashBoard3.png'),
  dashBoard4: require('./source/dashBoard4.png'),
  dashBoard5: require('./source/dashBoard5.png'),
  dashBoard6: require('./source/dashBoard6.png'),
  dashBoard7: require('./source/dashBoard7.png'),
  headerSetting: require('./source/header-setting.png'),
  headerVaccin: require('./source/header-vaccin.png'),
  addBaby: require('./source/add-baby.png'),
  editSchedule: require('./source/ic_edit_schedule.png'),
  addSchedule: require('./source/ic_add_schedule.png'),
  backgroundMilk: require('./source/img_bg_milk.png'),
  backgroundMilk2: require('./source/img_bg_milk2.png'),
  backgroundMilk3: require('./source/img_bg_milk3.png'),
  rateStar: require('./source/ic_rate_star.png'),
  babyMale: require('./source/ic_baby_male.png'),
  babyFemale: require('./source/ic_baby_female.png'),
  camera: require('./source/ic_camera.png'),
  check: require('./source/ic_check.png'),
  close: require('./source/ic_close.png'),
  backgroundStoreEasy: require('./source/img_bg_store_easy.png'),
  babyCare1: require('./source/baby_care_1.png'),
  babyCare2: require('./source/baby_care_2.png'),
  babyCare3: require('./source/baby_care_3.png'),
  babyCare4: require('./source/baby_care_4.png'),
  babyCare5: require('./source/baby_care_5.png'),
  babyCare6: require('./source/baby_care_6.png'),
  celsius: require('./source/ic_celsius.png'),
  sun: require('./source/ic_sun.png'),
  imgScanBar: require('./source/img_scan_bar.png'),
  flashOn: require('./source/ic_flash_on.png'),
  flashOff: require('./source/ic_flash_off.png'),
  header1: require('./source/img_header1.png'),
  quaver: require('./source/ic_quaver.png'),
  toy: require('./source/ic_toy.png'),
  wave: require('./source/ic_wave.png'),
  wifiInfo: require('./source/ic_wifi_info.png'),
  tab: {
    home: require('./source/ic_tab_home.png'),
    crib: require('./source/ic_tab_crib.png'),
    notify: require('./source/ic_tab_notify.png'),
    setting: require('./source/ic_tab_setting.png'),
  },
};

export default images;
