import {call, put, takeEvery} from 'redux-saga/effects';
import {loginFailed, loginSucceeded} from './slice';
import {login} from '/modules/auth/slice';
import {loginAppAPI} from '/services/api';
import {TResponse} from '/services/type';

type PayloadTypes = {
  type: string;
  payload: {
    data: {
      email: string;
      password: string;
    };
    onError?: any;
    onSuccess?: any;
  };
};
function* loginSideEffect({payload}: PayloadTypes) {
  try {
    const response: TResponse = yield call(loginAppAPI, payload.data);
    if (response.success) {
      yield put(loginSucceeded(response));
      if (payload.onSuccess) {
        yield call(payload.onSuccess, response);
      }
    } else {
      yield put(loginFailed());
      if (payload.onError) {
        yield call(payload.onError, response);
      }
    }
  } catch (error) {}
}

export default function* authSaga() {
  yield takeEvery(login.type, loginSideEffect);
}
