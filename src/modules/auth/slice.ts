import {createSlice} from '@reduxjs/toolkit';
import {persistReducer} from 'redux-persist';
import {generatePersistConfig} from '/utils/helper';

interface IState {
  token: string;
  loginLoading: boolean;
  loginError: string;
  isPassReview: boolean;
}

const initialState: IState = {
  token: '',
  loginLoading: false,
  loginError: '',
  isPassReview: false,
};

const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    login: state => {
      state.loginLoading = true;
    },
    loginSucceeded: (state, action: any) => {
      state.loginLoading = false;
      state.token = action.payload.access_token;
    },
    loginFailed: state => {
      state.loginLoading = false;
    },
    logout: state => {
      state.token = '';
    },
    setTokenSlice: (state, action: any) => {
      state.token = action.payload;
    },
  },
});

export const onLogin = (data: any) => {
  return {type: login.type, payload: data};
};

const persistConfig = generatePersistConfig('auth', ['token', 'isPassReview']);

export const {login, loginSucceeded, loginFailed, logout, setTokenSlice} =
  authSlice.actions;
export default persistReducer<IState>(persistConfig, authSlice.reducer);
