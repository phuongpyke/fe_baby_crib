import {createSlice} from '@reduxjs/toolkit';
import {persistReducer} from 'redux-persist';
import {generatePersistConfig} from '/utils/helper';

interface IState {
  labels: any[];
}

const initialState: IState = {
  labels: [],
};

const scheduleSlice = createSlice({
  name: 'schedule',
  initialState,
  reducers: {
    setLabels: (state, {payload}) => {
      state.labels = payload;
    },
  },
});

const persistConfig = generatePersistConfig('schedule', []);

export const scheduleActions = scheduleSlice.actions;
export default persistReducer<IState>(persistConfig, scheduleSlice.reducer);
