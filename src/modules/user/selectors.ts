import {createSelector} from 'reselect';

const selector = (state: any) => state.user;

export const babyListSelector = createSelector(
  selector,
  userReducer => userReducer.babyList,
);

export const loadingGetBabyListSelector = createSelector(
  selector,
  userReducer => userReducer.isLoadingGetBaByList,
);

export const cribQrSelector = createSelector(
  selector,
  userReducer => userReducer.cribQr,
);

export const blueToothIdSelector = createSelector(
  selector,
  userReducer => userReducer.bluetoothId,
);
