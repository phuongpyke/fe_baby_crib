import reactotron from 'reactotron-react-native';
import {getBabyListAPI} from '/services/api';
import {put, call, takeEvery} from 'redux-saga/effects';
import {getBaByListSuccess, getBaByListFailed, getBaByList} from './slice';
import {TResponse} from '/services/type';

type PayloadTypes = {
  type: string;
  payload: {
    data?: any;
    onError?: any;
    onSuccess?: any;
  };
};

function* getBabyListEffect({payload}: PayloadTypes) {
  try {
    const response: TResponse = yield call(getBabyListAPI);
    if (response.success) {
      yield put(getBaByListSuccess(response));
      if (payload.onSuccess) yield call(payload.onSuccess, response);
    } else {
      yield put(getBaByListFailed());
      if (payload.onError) yield call(payload.onError, response);
    }
  } catch (error) {}
}

export default function* userInfoSaga() {
  yield takeEvery(getBaByList.type, getBabyListEffect);
}
