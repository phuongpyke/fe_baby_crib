import {createSlice} from '@reduxjs/toolkit';
import {persistReducer} from 'redux-persist';
import {generatePersistConfig} from '/utils/helper';

interface IState {
  isLoading: boolean;
  errorMessage: string;
  babyList: any[];
  isLoadingGetBaByList: boolean;
  cribQr: string | null;
  bluetoothId: string | null;
  bleConnect: any[];
}

const initialState: IState = {
  isLoading: false,
  errorMessage: '',
  babyList: [],
  isLoadingGetBaByList: false,
  cribQr: null,
  bluetoothId: null,
  bleConnect: [],
};

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    getBaByList: state => {
      state.isLoadingGetBaByList = true;
    },
    getBaByListSuccess: (state, action: any) => {
      state.babyList = action.payload.data;
      state.isLoadingGetBaByList = false;
    },
    getBaByListFailed: state => {
      state.isLoadingGetBaByList = false;
    },
    setcribQr: (state, action: any) => {
      state.cribQr = action.payload.data;
    },
    setBlueToothId: (state, action: any) => {
      state.bluetoothId = action.payload.data;
    },
    setBleConnect: (state, action) => {
      state.bleConnect = action.payload;
    },
  },
});

export const onSetCrib = (data?: any) => {
  return {type: setcribQr.type, payload: data};
};

export const onSetBlueToothId = (data?: any) => {
  return {type: setBlueToothId.type, payload: data};
};

const persistConfig = generatePersistConfig('userSlice', [
  'babyList',
  'bleConnect',
]);

export const {
  getBaByList,
  getBaByListSuccess,
  getBaByListFailed,
  setcribQr,
  setBlueToothId,
  setBleConnect,
} = userSlice.actions;

export default persistReducer<IState>(persistConfig, userSlice.reducer);
