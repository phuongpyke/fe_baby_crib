import request from '/api/request';

export const getAllProductOfCategory = (params: {id: string | number}) =>
  request.get(`products/category/${params.id}`);

export const rateProduct = (params: {
  product_id: string | number;
  rating: number;
  comment: string;
}) => request.post('reviews/submit', params);
