import request from '/api/request';

export const createSchedule = (params: {
  baby_id: string | number;
  label: string | number;
  content?: string;
  start: string;
  stop: string;
}) => request.post('schedule', params);

export const getScheduleDetail = (params: {
  babyId: string | number;
  scheduleId: string | number;
}) => request.get(`schedule/${params.babyId}/${params.scheduleId}`);

export const getScheduleSettingTime = (params: {
  baby_id: string | number;
  increase: number;
  value: string;
}) => request.post('schedule/setting-time', params);

export const updateSchedule = (
  params: {
    baby_id: string | number;
    label: string | number;
    content?: string;
    start: string;
    stop: string;
  },
  scheduleId: string | number,
) => request.put(`schedule/${scheduleId}`, params);
