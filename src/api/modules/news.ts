import request from '/api/request';

export const getAllLabels = () => request.get('news/category');

export const getAllNews = (page: number) => request.get(`news?page=${page}`);
