import request from '/api/request';

export const getAllVaccines = (id: string | number) =>
  request.get(`vaccines?baby_id=${id}`);

export const getVaccineSchedule = () => request.get('vaccines/schedule');

export const getVaccineDetails = (id: string | number) =>
  request.get(`vaccines/${id}`);

export const updateVaccine = (params: {
  baby_id: string | number;
  vaccine_id: string | number;
  number_index: number;
  is_inject: number;
  date: string;
}) => request.put('vaccines/update', params);
