import request from '/api/request';

export const getProfile = (token?: string) =>
  request.get(
    'profile',
    token ? {headers: {Authorization: `Bearer ${token}`}} : {},
  );
export const login = (params: any) => request.post('auth/login', params);
export const register = (params: any) => request.post('auth/register', params);

export const checkPhoneExist = (params: {phone: string}) =>
  request.post('auth/check-phone-exist', params);
