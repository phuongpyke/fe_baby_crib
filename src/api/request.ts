import axios from 'axios';
import Config from 'react-native-config';
// import TokenProvider from '@utils/auth/TokenProvider';
// import AuthenticateService from '@utils/auth/AuthenticateService';
// import {logger} from 'utils/helper';
import NetInfo from '@react-native-community/netinfo';
import store from '/redux/configStore';
// import {apiLogger} from '@utils/helper';
// import i18next from 'utils/i18next';

const AUTH_URL_REFRESH_TOKEN = `${Config.API_URL}auth/refresh-token`;
let hasAnyNetworkDialogShown = false;

const request = axios.create({
  // baseURL: Config.API_URL,
  baseURL: 'https://babyai.lamwebnhanh.net/api/v2/',
  timeout: 8000,
  headers: {
    Accept: '*/*',
    'X-Requested-With': 'XMLHttpRequest',
    'Content-Type': 'application/json',
  },
});
// for multiple requests
let isRefreshing = false;
let failedQueue: any = [];

const processQueue = (error: any, token: string | null | undefined = null) => {
  failedQueue.forEach((prom: any) => {
    if (error) {
      prom.reject(error);
    } else {
      prom.resolve(token);
    }
  });

  failedQueue = [];
};

const rejectError = (err: string, validNetwork: boolean) => {
  // Avoid being null
  if (validNetwork !== false) {
    // return Promise.reject(i18next.t(err));
    return Promise.reject(err);
  }
  // return Promise.reject(i18next.t('ERROR NETWORK'));
  return Promise.reject('ERROR NETWORK');
};

request.interceptors.request.use(
  async (config: any) => {
    // Do something before API is sent
    const token = store.getState()?.auth?.token;
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  },
  (error: any) => {
    // Do something with API error
    // apiLogger(
    //   `%c FAILED ${error.response.method?.toUpperCase()} from ${
    //     error.response.config.url
    //   }:`,
    //   'background: red; color: #fff',
    //   error.response,
    // );
    return Promise.reject(error);
  },
);

request.interceptors.response.use(
  (response: any) => response.data,
  async (error: any) => {
    // Check network first
    const network: any = await NetInfo.fetch();
    const validNetwork = network.isInternetReachable && network.isConnected;
    // validNetwork on first render in iOS will return NULL
    if (validNetwork === false && !hasAnyNetworkDialogShown) {
      hasAnyNetworkDialogShown = true;
      //   renderAlert(i18next.t(ERRORS.network), () => {
      //     hasAnyNetworkDialogShown = false;
      //   });
    }
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    const {response} = error ?? {};
    const {data} = response ?? {};
    const {errorMessage, errorKey} = data ?? {};
    // apiLogger(
    //   `%c FAILED ${error.config?.method?.toUpperCase()} from ${
    //     error?.config?.url
    //   }:`,
    //   'background: red; color: #fff',
    //   error.response,
    // );
    const originalRequest = error.config;
    if (errorMessage === 'RefreshToken_NotExist') {
      // logger('RefreshToken_NotExist => logout');
      // Logout here
      // AuthenticateService.logOut();
      return rejectError(error, validNetwork);
    }
    if (
      ((error.response && error.response.status === 401) ||
        errorMessage === 'Token_Expire') &&
      !originalRequest.retry
    ) {
    }
    error.message = errorMessage;
    error.keyMessage = errorKey || '';
    return Promise.reject(data);
  },
);

export default request;
