import React, {useState} from 'react';
import {Image} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {ScaledSheet} from 'react-native-size-matters';
import {SceneMap, TabBar, TabView} from 'react-native-tab-view';
import TabFollow from './TabFollow';
import TabSettingCrib from './TabSettingCrib';
import {ColorsDefault, FontFamilyDefault, FontSizeDefault} from '/assets';
import images from '/assets/images';
import {FULL_WIDTH_DEVICE} from '/common';
import {Header} from '/components/common';

const renderScene = SceneMap({
  first: () => <TabSettingCrib />,
  second: () => <TabFollow />,
});

const BabyCrib = () => {
  const [index, setIndex] = useState(0);
  const [routes] = useState([
    {key: 'first', title: 'Cài đặt nôi'},
    {key: 'second', title: 'Theo dõi trong ngày'},
  ]);

  return (
    <SafeAreaView style={styles.container} edges={['right', 'left', 'top']}>
      <Header
        title="Nôi của bé"
        isBack={false}
        customStyle={styles.viewHeader}
        titleStyle={styles.textHeader}
      />
      <Image
        source={images.header1}
        style={styles.bgHeader}
        resizeMode={'stretch'}
      />
      <TabView
        navigationState={{index, routes}}
        renderScene={renderScene}
        onIndexChange={setIndex}
        lazy
        initialLayout={{width: FULL_WIDTH_DEVICE}}
        renderTabBar={props => (
          <TabBar
            style={[styles.tabBar]}
            labelStyle={styles.label}
            inactiveColor={ColorsDefault.gray3}
            activeColor={ColorsDefault.blue1}
            indicatorStyle={[styles.indicatorTabBar]}
            contentContainerStyle={[styles.contentContainerTabBar]}
            tabStyle={styles.tabStyle}
            {...props}
          />
        )}
      />
    </SafeAreaView>
  );
};

const styles = ScaledSheet.create({
  container: {
    backgroundColor: ColorsDefault.white,
    flex: 1,
  },
  viewHeader: {
    borderBottomWidth: 1,
    borderBottomColor: ColorsDefault.white,
  },
  textHeader: {
    fontSize: FontSizeDefault.FONT_24,
    fontFamily: FontFamilyDefault.primaryBold,
  },
  bgHeader: {
    width: FULL_WIDTH_DEVICE * 0.5,
    height: FULL_WIDTH_DEVICE * 0.37,
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: -100,
  },
  tabBar: {
    backgroundColor: ColorsDefault.transparent,
  },
  indicatorTabBar: {
    height: 0,
  },
  label: {
    textTransform: 'none',
    fontFamily: FontFamilyDefault.primaryMedium,
    fontSize: FontSizeDefault.FONT_14,
  },
  tabStyle: {
    width: (FULL_WIDTH_DEVICE - 64) / 2,
    borderRadius: 100,
    backgroundColor: ColorsDefault.white,
    shadowColor: ColorsDefault.black,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.1,
    shadowRadius: 3,
    elevation: 2,
  },
  contentContainerTabBar: {
    backgroundColor: ColorsDefault.transparent,
    justifyContent: 'space-between',
    paddingHorizontal: 24,
    paddingVertical: 8,
  },
});

export default BabyCrib;
