import React, {useState} from 'react';
import {Image, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {ColorsDefault, FontFamilyDefault, FontSizeDefault} from '/assets';
import images from '/assets/images';
import {FULL_WIDTH_DEVICE} from '/common';

const BUTTON = [
  {title: 'Nhiệt độ', index: 0},
  {title: 'Khóc', index: 1},
  {title: 'Ho', index: 2},
];

const LIST = [
  {title: 'Bé bị sốt', temperature: '39.5', time: '10:00am', id: 0},
  {
    title: 'Bé bình thường',
    temperature: '36.5',
    time: '10:00am - 15:00pm',
    id: 1,
  },
  {title: 'Bé có biểu hiện sốt', temperature: '38.1', time: '15:15pm', id: 2},
];

const TabFollow = () => {
  const [currentIndex, setCurrentIndex] = useState<number>(0);

  const onPressTab = (index: number) => {
    setCurrentIndex(index);
  };

  return (
    <View style={{flex: 1}}>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          marginTop: 8,
        }}>
        {BUTTON.map((item: any) => (
          <TouchableOpacity
            style={styles.btn}
            key={item.index}
            onPress={() => onPressTab(item.index)}>
            <Text
              style={[
                currentIndex === item.index
                  ? styles.textFocused
                  : styles.textUnfocused,
              ]}>
              {item.title}
            </Text>
          </TouchableOpacity>
        ))}
      </View>
      <ScrollView style={{flex: 1}} showsVerticalScrollIndicator={false}>
        {LIST.map(item => (
          <View key={item.id} style={styles.viewItem}>
            <View style={{flex: 1}}>
              <Text
                style={{
                  fontSize: FontSizeDefault.FONT_16,
                  fontFamily: FontFamilyDefault.primaryMedium,
                  color: ColorsDefault.black3,
                }}>
                {item.title}
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginTop: 8,
                }}>
                <Image
                  source={images.thermometer}
                  style={{width: 24, height: 24}}
                  resizeMode={'contain'}
                />
                <View style={{flexDirection: 'row', marginLeft: 4}}>
                  <Text
                    style={{
                      fontSize: FontSizeDefault.FONT_16,
                      fontFamily: FontFamilyDefault.primaryMedium,
                      color: ColorsDefault.black3,
                      lineHeight: FontSizeDefault.FONT_24,
                    }}>
                    {item.temperature}
                  </Text>
                  <Text
                    style={{
                      fontSize: FontSizeDefault.FONT_10,
                      paddingBottom: FontSizeDefault.FONT_10,
                    }}>
                    {'o'}
                  </Text>
                  <Text
                    style={{
                      fontSize: FontSizeDefault.FONT_16,
                      fontFamily: FontFamilyDefault.primaryMedium,
                      color: ColorsDefault.black3,
                      lineHeight: FontSizeDefault.FONT_24,
                    }}>
                    {'C'}
                  </Text>
                </View>
              </View>
            </View>
            <Text
              style={{
                fontSize: FontSizeDefault.FONT_16,
                fontFamily: FontFamilyDefault.primaryMedium,
                color: ColorsDefault.black3,
              }}>
              {item.time}
            </Text>
          </View>
        ))}
      </ScrollView>
    </View>
  );
};

const styles = ScaledSheet.create({
  textFocused: {
    fontSize: FontSizeDefault.FONT_16,
    fontFamily: FontFamilyDefault.primaryMedium,
    color: ColorsDefault.black3,
  },
  textUnfocused: {
    fontSize: FontSizeDefault.FONT_16,
    fontFamily: FontFamilyDefault.primaryRegular,
    color: ColorsDefault.gray3,
  },
  btn: {
    height: 40,
    width: FULL_WIDTH_DEVICE / 3,
    alignItems: 'center',
    justifyContent: 'center',
  },
  viewItem: {
    backgroundColor: ColorsDefault.pink2,
    borderRadius: 12,
    marginHorizontal: 16,
    paddingVertical: 12,
    paddingHorizontal: 20,
    marginTop: 16,
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export default TabFollow;
