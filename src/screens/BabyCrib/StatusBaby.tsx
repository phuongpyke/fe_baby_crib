import React, {useState} from 'react';
import {Image, Text, View} from 'react-native';
import {ColorsDefault, FontFamilyDefault, FontSizeDefault} from '/assets';
import images from '/assets/images';

interface Props {}

const StatusBaby = (props: Props) => {
  const {} = props;
  return (
    <View
      style={{
        backgroundColor: '#FFE8E8',
        borderRadius: 16,
        marginHorizontal: 16,
        paddingHorizontal: 12,
        paddingVertical: 10,
        marginTop: 16,
        flexDirection: 'row',
        alignItems: 'center',
      }}>
      <Image
        source={images.crying}
        style={{width: 48, height: 48}}
        resizeMode={'contain'}
      />
      <View style={{flex: 1, justifyContent: 'center', marginHorizontal: 12}}>
        <Text
          style={{
            fontSize: FontSizeDefault.FONT_16,
            fontFamily: FontFamilyDefault.primaryBold,
            color: '#EB5757',
          }}>
          {'Bé đang khóc nhè'}
        </Text>
      </View>
      <View
        style={{
          height: 60,
          width: 60,
          borderRadius: 8,
          backgroundColor: ColorsDefault.white,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Image
          source={images.thermometer}
          style={{width: 23, height: 23}}
          resizeMode={'contain'}
        />
        <View style={{flexDirection: 'row'}}>
          <Text
            style={{
              fontSize: FontSizeDefault.FONT_16,
              fontFamily: FontFamilyDefault.primaryMedium,
              color: ColorsDefault.black3,
              lineHeight: FontSizeDefault.FONT_24,
            }}>
            {'36'}
          </Text>
          <Text
            style={{
              fontSize: FontSizeDefault.FONT_10,
              paddingBottom: FontSizeDefault.FONT_10,
            }}>
            {'o'}
          </Text>
          <Text
            style={{
              fontSize: FontSizeDefault.FONT_16,
              fontFamily: FontFamilyDefault.primaryMedium,
              color: ColorsDefault.black3,
              lineHeight: FontSizeDefault.FONT_24,
            }}>
            {'C'}
          </Text>
        </View>
      </View>
    </View>
  );
};

export default StatusBaby;
