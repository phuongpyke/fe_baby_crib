import React from 'react';
import isEqual from 'react-fast-compare';
import {
  Image,
  ImageSourcePropType,
  StyleProp,
  Text,
  View,
  ViewStyle,
} from 'react-native';
import {ColorsDefault, FontFamilyDefault, FontSizeDefault} from '/assets';
import {FULL_WIDTH_DEVICE} from '/common';
import {ToggleSwitch} from '/components/common';

interface Props {
  icon: ImageSourcePropType;
  title: string;
  content: string;
  customStyle?: StyleProp<ViewStyle>;
}

const SettingCrib = (props: Props) => {
  const {icon, title, content, customStyle} = props;

  return (
    <View
      style={[
        {
          shadowColor: ColorsDefault.black,
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 0.1,
          shadowRadius: 3,
          elevation: 2,
          width: (FULL_WIDTH_DEVICE - 40) / 2,
          borderRadius: 12,
          backgroundColor: ColorsDefault.white,
          padding: 16,
        },
        customStyle,
      ]}>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}>
        <View
          style={{
            width: 36,
            height: 36,
            borderRadius: 36,
            backgroundColor: ColorsDefault.purple1,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Image
            source={icon}
            style={{width: 20, height: 20}}
            resizeMode={'contain'}
          />
        </View>
        <ToggleSwitch value={true} />
      </View>
      <Text
        style={{
          color: ColorsDefault.black3,
          fontSize: FontSizeDefault.FONT_24,
          fontFamily: FontFamilyDefault.primaryBold,
          marginTop: 8,
        }}>
        {title}
      </Text>
      <Text
        style={{
          color: ColorsDefault.black2,
          fontSize: FontSizeDefault.FONT_16,
          fontFamily: FontFamilyDefault.primaryRegular,
          marginTop: 4,
        }}>
        {content}
      </Text>
    </View>
  );
};

export default React.memo(SettingCrib, isEqual);
