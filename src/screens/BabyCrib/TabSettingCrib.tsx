import React, {useState} from 'react';
import {Image, ScrollView, Text, View} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import SettingCrib from './SettingCrib';
import StatusBaby from './StatusBaby';
import {ColorsDefault, FontFamilyDefault, FontSizeDefault} from '/assets';
import images from '/assets/images';
import {ToggleSwitch} from '/components/common';

const TabSettingCrib = () => {
  return (
    <ScrollView
      showsVerticalScrollIndicator={false}
      bounces={false}
      contentContainerStyle={{paddingBottom: 20}}
      style={{flex: 1}}>
      <View
        style={{
          alignSelf: 'center',
          flexDirection: 'row',
          alignItems: 'center',
          marginTop: 8,
        }}>
        <Text
          style={{
            fontSize: FontSizeDefault.FONT_14,
            fontFamily: FontFamilyDefault.primaryRegular,
            color: ColorsDefault.black3,
          }}>
          {'Tình trạng kết nối nôi:'}
        </Text>
        <Image
          source={images.connect}
          style={{width: 16, height: 16, marginLeft: 8, marginRight: 4}}
          resizeMode={'contain'}
        />
        <Text
          style={{
            fontSize: FontSizeDefault.FONT_14,
            fontFamily: FontFamilyDefault.primaryRegular,
            color: ColorsDefault.green2,
          }}>
          {'Đã kết nối'}
        </Text>
      </View>
      <StatusBaby />
      <View
        style={[{flexDirection: 'row', alignItems: 'center'}, styles.shadow]}>
        <View style={{flex: 1}}>
          <Text
            style={{
              fontSize: FontSizeDefault.FONT_16,
              fontFamily: FontFamilyDefault.primaryRegular,
              color: ColorsDefault.black3,
            }}>
            {'Tự động bật'}
          </Text>
          <Text
            style={{
              fontSize: FontSizeDefault.FONT_12,
              fontFamily: FontFamilyDefault.primaryRegular,
              color: ColorsDefault.gray3,
            }}>
            {
              'Tự động bật nhận diện bé khóc.\nTự động tắt sau 30 phút khi bé khóc.'
            }
          </Text>
        </View>
        <ToggleSwitch value={true} />
      </View>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          paddingHorizontal: 16,
          marginTop: 12,
        }}>
        <SettingCrib icon={images.wave} title="Rung" content="Công suất 8kW" />
        <SettingCrib
          icon={images.quaver}
          title="Âm nhạc"
          content="Đang phát - Mozart"
        />
      </View>
      <SettingCrib
        icon={images.toy}
        title="Đồ chơi"
        content="Đang chạy-10’"
        customStyle={{marginTop: 12, marginLeft: 16}}
      />
    </ScrollView>
  );
};

const styles = ScaledSheet.create({
  shadow: {
    backgroundColor: ColorsDefault.white,
    shadowColor: ColorsDefault.black,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.1,
    shadowRadius: 3,
    elevation: 2,
    borderRadius: 12,
    paddingVertical: 8,
    paddingHorizontal: 16,
    marginHorizontal: 16,
    marginTop: 16,
  },
});

export default TabSettingCrib;
