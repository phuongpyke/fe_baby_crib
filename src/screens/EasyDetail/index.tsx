import {useNavigation} from '@react-navigation/native';
import React, {useState} from 'react';
import {
  FlatList,
  Image,
  LayoutAnimation,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {s, ScaledSheet} from 'react-native-size-matters';
import StaticSafeAreaInsets from 'react-native-static-safe-area-insets';
import {ColorsDefault, FontFamilyDefault, FontSizeDefault} from '/assets';
import images from '/assets/images';
import {BOTTOM_PADDING, FULL_WIDTH_DEVICE} from '/common';
import {findLabelSchedule} from '/utils/helper';

const data = [
  {
    content:
      'Thức dậy, cho bé uống sữa lần 1 cho bé, mỗi lần bú kéo dài 20- 45 phút.',
    id: 2039,
    label: '1',
    time_start: '2022-09-28 07:00:00',
    time_string: '7h-7h45',
  },
  {
    content: 'Thay tả lần 1 và vận động tay chân cho bé.',
    id: 2040,
    label: '2',
    time_start: '2022-09-28 07:45:00',
    time_string: '7h45-8h',
  },
  {
    content:
      'Thức dậy, cho bé uống sữa lần 1 cho bé, mỗi lần bú kéo dài 20- 45 phút.',
    id: 2041,
    label: '1',
    time_start: '2022-09-28 07:00:00',
    time_string: '7h-7h45',
  },
  {
    content: 'Thay tả lần 1 và vận động tay chân cho bé.',
    id: 2042,
    label: '2',
    time_start: '2022-09-28 07:45:00',
    time_string: '7h45-8h',
  },
  {
    content:
      'Thức dậy, cho bé uống sữa lần 1 cho bé, mỗi lần bú kéo dài 20- 45 phút.',
    id: 2043,
    label: '1',
    time_start: '2022-09-28 07:00:00',
    time_string: '7h-7h45',
  },
  {
    content: 'Thay tả lần 1 và vận động tay chân cho bé.',
    id: 2044,
    label: '2',
    time_start: '2022-09-28 07:45:00',
    time_string: '7h45-8h',
  },
];

const content =
  'Độ tuổi áp dụng: Khi bé được khoảng 0-6 tuần: Mẹ có thể bắt đầu áp dụng EASY 3 sau khi bé được khoảng 2 tuần tuổi và có cân nặng trung bình từ 2.9  kg trở lên. Các bé sinh non dưới 28 tuần sẽ cần đạt đủ số cân nặng trung bình.\n\nĂn: Mỗi cữ bú cách nhau khoảng 2.5-3 giờ, bé được ăn ngay sau khi ngủ dậy, bé bú một cữ mất khoảng 20-40 phút.\n\nHoạt động: Sau khi ăn xong bé được ợ hơi kỹ, được thay bỉm và mẹ quan sát tín hiệu để thực hiện trình tự ngủ cho bé.\n\nNgủ: Bé ngủ 4 giấc ngày bao gồm 3 giấc dài 1,5-2h và 1 giấc ngắn cuối ngày từ 30-40 phút.';

const EasyDetail = () => {
  const navigation = useNavigation();

  const [isShowContent, setShowContent] = useState<boolean>(false);

  const goBack = () => {
    navigation.goBack();
  };

  const onChangeStatusContent = () => {
    setShowContent(prevState => !prevState);
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
  };

  const renderItem = ({item}: any) => {
    return (
      <TouchableOpacity style={styles.btnItem} activeOpacity={0.8}>
        <View style={styles.viewLeftItem}>
          <View
            style={{
              width: 96,
              height: 96,
              borderRadius: 12,
              backgroundColor: ColorsDefault.purple1,
              alignItems: 'center',
              justifyContent: 'flex-end',
            }}>
            <Text style={styles.textLeftItem}>
              {findLabelSchedule(item?.label)}
            </Text>
          </View>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              width: 80,
              height: 20,
              backgroundColor: ColorsDefault.gray1,
              borderRadius: 6,
              marginTop: 8,
            }}>
            <Text style={styles.textTimeItem}>{item?.time_string}</Text>
          </View>
        </View>
        <View style={styles.viewRightItem}>
          <Text style={styles.textRightItem}>{item.content}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  const renderHeaderList = () => (
    <View style={styles.wrapHeader}>
      <Image source={images.backgroundStoreEasy} style={styles.bgHeader} />
      <View style={styles.viewHeader}>
        <TouchableOpacity style={styles.btnBack} onPress={goBack}>
          <Image source={images.backLeft} style={styles.icBackHeader} />
        </TouchableOpacity>
        <Text style={styles.textHeader}>{'Giai đoạn'}</Text>
      </View>
      <Text style={styles.textSubTitle}>{'( Từ 6 - 8 tuần)'}</Text>
      <Text style={styles.textEasy}>
        {'E.A.S.Y.'} <Text style={{color: ColorsDefault.yellow5}}>{'3'}</Text>
      </Text>
      {isShowContent && <Text style={styles.textContentHeader}>{content}</Text>}
      <TouchableOpacity
        style={styles.btnShowDetail}
        onPress={onChangeStatusContent}>
        <Text style={styles.textDetail}>
          {isShowContent ? 'Ẩn chi tiết' : 'Xem chi tiết'}
        </Text>
        <Image
          source={images.bottomArrow}
          style={{width: 16, height: 16, tintColor: ColorsDefault.yellow5}}
        />
      </TouchableOpacity>
    </View>
  );

  return (
    <View style={styles.container}>
      <FlatList
        style={styles.list}
        contentContainerStyle={{
          paddingBottom: StaticSafeAreaInsets.safeAreaInsetsBottom + s(88),
        }}
        data={data}
        renderItem={renderItem}
        ListHeaderComponent={renderHeaderList}
        keyExtractor={(item: any) => item.id}
        showsVerticalScrollIndicator={false}
      />
      <TouchableOpacity style={styles.btnChoose} activeOpacity={0.8}>
        <Text style={styles.textChoose}>{'Chọn phương pháp này'}</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    backgroundColor: ColorsDefault.blue1,
    flex: 1,
  },
  wrapHeader: {
    backgroundColor: ColorsDefault.blue1,
    paddingBottom: '20@vs',
    borderBottomRightRadius: 36,
    borderBottomLeftRadius: 36,
  },
  bgHeader: {
    width: FULL_WIDTH_DEVICE,
    position: 'absolute',
    top: StaticSafeAreaInsets.safeAreaInsetsTop,
  },
  viewHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: StaticSafeAreaInsets.safeAreaInsetsTop,
    height: '60@vs',
  },
  textHeader: {
    fontSize: FontSizeDefault.FONT_16,
    fontFamily: FontFamilyDefault.primaryBold,
    color: ColorsDefault.white,
  },
  icBackHeader: {
    height: 24,
    width: 24,
    tintColor: ColorsDefault.white,
  },
  btnBack: {
    position: 'absolute',
    left: '10@s',
  },
  textSubTitle: {
    fontSize: FontSizeDefault.FONT_16,
    fontFamily: FontFamilyDefault.primaryRegular,
    color: ColorsDefault.white,
    textAlign: 'center',
  },
  textEasy: {
    fontSize: FontSizeDefault.FONT_44,
    fontFamily: FontFamilyDefault.primarySemiBold,
    color: ColorsDefault.white,
    textAlign: 'center',
  },
  textContentHeader: {
    fontSize: FontSizeDefault.FONT_16,
    fontFamily: FontFamilyDefault.primaryMedium,
    color: ColorsDefault.white,
    lineHeight: FontSizeDefault.FONT_25,
    marginHorizontal: '16@s',
    marginBottom: '16@vs',
  },
  btnShowDetail: {
    alignSelf: 'center',
    marginTop: '5@vs',
    flexDirection: 'row',
    alignItems: 'center',
  },
  textDetail: {
    fontSize: FontSizeDefault.FONT_16,
    fontFamily: FontFamilyDefault.primaryRegular,
    color: ColorsDefault.yellow5,
    marginRight: '4@s',
  },
  btnItem: {
    flexDirection: 'row',
    marginHorizontal: '12@s',
    padding: '8@s',
    borderRadius: 16,
    backgroundColor: ColorsDefault.white,
    borderWidth: 1,
    borderColor: ColorsDefault.gray1,
    marginTop: '16@vs',
  },
  viewRightItem: {
    flexDirection: 'row',
    flex: 1,
    marginLeft: '12@s',
  },
  viewLeftItem: {
    alignItems: 'center',
  },
  textRightItem: {
    fontSize: FontSizeDefault.FONT_14,
    lineHeight: FontSizeDefault.FONT_22,
    fontFamily: FontFamilyDefault.primaryRegular,
    color: ColorsDefault.black3,
  },
  textLeftItem: {
    fontSize: FontSizeDefault.FONT_14,
    fontFamily: FontFamilyDefault.primaryMedium,
    color: ColorsDefault.black3,
  },
  textTimeItem: {
    fontSize: FontSizeDefault.FONT_12,
    fontFamily: FontFamilyDefault.primaryRegular,
    color: ColorsDefault.black3,
  },
  btnSeeMore: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: '16@vs',
  },
  textSeeMore: {
    fontSize: FontSizeDefault.FONT_16,
    fontFamily: FontFamilyDefault.primaryRegular,
    color: ColorsDefault.black3,
    marginRight: '4@s',
  },
  list: {
    backgroundColor: ColorsDefault.white,
  },
  btnChoose: {
    backgroundColor: ColorsDefault.blue1,
    height: '52@s',
    borderRadius: '52@s',
    justifyContent: 'center',
    alignItems: 'center',
    width: FULL_WIDTH_DEVICE - s(32),
    position: 'absolute',
    bottom: BOTTOM_PADDING,
    alignSelf: 'center',
  },
  textChoose: {
    fontSize: FontSizeDefault.FONT_16,
    fontFamily: FontFamilyDefault.primaryMedium,
    color: ColorsDefault.white,
  },
});

export default EasyDetail;
