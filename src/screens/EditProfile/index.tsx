import {RouteProp, useNavigation} from '@react-navigation/native';
import {FormikProps, useFormik} from 'formik';
import moment from 'moment';
import React, {useEffect, useLayoutEffect, useRef, useState} from 'react';
import {Modal, StyleSheet, TouchableOpacity} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import {vs} from 'react-native-size-matters';
import StaticSafeAreaInsets from 'react-native-static-safe-area-insets';
import Toast from 'react-native-toast-message';
import {useDispatch} from 'react-redux';
import {ItemCheckbox} from './components/ItemCheckbox';
import {ColorsDefault, FontFamilyDefault, FontSizeDefault} from '/assets';
import images from '/assets/images';
import {FULL_WIDTH_DEVICE, WIDTH_DEVICE} from '/common';
import {
  Block,
  Image,
  Input,
  LoadingOverlay,
  Text,
  Touchable,
} from '/components/base';
import {ModalActionSheet, ModalConfirm} from '/components/common';
import {Routes} from '/navigations/Routes';
import {createBaByAPI, updateBaByAPI} from '/services/api';
import {IBabyInfo} from '/services/type';
import {Constants} from '/utils';
import ImageUploaded from '/utils/ImageUploader';
import {checkCamera, checkPhoto} from '/utils/permissions';

const DATE_FORMAT = 'DD/MM/YYYY';

interface Props {
  route: RouteProp<
    {
      params: {
        mode?: 'edit' | 'new';
        userInfo?: IBabyInfo;
      };
    },
    'params'
  >;
}

const followMother = [
  {id: 1, name: 'Sữa mẹ.'},
  {id: 2, name: 'Sữa ngoài.'},
  {id: 3, name: 'Cả 2.'},
];

const gender = [
  {id: 1, name: 'Nam', icon: images.babyMale},
  {id: 2, name: 'Nữ', icon: images.babyFemale},
];
interface MyValues {
  name: string;
  height: string;
  weight: string;
  sick: string;
}

const validate = (values: any) => {
  const errors: any = {};
  if (!values.name) {
    errors.name = 'Hãy nhập họ và tên bé';
  }
  if (!values.height) {
    errors.height = 'Hãy nhập chiều cao của bé';
  }
  if (!values.weight) {
    errors.weight = 'Hãy nhập cân nặng của bé';
  }
  return errors;
};

const EditProfile = ({route}: Props) => {
  const {mode, userInfo} = route.params ?? {};

  const navigation = useNavigation<any>();
  const dispatch = useDispatch();

  const [loading, setLoading] = useState(false);
  const actionSheet = useRef<any>(null);
  const photoRef = useRef<any>(null);

  const formik: FormikProps<MyValues> = useFormik<MyValues>({
    initialValues: {
      name: mode === 'new' ? '' : userInfo?.name,
      height: mode === 'new' ? '' : userInfo?.height.toString(),
      weight:
        mode === 'new' ? '' : userInfo?.weight.toString().replace('.', ','),
      sick: '',
    },
    validate,
    onSubmit: () => {},
  });

  const [data, setData] = useState<any>({
    dob: userInfo?.dod ? moment(userInfo?.dod).format(DATE_FORMAT) : null,
    gender: userInfo?.gender,
    follow_mother: userInfo?.follow_mother,
  });

  const [showErrorDate, setShowErrorDate] = useState(false);

  const [show, setShow] = useState(false);

  const handleConfirm = (date: any) => {
    setData({...data, dob: moment(date).format(DATE_FORMAT)});
    hideDatePicker();
  };

  const showDatePicker = () => {
    setShowErrorDate(true);
    setShow(true);
  };

  const hideDatePicker = () => {
    setShow(false);
  };

  const onSelectActionSheet = async (index: number) => {
    try {
      let localPath: any = '';
      if (index === 1) {
        await checkPhoto(() => {
          photoRef.current?.show();
        });
        localPath = await ImageUploaded.chooseImageFromGallery();
      } else if (index === 2) {
        await checkCamera();
        localPath = await ImageUploaded.chooseImageFromCamera();
      }
    } catch (err) {}
  };

  const showActionSheet = () => {
    actionSheet?.current?.show();
  };

  const onSave = async () => {
    setLoading(true);
    const arr = data?.dob?.split('/');
    const tempDob = moment(
      new Date(parseInt(arr[2]), parseInt(arr[1]) - 1, parseInt(arr[0])),
    ).format('DD/MM/YYYY');

    const params = {
      name: formik.values.name,
      dob: tempDob,
      weight: formik.values.weight.replace(',', '.'),
      height: formik.values.height,
      breast_milk: data.follow_mother,
      gender: data.gender,
      background_disease: formik.values.sick,
    };
    try {
      let response: any;
      if (mode === 'new') {
        response = await createBaByAPI(params);
      } else {
        response = await updateBaByAPI({...params, id: userInfo?.id});
      }
      if (response.success) {
        Toast.show({
          type: 'custom',
          text1:
            mode === 'new'
              ? 'Thêm thông tin bé thành công'
              : 'Cập nhật thành công',
          props: {
            type: 'success',
          },
        });
        navigation.navigate(Routes.Dashboard);
      } else {
        Toast.show({
          type: 'custom',
          text1: `${response.message || 'Có lỗi xảy ra. Vui lòng thử lại!'}`,
          props: {
            type: 'error',
          },
        });
      }
    } catch (error: any) {
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    if (userInfo?.dob) {
      setData({...data, dob: moment(userInfo?.dod).format(DATE_FORMAT)});
    }
  }, []);

  const isValidNew = !(
    !formik.errors.name &&
    !formik.errors.height &&
    !formik.errors.weight &&
    formik.values.name &&
    formik.values.height &&
    formik.values.weight &&
    data.dob &&
    data.gender &&
    data.follow_mother
  );

  const isValidEdit = !(
    !formik.errors.name &&
    !formik.errors.height &&
    !formik.errors.weight &&
    formik.values.name &&
    formik.values.height &&
    formik.values.weight &&
    data.gender &&
    data.dob
  );

  useLayoutEffect(() => {
    navigation.setOptions({
      ...(mode === 'edit' && {
        headerRight: () => (
          <Touchable p={8} disabled={isValidEdit} onPress={onSave}>
            <Text
              type={'h4'}
              fontWeight={'medium'}
              color={isValidEdit ? ColorsDefault.gray3 : ColorsDefault.blue1}>
              {'Lưu'}
            </Text>
          </Touchable>
        ),
      }),
    });
  }, [mode, data, formik]);

  const renderGenderButton = (item: any, index: number) => {
    return (
      <Touchable
        onPress={() => {
          setData({...data, gender: item.id});
        }}
        key={index}
        borderRadius={58}
        borderWidth={1}
        borderColor={
          data.gender === item.id ? ColorsDefault.blue1 : ColorsDefault.gray1
        }
        bg={
          data.gender === item.id ? ColorsDefault.purple1 : ColorsDefault.gray1
        }
        middle
        center
        row
        height={58}
        width={(FULL_WIDTH_DEVICE - 48) / 2}>
        <Image source={item.icon} height={20} width={20} />
        <Text
          style={{
            fontSize: FontSizeDefault.FONT_16,
            fontFamily: FontFamilyDefault.primaryRegular,
            lineHeight: undefined,
          }}
          ml={12}>
          {item.name}
        </Text>
      </Touchable>
    );
  };

  return (
    <KeyboardAwareScrollView
      style={styles.container}
      contentContainerStyle={styles.contentScroll}
      keyboardShouldPersistTaps="handled"
      showsVerticalScrollIndicator={false}
      enableResetScrollToCoords={true}>
      <LoadingOverlay loading={loading} />
      <Touchable
        onPress={showActionSheet}
        mt={24}
        mb={8}
        alignSelfCenter
        middle
        center
        circle={96}
        borderColor={ColorsDefault.blue1}
        borderWidth={2}>
        <Image source={images.babyBoy3} width={60} height={60} />
        <Image
          source={images.camera}
          width={24}
          height={24}
          absolute
          bottom={1}
          right={1}
        />
      </Touchable>
      <Text mt={24} type="p" fontWeight="medium">
        Họ và tên bé
      </Text>
      <Input
        style={{
          borderWidth: 1,
          width: WIDTH_DEVICE - 32,
          borderRadius: 60,
          borderColor: ColorsDefault.gray1,
        }}
        mt={8}
        height={58}
        mb={5}
        value={formik.values.name}
        onChangeText={formik.handleChange('name')}
        placeholder="VD: Nguyễn Thị A"
        error={formik.errors.name && formik.touched.name}
        errorMessage={formik.errors.name}
        onBlur={formik.handleBlur('name')}
      />
      <Text mt={16} type="p" fontWeight="medium">
        Ngày/tháng/năm sinh
      </Text>
      <Touchable
        style={[
          styles.inputDate,
          {
            borderColor:
              !data.dob && showErrorDate
                ? ColorsDefault.red
                : ColorsDefault.gray3,
          },
        ]}
        onPress={showDatePicker}>
        <Text
          type="p"
          color={data.dob ? ColorsDefault.black : ColorsDefault.gray3}>
          {data?.dob || 'dd/mm/yyyy'}
        </Text>
        <Image source={images.calendar3} style={{width: 20, height: 20}} />
      </Touchable>
      {!data.dob && showErrorDate && (
        <Text fontSize={12} color={ColorsDefault.red}>
          Hãy chọn ngày tháng tháng năm sinh
        </Text>
      )}
      <Text mt={16} type="p" fontWeight="medium">
        Cân nặng
      </Text>
      <Input
        style={{
          borderWidth: 1,
          width: WIDTH_DEVICE - 32,
          borderRadius: 60,
          borderColor: ColorsDefault.gray1,
        }}
        mt={8}
        height={58}
        mb={5}
        value={formik.values.weight}
        onChangeText={formik.handleChange('weight')}
        placeholder="VD: 3kg"
        error={formik.errors.weight && formik.touched.weight}
        errorMessage={formik.errors.weight}
        onBlur={formik.handleBlur('weight')}
        keyboardType={'numeric'}
      />
      <Text mt={16} type="p" fontWeight="medium">
        Chiều cao
      </Text>
      <Input
        style={{
          borderWidth: 1,
          width: WIDTH_DEVICE - 32,
          borderRadius: 60,
          fontSize: 16,
          borderColor: ColorsDefault.gray1,
          paddingHorizontal: 24,
        }}
        mt={8}
        height={58}
        mb={5}
        value={formik.values.height}
        onChangeText={formik.handleChange('height')}
        placeholder="VD: 90cm"
        error={formik.errors.height && formik.touched.height}
        errorMessage={formik.errors.height}
        onBlur={formik.handleBlur('height')}
        keyboardType={'number-pad'}
      />
      <Text mt={16} type="p" fontWeight="medium">
        Giới tính
      </Text>
      <Block row justifyBetween middle>
        {gender.map((item, index) => {
          // return (
          //   <ItemCheckbox
          //     selectValue={data.gender}
          //     key={index}
          //     id={item.id}
          //     name={item.name}
          //     onChange={(id: number) => {
          //       setData({...data, gender: id});
          //     }}
          //   />
          // );
          return renderGenderButton(item, index);
        })}
      </Block>
      <Text mt={16} type="p" fontWeight="medium">
        Bệnh nền
      </Text>
      <Input
        style={{
          borderWidth: 1,
          width: WIDTH_DEVICE - 32,
          borderRadius: 60,
          fontSize: 16,
          borderColor: ColorsDefault.gray1,
        }}
        mt={8}
        height={58}
        mb={5}
        value={formik.values.sick}
        onChangeText={formik.handleChange('sick')}
        placeholder="VD: Bé có bị sao không?"
        error={formik.errors.sick && formik.touched.sick}
        errorMessage={formik.errors.sick}
        onBlur={formik.handleBlur('sick')}
        // keyboardType={'number-pad'}
      />
      {mode === 'new' && (
        <Text mt={16} type="p" fontWeight="medium">
          Chăm bé theo?
        </Text>
      )}
      {mode === 'new' &&
        followMother.map((item, index) => {
          return (
            <ItemCheckbox
              selectValue={data.follow_mother}
              key={index}
              id={item.id}
              name={item.name}
              onChange={(id: number) => {
                setData({...data, follow_mother: id});
              }}
            />
          );
        })}
      {mode === 'new' && (
        <TouchableOpacity
          onPress={onSave}
          disabled={isValidNew}
          activeOpacity={0.8}
          style={{
            height: 53,
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 53,
            backgroundColor: isValidNew
              ? ColorsDefault.gray2
              : ColorsDefault.blue1,
            marginTop: 30,
          }}>
          <Text
            color={ColorsDefault.white}
            fontSize={FontSizeDefault.FONT_16}
            fontWeight={'bold'}>
            {'Hoàn thành'}
          </Text>
        </TouchableOpacity>
      )}

      <Modal transparent={true} visible={show}>
        <DateTimePickerModal
          isVisible={show}
          mode="date"
          maximumDate={new Date(new Date().getFullYear() + 50, 1, 1)}
          minimumDate={new Date(new Date().getFullYear() - 50, 1, 1)}
          onConfirm={handleConfirm}
          onCancel={hideDatePicker}
          confirmTextIOS={Constants.Confirm}
          cancelTextIOS={Constants.Cancel}
        />
      </Modal>
      <ModalActionSheet
        title={'Chọn ảnh cho bé'}
        ref={actionSheet}
        onSelect={onSelectActionSheet}
      />
      <ModalConfirm
        ref={photoRef}
        title={'Quyền truy cập vào ảnh'}
        subTitle={
          'Đi tới "Cài đặt-> Quyền riêng tư -> Ảnh" mà bạn đã yêu cầu quyền truy cập vào ảnh thành "Bật"'
        }
        textConfirm={'Cài đặt'}
        textCancel={'Không'}
      />
    </KeyboardAwareScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: ColorsDefault.white,
  },
  contentScroll: {
    paddingBottom: StaticSafeAreaInsets.safeAreaInsetsBottom + vs(20),
    paddingHorizontal: 16,
  },
  avatarContainer: {
    paddingBottom: 35,
    position: 'relative',
  },
  icon: {
    position: 'absolute',
    bottom: 28,
    right: 0,
  },
  inputContainer: {
    width: '100%',
    paddingBottom: 16,
  },
  inputDate: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    justifyContent: 'space-between',
    backgroundColor: ColorsDefault.gray1,
    height: 54,
    borderRadius: 60,
    borderColor: '#828282',
    marginBottom: 8,
    paddingHorizontal: 24,
    marginTop: 8,
  },
  title: {
    color: ColorsDefault.black,
    fontSize: Constants.txtSize,
    fontWeight: Constants.fontWeight,
    paddingBottom: 5,
  },
  btn: {
    marginTop: 8,
    marginBottom: 100,
  },
});

export default EditProfile;
