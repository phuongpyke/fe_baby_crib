import React, {useState} from 'react';
import {ColorsDefault} from '/assets';
import {Block, Body, List, Text, Touchable, Image} from '/components';
import images from '/assets/images';

interface Props {
  id: number;
  name: string;
  selectValue?: any;
  onChange?: any;
  item?: any;
}

export const ItemCheckbox = ({
  id,
  name,
  selectValue,
  onChange,
  item,
}: Props) => {
  return (
    <>
      <Touchable
        onPress={() => {
          onChange(id);
        }}
        key={id}
        mr={12}
        mt={16}
        row
        middle>
        <Image
          m={4}
          source={selectValue === id ? images.checkboxActive : images.checkbox}
          width={16}
          height={16}
          resizeMode={'contain'}
        />
        <Text ml={16} type="h4" color={ColorsDefault.black2}>
          {name}
        </Text>
      </Touchable>
    </>
  );
};
