import {useNavigation} from '@react-navigation/native';
import LottieView from 'lottie-react-native';
import React, {useEffect, useState} from 'react';
import {
  FlatList,
  NativeEventEmitter,
  NativeModules,
  PermissionsAndroid,
  Platform,
  View,
} from 'react-native';
import BleManager from 'react-native-ble-manager';
import {getBottomSpace} from 'react-native-iphone-x-helper';
import {useDispatch} from 'react-redux';
import reactotron from 'reactotron-react-native';
import {storeObjData} from '/config/AsyncStorage';
import {read} from '/config/BleController';
import {styles} from './styles';
import {ColorsDefault} from '/assets';
import {Block, Body, Loading, Text, Touchable} from '/components';
import {onSetBlueToothId} from '/modules/user/slice';
import {Routes} from '/navigations/Routes';

const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);

const addressMacService = '00000001-373F-11EC-8D3D-0242AC130003';
const adddresMacCharacteristic = '00000009-373F-11EC-8D3D-0242AC130003';

const wifiCharacteristic = '00000010-373F-11EC-8D3D-0242AC130003';
const wifiService = '00000001-373F-11EC-8D3D-0242AC130003';

const ScanDevices = () => {
  const peripherals = new Map();
  const [list, setList] = useState([]);
  const [isConnected, setIsConnected] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [name, setName] = useState('');
  const navigation = useNavigation();
  const [peripheralId, setPeripheralId] = useState(null);
  const [listWifi, setListWifi] = useState([]);
  const dispatch = useDispatch();

  useEffect(() => {
    BleManager.start();

    const discoverPeripheral = bleManagerEmitter.addListener(
      'BleManagerDiscoverPeripheral',
      handleDiscoverPeripheral,
    );
    const stopScan = bleManagerEmitter.addListener(
      'BleManagerStopScan',
      handleStopScan,
    );
    const disconnectedPeripheral = bleManagerEmitter.addListener(
      'BleManagerDisconnectPeripheral',
      handleDisconnectedPeripheral,
    );
    const updateValue = bleManagerEmitter.addListener(
      'BleManagerDidUpdateValueForCharacteristic',
      handleUpdateValueForCharacteristic,
    );

    // startScan()
    if (Platform.OS === 'android' && Platform.Version >= 23) {
      PermissionsAndroid.check(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      ).then(result => {
        if (result) {
          console.log('Permission is OK');
        } else {
          PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          ).then(result => {
            if (result) {
              console.log('User accept');
            } else {
              console.log('User refuse');
            }
          });
        }
      });
    }

    return () => {
      discoverPeripheral.remove();
      stopScan.remove();
      disconnectedPeripheral.remove();
      updateValue.remove();
    };
  }, []);

  useEffect(() => {
    startScan();
    const interval = setInterval(() => {
      startScan();
    }, 10000);
    return () => clearInterval(interval);
  }, []);

  // Functions
  const handleDisconnectedPeripheral = data => {
    let peripheral = peripherals.get(data.peripheral);
    if (peripheral) {
      peripheral.connected = false;
      peripherals.set(peripheral.id, peripheral);
      setList(Array.from(peripherals.values()));
    }
  };

  const handleDiscoverPeripheral = peripheral => {
    if (peripheral.name) {
      peripherals.set(peripheral.id, peripheral);
      setList(Array.from(peripherals.values()));
    }
  };

  const handleStopScan = () => {};

  const handleUpdateValueForCharacteristic = data => {
    console.log(
      'Received data from ' +
        data.peripheral +
        ' characteristic ' +
        data.characteristic,
      data.value,
    );
  };

  const startScan = () => {
    BleManager.scan([], 5, false)
      .then(() => {
        reactotron.log('xxxxxxxx');
      })
      .catch(err => {
        console.error(err);
      });
  };

  const handleConnect = peripheral => {
    if (
      peripheral?.name === 'raspberrypi' ||
      peripheral?.name === 'Crib Controller'
    ) {
      if (peripheral.connected) {
        return BleManager.disconnect(peripheral.id);
      }
      setIsLoading(true);
      BleManager.connect(peripheral.id)
        .then(() => {
          setPeripheralId(peripheral.id);
          if (Platform.OS === 'ios') {
            BleManager.retrieveServices(peripheral.id)
              .then(peripheralData => {
                peripheralData.characteristics.forEach(async item => {
                  if (
                    item.characteristic.toUpperCase() === wifiCharacteristic
                  ) {
                    let stringWifi = await read(
                      peripheral.id,
                      wifiService,
                      wifiCharacteristic,
                    );
                    reactotron.log(stringWifi);
                    setListWifi(stringWifi.split('_'));
                  }
                  if (
                    item.characteristic.toUpperCase() ===
                    adddresMacCharacteristic
                  ) {
                    let result = await read(
                      peripheral.id,
                      item.service,
                      item.characteristic,
                    );
                    if (result) {
                      dispatch(onSetBlueToothId({data: peripheral.id}));
                      setName(peripheral.name);
                      setIsConnected(true);
                    }
                  }
                });
              })
              .catch(error => {
                reactotron.log(error);
              });
          } else {
            storeObjData('crib', peripheral);
            setName(peripheral.name);
          }
          setIsLoading(false);
        })
        .catch(error => {
          setIsLoading(false);
          reactotron.log(error);
        });
    }
  };

  // Components
  const renderItem = ({item}) => {
    const color = item.connected ? 'green' : ColorsDefault.black;
    return (
      <Touchable mt={16} onPress={() => handleConnect(item)}>
        <Block>
          <Text fontWeight={'medium'} fontSize={16} color={color}>
            {item.name}
          </Text>
          <Text
            mt={8}
            fontSize={14}
            color={item.connected ? 'green' : ColorsDefault.textPlaceHolder}>
            {item.id}
          </Text>
        </Block>
        <Block mt={16} height={1} bg={ColorsDefault.gray1}></Block>
      </Touchable>
    );
  };

  const renderNodata = () => {
    return (
      <Text mt={'30%'} textCenter lineHeight={28} fontSize={18}>
        {' '}
        Không tìm thấy thiết bị nào
      </Text>
    );
  };

  const goScanWifi = async () => {
    navigation.navigate(Routes.Wifi, {
      listWifi,
      peripheralId,
    });
  };

  return (
    <>
      <View style={styles.page}>
        {isConnected ? (
          <>
            <LottieView
              source={require('../../assets/loader/connected.json')}
              autoPlay
              loop
              style={styles.lottie}
            />
            <Text style={{textAlign: 'center', paddingTop: 50}}>
              Đã kết nối thành công với: {name}
            </Text>
          </>
        ) : (
          <Body bg={ColorsDefault.white} loading={isLoading}>
            <Block mt={26} row ph={16}>
              <Text
                lineHeight={24}
                uppercase
                fontSize={16}
                fontWeight={'medium'}
                color={ColorsDefault.gray3}>
                các thiết bị
              </Text>
              <Loading ml={8} />
            </Block>
            <FlatList
              data={list}
              contentContainerStyle={{paddingHorizontal: 16, paddingBottom: 30}}
              renderItem={renderItem}
              keyExtractor={item => item.id}
              ListEmptyComponent={renderNodata}
            />
          </Body>
        )}
      </View>
      {isConnected && (
        <Touchable
          onPress={goScanWifi}
          bg={ColorsDefault.blue1}
          height={53}
          borderRadius={6}
          middle
          center
          mb={getBottomSpace() + 16}
          mh={16}>
          <Text
            lineHeight={22}
            fontSize={16}
            fontWeight={'medium'}
            color={ColorsDefault.white}>
            Tiếp theo
          </Text>
        </Touchable>
      )}
    </>
  );
};

export default ScanDevices;
