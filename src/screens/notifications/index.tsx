import React from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import {ColorsDefault, FontFamilyDefault, FontSizeDefault} from '/assets';
import images from '/assets/images';
import {Block, Image, List, Text} from '/components';
import {Header} from '/components/common';

const dataFake = [
  {
    id: 0,
    title: 'Bé khóc nhè',
    time: '1 giờ trước',
  },
  {
    id: 1,
    title: 'Bé ngoan nè',
    time: '1 giờ trước',
  },
  {
    id: 2,
    title: 'Đến giờ ăn của bé',
    time: '1 giờ trước',
  },
];

const Notification = () => {
  const renderItem = ({item}: any) => {
    return (
      <Block row pv={16} middle>
        <Image source={images.babyNotification} width={64} height={64}></Image>
        <Block ml={24}>
          <Text fontSize={16} fontWeight={'medium'}>
            {item.title}
          </Text>
          <Text fontSize={12} color={'#404040'}>
            {item.time}
          </Text>
        </Block>
      </Block>
    );
  };

  return (
    <SafeAreaView
      style={{flex: 1, backgroundColor: ColorsDefault.white}}
      edges={['right', 'left', 'top']}>
      <Header
        isBack={false}
        title="Thông báo"
        titleStyle={{
          fontSize: FontSizeDefault.FONT_24,
          fontFamily: FontFamilyDefault.primaryBold,
        }}
      />
      <List
        data={dataFake}
        contentContainerStyle={{paddingHorizontal: 16, paddingBottom: 64}}
        renderItem={renderItem}
      />
    </SafeAreaView>
  );
};

export default Notification;
