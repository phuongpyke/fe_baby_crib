import React, {useEffect, useState} from 'react';
import {FlatList} from 'react-native';
import reactotron from 'reactotron-react-native';
import ItemDiaper from './ItemDiaper';
import {getAllProductOfCategory} from '/api/modules/product';
import {ColorsDefault} from '/assets';
import images from '/assets/images';
import {BOTTOM_PADDING} from '/common';
import {Block, Body, Image, Text, Touchable} from '/components/base';
import {TextDetail} from '/components/common';
import {hitSlop} from '/utils/helper';

const dataFake = {
  hieght: '90cm',
  wieght: '10kg',
  ageMonth: '4',
  suggest: 'Size S ( dành cho bé dưới 5kg)',
  type: 'Tã dán',
  list: [
    {
      id: 1,
      name: 'Bỉm merries',
      content:
        'Chất liệu mềm mịn, độ dán của bỉm cũng khá chắc không bị lỏng lẻo sau mỗi lần tháo. Với công nghệ vượt trội cùng tiêu chí tiên phong "Luôn mềm mại và thông thoáng cho da bé", sản phẩm mang đến cảm giác thông thoáng, mềm mại tuyệt vời cho làn da mỏng manh của bé, giúp bé có thể thoải mái vui đùa hay có những giấc ngủ thật ngon. Nay Merries có phiên bản cải tiến được thêm 6 miếng với chất lượng nội địa Nhật Bản cao cấp, mang đến sản phẩm vệ sinh hữu ích lại giúp tiết kiệm chi phí cho mẹ',
      isLike: true,
      numberLike: 10,
      numberComment: 2,
      image: images.diapers1,
      price: '295.000đ',
      size: 'Size M 58+6',
    },
    {
      id: 2,
      name: 'Bỉm Moony',
      content:
        'Moony là thương hiệu tã cao cấp nhập khẩu trực tiếp từ NHÀ SẢN XUẤT TÃ SỐ 1 NHẬT BẢN - Tập đoàn Unicharm. Có mặt trên thị trường gần 40 năm, Moony đã giành được sự tin yêu của các bà mẹ Nhật Bản nhờ những tính năng vượt trội và công nghệ tân tiến với rất nhiều giải thưởng danh giá',
      isLike: false,
      numberLike: 10,
      numberComment: 2,
      image: images.diapers1,
      price: '295.000đ',
      size: 'Size M 58+6',
    },
  ],
};

const Diapers = () => {
  const [listDiaper, setListDiaper] = useState<any[]>([]);
  const [paging, setPaging] = useState<any>({
    currentPage: 0,
    lastPage: 0,
  });

  useEffect(() => {
    const getData = async () => {
      try {
        const res: any = await getAllProductOfCategory({id: 1});
        if (res?.success) {
          const {data = [], meta} = res ?? {};
          setListDiaper(data);
          setPaging({
            currentPage: meta?.current_page,
            lastPage: meta?.last_page,
          });
        }
      } catch (err) {
        reactotron.warn!(err);
      }
    };
    getData();
  }, []);

  const renderInformationBaby = () => {
    return (
      <Block
        m={16}
        borderRadius={12}
        bg={ColorsDefault.white}
        ph={12}
        pv={16}
        borderWidth={1}
        borderColor={ColorsDefault.white}
        shadow>
        <Image
          style={{position: 'absolute', top: 0, alignSelf: 'center'}}
          source={images.backgroundDiapers}
          width={268}
          height={108}
        />
        <Text
          textCenter
          type="h3"
          color={ColorsDefault.orange1}
          fontWeight={'bold'}>
          {' '}
          Thông tin bé
        </Text>
        <Block row middle justifyBetween mt={16}>
          <Block row middle>
            <Image width={36} height={36} source={images.heightBaby} />
            <Text ml={8} type="h5" fontSize={16}>
              Chiều cao
            </Text>
          </Block>
          <Text type="h5" fontWeight={'extraBold'}>
            {dataFake.hieght}
          </Text>
        </Block>
        <Block row middle justifyBetween mt={16}>
          <Block row middle>
            <Image width={36} height={36} source={images.weightBaby} />
            <Text ml={8} type="h5">
              Cân nặng
            </Text>
          </Block>
          <Text type="h5" fontWeight={'bold'}>
            {dataFake.wieght}
          </Text>
        </Block>
        <Block row middle justifyBetween mt={16}>
          <Block row middle>
            <Image width={36} height={36} source={images.calendar} />
            <Text ml={8} type="h5">
              Số tháng tuổi
            </Text>
          </Block>
          <Text fontSize={16} fontWeight={'medium'}>
            {dataFake.ageMonth}
          </Text>
        </Block>
      </Block>
    );
  };

  const renderHeader = () => {
    return (
      <Block>
        <TextDetail
          title={
            'AI dựa theo chiều cao, cân nặng, tháng tuổi và thời tiết để đưa ra gợi ý size bỉm, loại bỉm cho bé. '
          }
        />
        {renderInformationBaby()}

        <Block
          row
          middle
          bg={ColorsDefault.pink2}
          pv={5}
          justifyBetween
          mb={16}
          ph={32}>
          <Text>{'Hà Nội'}</Text>
          <Block>
            <Text>{'Mùa thu'}</Text>
          </Block>
          <Block row middle>
            <Image source={images.celsius} height={16} width={16} />
            <Text ml={5}>{'30 C'}</Text>
          </Block>
          <Block row middle>
            <Image source={images.sun} height={16} width={16} />
            <Text ml={5}>{'68%'}</Text>
          </Block>
        </Block>

        <Block bg={ColorsDefault.purple1} mh={16} borderRadius={6} ph={12}>
          <Image
            source={images.diaper}
            style={{
              position: 'absolute',
              right: 8,
              top: 22,
            }}
            width={102}
            height={89}
          />
          <Text type="h4" fontWeight={'semiBold'} mt={24}>
            AI gợi ý size bỉm cho bé
          </Text>
          <Text mt={16} type="h4">
            {dataFake.suggest} - Loại: {dataFake.type}
          </Text>
          <Text mt={16} type="h4" mb={24}>
            Thời gian thay bỉm: 2-3 tiếng/lần
          </Text>
        </Block>
        <Block row middle justifyBetween ph={16} mt={24}>
          <Text type="h4" fontWeight={'medium'}>
            {'Chọn loại bỉm cho bé'}
          </Text>
          <Touchable hitSlop={hitSlop(10)}>
            <Image source={images.rightArrow} width={24} height={24} />
          </Touchable>
        </Block>
        <Text ph={16} mt={16} color={ColorsDefault.gray3} type="h5">
          {
            'Chúng tôi sẽ dựa vào loại bỉm bạn chọn để đưa ra gợi ý size bỉm, giá tiền và thời gian thay bỉm cho bé trong ngày'
          }
        </Text>
      </Block>
    );
  };

  const renderItem = ({item}: any) => <ItemDiaper data={item} />;

  return (
    <Body
      bg={ColorsDefault.white}
      loading={false}
      showsVerticalScrollIndicator={false}>
      <FlatList
        showsVerticalScrollIndicator={false}
        data={listDiaper}
        contentContainerStyle={{paddingBottom: BOTTOM_PADDING}}
        ListHeaderComponent={renderHeader}
        renderItem={renderItem}
        keyExtractor={(item: any) => item.id}
      />
    </Body>
  );
};

export default Diapers;
