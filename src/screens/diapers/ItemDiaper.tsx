import React from 'react';
import {rateProduct} from '/api/modules/product';
import {ColorsDefault} from '/assets';
import images from '/assets/images';
import {Block, Image, Text, Touchable} from '/components/base';
import {RateStar, TextSeeMore} from '/components/common';
import {navigate} from '/navigations';
import {Routes} from '/navigations/Routes';
import {showToastSuccess} from '/utils/helper';

interface Props {
  data: any;
}

const ItemDiaper = (props: Props) => {
  const {data} = props;

  const goDiaperDetail = () => navigate(Routes.DiapersDetail);

  const onChangeRate = async (rating: number) => {
    try {
      const res: any = await rateProduct({
        product_id: data.id,
        rating,
        comment: '',
      });
      if (res.success) {
        showToastSuccess(res?.message);
      }
    } catch (err) {}
  };

  return (
    <Block
      mt={16}
      mh={16}
      pt={12}
      bg={ColorsDefault.white}
      borderRadius={6}
      shadow>
      <Touchable absolute zIndex={10} right={12} top={12}>
        <Image
          source={images.checkbox}
          width={20}
          height={20}
          resizeMode={'contain'}
        />
      </Touchable>
      <Touchable onPress={goDiaperDetail} row>
        <Block middle>
          <Image width={96} height={96} source={data.image} />
          <Block row middle mt={8}>
            <Image
              source={images.rateStar}
              width={16}
              height={16}
              tintColor={ColorsDefault.yellow3}
            />
            <Text ml={4} fontSize={16}>
              {data.rating}
            </Text>
          </Block>
        </Block>
        <Block ml={8} mr={12} flex={1}>
          {!!data.name && (
            <Text type="h4" fontWeight="semiBold">
              {data.name}
            </Text>
          )}
          {!!data?.price_str && (
            <Text mt={4} type="h5" fontSize={14} color={ColorsDefault.blue1}>
              {data?.price_str}
            </Text>
          )}
          {!!data?.size_str && (
            <Text mt={4} type="h5" fontSize={14} color={ColorsDefault.blue1}>
              {data?.size_str}
            </Text>
          )}
          {!!data?.description && (
            <TextSeeMore content={data?.description} defaultNumberOfLines={3} />
          )}
        </Block>
      </Touchable>
      <Block height={1} bg={ColorsDefault.gray} mt={16} mh={16} />
      <Block row middle justifyBetween mh={16} pv={12}>
        <Text type={'h5'} color={ColorsDefault.black3} fontWeight={'medium'}>
          {'Đánh giá ngay'}
        </Text>
        <RateStar onPress={onChangeRate} />
      </Block>
    </Block>
  );
};

export default ItemDiaper;
