import auth from '@react-native-firebase/auth';
import messaging from '@react-native-firebase/messaging';
import {useNavigation} from '@react-navigation/native';
import {FormikProps, useFormik} from 'formik';
import React from 'react';
import Toast from 'react-native-toast-message';
import {checkPhoneExist} from '/api/modules/auth';
import {ColorsDefault} from '/assets';
import images from '/assets/images';
import {WIDTH_DEVICE} from '/common';
import {Block, Body, Image, Input, Text, Touchable} from '/components';
import {Routes} from '/navigations/Routes';

interface Props {}

interface MyValues {
  phone: string;
}

const validate = (values: any) => {
  const errors: any = {};
  if (!values.phone) {
    errors.phone = 'Hãy nhập số điện thoại';
  }
  if (values.phone && values.phone.length !== 10) {
    errors.phone = 'Số điện thoại không hợp lệ';
  }
  return errors;
};
export const ForgotPassword = (props: Props) => {
  const navigation = useNavigation<any>();

  const formik: FormikProps<MyValues> = useFormik<MyValues>({
    initialValues: {phone: ''},
    validate,
    onSubmit: () => {},
  });

  const goOtp = async () => {
    try {
      const phone = formik.values.phone;
      // const checkPhone: any = await checkPhoneExist({phone});
      // if (checkPhone?.success) {
      const formatPhone = `+84 ${phone.substring(1, 3)}-${phone.substring(
        3,
        6,
      )}-${phone.substring(6, 10)}`;
      await messaging().getToken();
      const confirmation = await auth().signInWithPhoneNumber(formatPhone);
      navigation.navigate(Routes.Otp, {
        phone,
        isForgotPassword: true,
        confirmation,
      });
      // }
    } catch (err: any) {
      if (err?.message) {
        Toast.show({
          type: 'custom',
          text1: err?.message,
          props: {
            type: 'error',
          },
        });
      }
    }
  };

  return (
    <Body scroll keyboardAvoid bg={ColorsDefault.white}>
      <Image
        resizeMode={'stretch'}
        source={images.headerForgotPassword}
        width={WIDTH_DEVICE}
        height={181}>
        <Block mt={58} pl={16} row justifyBetween middle>
          <Touchable onPress={() => navigation.goBack()}>
            <Image
              source={images.backLeft}
              tintColor={ColorsDefault.black}
              width={24}
              height={24}
            />
          </Touchable>
          <Text type="p" fontWeight="semiBold">
            Quên mật khẩu
          </Text>
          <Block width={24}></Block>
        </Block>
      </Image>
      <Block mh={16} mt={24}>
        <Text type="p" fontWeight="medium" textCenter>
          Vui lòng nhập số điện thoại đăng nhập
        </Text>
        <Input
          mt={24}
          width={WIDTH_DEVICE - 32}
          style={{
            borderWidth: 1,
            borderRadius: 60,
            fontSize: 16,
            borderColor: ColorsDefault.gray1,
          }}
          keyboardType={'number-pad'}
          height={58}
          value={formik.values?.phone}
          onChangeText={formik.handleChange('phone')}
          placeholder="Số điện thoại"
          error={formik.errors.phone && formik.touched.phone}
          errorMessage={formik.errors.phone}
          onBlur={formik.handleBlur('phone')}
        />
        <Block mv={15} />
        <Touchable
          onPress={goOtp}
          disabled={!(!formik.errors.phone && formik.values.phone)}
          middle
          mt={13}
          center
          borderRadius={60}
          bg={
            !(!formik.errors.phone && formik.values.phone)
              ? ColorsDefault.gray2
              : ColorsDefault.blue1
          }
          height={53}>
          <Text type="p" color={ColorsDefault.white} fontWeight={'bold'}>
            Tiếp theo
          </Text>
        </Touchable>
      </Block>
      <Block style={{position: 'absolute', bottom: 0, left: 0, width: 0}}>
        <Image
          center
          middle
          source={images.footerRegister}
          width={WIDTH_DEVICE}
          height={265}
        />
      </Block>
    </Body>
  );
};
