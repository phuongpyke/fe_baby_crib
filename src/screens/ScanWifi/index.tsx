import {RouteProp, useNavigation, useRoute} from '@react-navigation/native';
import {stringToBytes} from 'convert-string';
import React, {useState} from 'react';
import {FlatList, Modal} from 'react-native';
import BleManager from 'react-native-ble-manager';
import Toast from 'react-native-toast-message';
import reactotron from 'reactotron-react-native';
import {ColorsDefault} from '/assets';
import images from '/assets/images';
import {HEIGHT_DEVICE, WIDTH_DEVICE} from '/common';
import {Block, Body, Image, Input, Loading, Text, Touchable} from '/components';
import {writeWithoutResponse} from '/config/BleController';
import {Routes} from '/navigations/Routes';

const data = ['Phong 512', 'phong 602', 'ok baby'];

type RouteParams = {
  listWifi: string[];
  peripheralId: string;
};

const writeWifi = '00000012-373F-11EC-8D3D-0242AC130003';

const ScanWifi = () => {
  const {params} = useRoute<RouteProp<Record<string, RouteParams>, string>>();

  const [isLoading, setIsLoading] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [passWifi, setPassWifi] = useState('');
  const [nameWifi, setNameWifi] = useState('');

  const navigation: any = useNavigation();

  const goLoadingWifi = () => {
    navigation.navigate(Routes.LoadingWifi);
  };

  const renderItem = ({item}: any) => {
    const color = item.connected ? 'green' : ColorsDefault.black;
    return (
      <>
        <Touchable
          mt={18}
          onPress={() => {
            setNameWifi(item);
            setShowModal(true);
          }}>
          <Block row justifyBetween center>
            <Text fontWeight={'medium'} fontSize={16} color={color}>
              {item}
            </Text>
            <Block row center middle mr={8}>
              <Image source={images.lock} width={12} height={16} />
              <Image source={images.wifi} width={16} height={12} ml={14} />
            </Block>
          </Block>
          <Block mt={18} height={1} bg={ColorsDefault.gray1}></Block>
        </Touchable>
      </>
    );
  };

  const renderNodata = () => {
    return (
      <Text mt={HEIGHT_DEVICE / 3} textCenter lineHeight={28} fontSize={14}>
        {' '}
        Không tìm thấy thiết bị nào
      </Text>
    );
  };

  const connectWifi = () => {
    BleManager.retrieveServices(params?.peripheralId).then(
      (peripheralData: any) => {
        peripheralData.characteristics.forEach(async (item: any) => {
          if (item.characteristic === writeWifi) {
            const data = stringToBytes(`${nameWifi}_${passWifi}`);
            reactotron.log('xxxxxxx', data);
            const status: any = await writeWithoutResponse(
              params?.peripheralId,
              item.service,
              writeWifi,
              data,
            );
            if (status) {
              setShowModal(false);
              goLoadingWifi();
            } else {
              setShowModal(false);
              Toast.show({
                type: 'custom',
                text1: `${'Có lỗi xảy ra. Vui lòng thử lại!'}`,
                props: {
                  type: 'error',
                },
              });
            }
          }
        });
      },
    );
  };

  return (
    <>
      <Body bg={ColorsDefault.white} loading={isLoading}>
        <Block mt={26} row ph={16}>
          <Text
            lineHeight={24}
            uppercase
            fontSize={16}
            fontWeight={'medium'}
            color={ColorsDefault.gray3}>
            Mạng
          </Text>
          <Loading ml={8} />
        </Block>
        <FlatList
          data={params.listWifi}
          contentContainerStyle={{paddingHorizontal: 16, paddingBottom: 30}}
          renderItem={renderItem}
          keyExtractor={(item, index) => index.toString()}
          ListEmptyComponent={renderNodata}
        />
      </Body>
      <Modal
        animationType="fade"
        transparent={true}
        visible={showModal}
        style={{flex: 1}}>
        <Block flex={1} center bg={'#00000033'}>
          <Block bg={ColorsDefault.white} borderRadius={8} mh={16} pb={32}>
            <Text fontSize={18} pt={24} pl={16} fontWeight={'medium'}>
              Nhập mật khẩu
            </Text>
            <Input
              value={passWifi}
              onChangeText={text => setPassWifi(text)}
              mt={16}
              mh={16}
              style={{
                width: WIDTH_DEVICE - 64,
              }}
              height={48}
            />
            <Touchable
              onPress={connectWifi}
              bg={ColorsDefault.blue1}
              height={53}
              borderRadius={6}
              middle
              center
              mh={16}
              mv={16}>
              <Text
                lineHeight={22}
                fontSize={16}
                fontWeight={'medium'}
                color={ColorsDefault.white}>
                Tiếp theo
              </Text>
            </Touchable>
            <Touchable
              absolute
              top={20}
              right={20}
              onPress={() => setShowModal(false)}>
              <Image source={images.close} style={{width: 18, height: 18}} />
            </Touchable>
          </Block>
        </Block>
      </Modal>
    </>
  );
};

export default ScanWifi;
