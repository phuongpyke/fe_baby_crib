import {yupResolver} from '@hookform/resolvers/yup';
import {useNavigation} from '@react-navigation/native';
import moment from 'moment';
import React, {useEffect, useLayoutEffect, useRef, useState} from 'react';
import isEqual from 'react-fast-compare';
import {Controller, useForm} from 'react-hook-form';
import {Image, Text, TextInput, TouchableOpacity, View} from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {s, ScaledSheet, vs} from 'react-native-size-matters';
import reactotron from 'reactotron-react-native';
import * as yup from 'yup';
import {getAllLabels} from '/api/modules/news';
import {createSchedule} from '/api/modules/schedule';
import {ColorsDefault, FontFamilyDefault, FontSizeDefault} from '/assets';
import images from '/assets/images';
import {FULL_WIDTH_DEVICE} from '/common';
import {SelectTimeSchedule} from '/components/common';
import {useAppSelector} from '/redux/hooks';
import {showToastError, showToastSuccess} from '/utils/helper';

const CreateSchedule = () => {
  const navigation: any = useNavigation();

  const {babyList} = useAppSelector(
    ({user: {babyList}}) => ({babyList}),
    isEqual,
  );

  const [valueAction, setValueAction] = useState<any>(null);
  const [listActions, setListActions] = useState<any[]>([]);
  const [openModalAction, setModalAction] = useState<boolean>(false);
  const [startTime, setStartTime] = useState<string>('06:15');
  const selectTimeRef = useRef<any>(null);

  const yupSchema = yup.object().shape({
    label: yup.string().required(),
    content: yup.string().required('Lịch chăm bé không hợp lệ'),
  });
  const {
    setValue,
    handleSubmit,
    control,
    formState: {errors, isValid},
  } = useForm({
    mode: 'onChange',
    resolver: yupResolver(yupSchema),
    reValidateMode: 'onChange',
    criteriaMode: 'firstError',
  });

  const goBack = () => navigation.goBack();

  const onSubmit = async (data: any) => {
    try {
      const stopTime = moment(startTime, 'HH:mm')
        .add(1, 'hour')
        .format('HH:mm');
      const res: any = await createSchedule({
        baby_id: babyList?.[0]?.id,
        label: data.label,
        // content: data.content,
        start: startTime,
        stop: stopTime,
      });
      if (res?.success) {
        showToastSuccess(res?.message);
        goBack();
      }
    } catch (err: any) {
      reactotron.warn!(err);
      showToastError(err?.message);
    } finally {
    }
  };

  const onChangeLabel = (data: any) => {
    setValue('label', data, {
      shouldValidate: true,
    });
  };

  useEffect(() => {
    const getData = async () => {
      try {
        const res: any = await getAllLabels();
        if (res.success) {
          setListActions(res?.data);
        }
      } catch (err) {
      } finally {
      }
    };
    getData();
  }, []);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <TouchableOpacity style={styles.btnHeader} onPress={goBack}>
          <Text style={styles.textLeftHeader}>{'Hủy'}</Text>
        </TouchableOpacity>
      ),
      headerRight: () => (
        <TouchableOpacity
          style={styles.btnHeader}
          onPress={handleSubmit(onSubmit)}
          disabled={!isValid}>
          <Text
            style={[
              styles.textRightHeader,
              isValid && {
                color: ColorsDefault.blue1,
              },
            ]}>
            {'Thêm'}
          </Text>
        </TouchableOpacity>
      ),
    });
  }, [isValid, startTime]);

  return (
    <KeyboardAwareScrollView
      style={styles.container}
      contentContainerStyle={styles.contentStyle}
      keyboardShouldPersistTaps="handled"
      showsVerticalScrollIndicator={false}
      enableResetScrollToCoords={true}>
      <SelectTimeSchedule
        ref={selectTimeRef}
        boxStyle={styles.selectTime}
        onChangeValue={value => {
          setStartTime(value);
        }}
      />
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          marginHorizontal: s(16),
          marginTop: vs(25),
          zIndex: 10,
        }}>
        <Text
          style={{
            fontSize: FontSizeDefault.FONT_16,
            fontFamily: FontFamilyDefault.primaryMedium,
          }}>
          {'Nhãn'}
        </Text>
        <DropDownPicker
          open={openModalAction}
          value={valueAction}
          items={listActions}
          setOpen={setModalAction}
          setValue={setValueAction}
          setItems={setListActions}
          onChangeValue={onChangeLabel}
          placeholder={'Chọn nhãn'}
          listMode={'SCROLLVIEW'}
          zIndex={10}
          style={{
            borderColor: ColorsDefault.gray1,
            borderRadius: 6,
          }}
          dropDownContainerStyle={{
            borderColor: ColorsDefault.gray1,
            borderRadius: 6,
          }}
          placeholderStyle={{
            color: ColorsDefault.black3,
            fontFamily: FontFamilyDefault.primaryRegular,
            fontSize: FontSizeDefault.FONT_16,
          }}
          containerStyle={{
            width: FULL_WIDTH_DEVICE * 0.4,
          }}
          schema={{
            label: 'category_name',
            value: 'id',
            testID: 'id',
          }}
        />
      </View>
      <Text style={styles.textSubTitle}>{'Nội dung'}</Text>
      <Controller
        control={control}
        render={({field: {onChange, onBlur, value}}) => (
          <TextInput
            style={[styles.textInput, errors?.content && styles.textInputError]}
            autoCorrect={false}
            placeholder="VD: E.A.S.Y - 1"
            placeholderTextColor={ColorsDefault.gray3}
            onChangeText={value => onChange(value)}
            onBlur={onBlur}
            value={value}
          />
        )}
        name="content"
        rules={{required: true}}
      />
      {errors?.content?.message && (
        <Text style={styles.textError}>{`${errors?.content?.message}`}</Text>
      )}
      <View style={{flex: 1}} />
      <Image source={images.footerRegister} style={styles.imgFooter} />
    </KeyboardAwareScrollView>
  );
};

const styles = ScaledSheet.create({
  container: {
    backgroundColor: ColorsDefault.white,
    flex: 1,
  },
  contentStyle: {
    flexGrow: 1,
    paddingTop: '50@vs',
  },
  btnHeader: {
    padding: 8,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textLeftHeader: {
    fontSize: FontSizeDefault.FONT_16,
    fontFamily: FontFamilyDefault.primaryRegular,
    color: ColorsDefault.blue1,
  },
  textRightHeader: {
    fontSize: FontSizeDefault.FONT_16,
    fontFamily: FontFamilyDefault.primaryBold,
    color: ColorsDefault.gray3,
  },
  textSubTitle: {
    fontSize: FontSizeDefault.FONT_18,
    fontFamily: FontFamilyDefault.primaryMedium,
    color: ColorsDefault.black3,
    marginHorizontal: '16@s',
    marginTop: '36@vs',
    marginBottom: '12@vs',
  },
  viewBtn: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: '16@s',
  },
  btn: {
    height: '58@s',
    borderRadius: '58@s',
    justifyContent: 'center',
    alignItems: 'center',
    width: (FULL_WIDTH_DEVICE - s(48)) / 2,
    backgroundColor: ColorsDefault.gray1,
  },
  btnFocus: {
    backgroundColor: ColorsDefault.purple1,
    borderColor: ColorsDefault.blue1,
    borderWidth: 1,
  },
  textBtn: {
    fontSize: FontSizeDefault.FONT_16,
    fontFamily: FontFamilyDefault.primaryRegular,
    color: ColorsDefault.black3,
  },
  textInput: {
    height: '58@s',
    borderRadius: '58@s',
    backgroundColor: ColorsDefault.gray1,
    width: FULL_WIDTH_DEVICE - s(32),
    alignSelf: 'center',
    paddingHorizontal: '16@s',
    fontFamily: FontFamilyDefault.primaryRegular,
    fontSize: FontSizeDefault.FONT_18,
    color: ColorsDefault.black3,
  },
  textInputError: {
    borderWidth: 1,
    borderColor: ColorsDefault.textRed,
  },
  selectTime: {
    backgroundColor: ColorsDefault.white,
    borderWidth: 1,
    borderColor: ColorsDefault.gray1,
  },
  textError: {
    fontSize: FontSizeDefault.FONT_12,
    fontFamily: FontFamilyDefault.primaryRegular,
    color: ColorsDefault.textRed,
    marginTop: '4@vs',
    marginHorizontal: '16@s',
  },
  imgFooter: {
    bottom: 0,
    right: 0,
    left: 0,
    zIndex: -10,
    width: FULL_WIDTH_DEVICE,
    height: FULL_WIDTH_DEVICE * 0.7,
  },
});

export default CreateSchedule;
