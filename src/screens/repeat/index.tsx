import { useNavigation } from '@react-navigation/native';
import React, { useState } from 'react';
import { getStatusBarHeight } from 'react-native-iphone-x-helper';
import { ColorsDefault } from '/assets';
import { Block, Body, Loading, Text, Touchable, Image, Input } from '/components';
import images from "/assets/images";
import { Modal, Platform } from 'react-native';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import reactotron from 'reactotron-react-native';
import moment from 'moment';


const Repeat = () => {
    const navigation = useNavigation()
    const [timer, setTimer] = useState('06:15')
    const [showModalTime, setShowModalTime] = useState(false)

    const onClickModalTime = () => setShowModalTime(!showModalTime)

    const handleConfirm = (date: any) => {
        const _time = moment(date).format('HH:mm')
        setTimer(_time)
        onClickModalTime()
    }

    const TimePicker = () => {
        return (
            <Modal transparent={true} visible={showModalTime}>
                <DateTimePickerModal
                    isVisible={showModalTime}
                    mode="time"
                    locale="en_GB"
                    onConfirm={handleConfirm}
                    onCancel={onClickModalTime}
                    confirmTextIOS={"Xác nhận"}
                    cancelTextIOS={'Hủy'}
                />
            </Modal>
        );
    };

    return (
        <Body bg={ColorsDefault.white} ph={16}>
            <Block row center justifyBetween
                mt={Platform.OS === 'android' ? 16 : getStatusBarHeight() + 16}
            >
                <Touchable row onPress={() => navigation.goBack()} center middle>
                    <Image source={images.backLeft} width={20} height={20} />
                    <Text ml={12} lineHeight={24} fontSize={16}>
                        Quay lại
                    </Text>
                </Touchable>
                <Text fontSize={18} color={ColorsDefault.black} lineHeight={24} fontWeight={'medium'}> Lặp lại</Text>
                <Block width={60}></Block>
            </Block>
            <Touchable onPress={onClickModalTime} row center middle mt={160}>
                <Block borderWidth={1} borderColor={ColorsDefault.gray1} height={64} width={64} center middle>
                    <Text fontWeight={'bold'} fontSize={24} lineHeight={28}>{timer.slice(0, 1)}</Text>
                </Block>
                <Block ml={8} borderWidth={1} borderColor={ColorsDefault.gray1} height={64} width={64} center middle>
                    <Text fontWeight={'bold'} fontSize={24} lineHeight={28}>{timer.slice(1, 2)}</Text>
                </Block>
                <Text fontSize={16} mh={16} fontWeight={'medium'}>:</Text>
                <Block borderWidth={1} borderColor={ColorsDefault.gray1} height={64} width={64} center middle>
                    <Text fontWeight={'bold'} fontSize={24} lineHeight={28}>{timer.slice(3, 4)}</Text>
                </Block>
                <Block ml={8} borderWidth={1} borderColor={ColorsDefault.gray1} height={64} width={64} center middle>
                    <Text fontWeight={'bold'} fontSize={24} lineHeight={28}>{timer.slice(4, 5)}</Text>
                </Block>
            </Touchable>
            <TimePicker />
        </Body >
    )
}

export default Repeat;