import React, {useState} from 'react';
import {ImageBackground, StyleSheet} from 'react-native';
import Toast from 'react-native-toast-message';
import {useNavigation} from '@react-navigation/native';
import {
  Body,
  Image,
  Text,
  Block,
  Input,
  Touchable,
  List,
} from '/components/base';
import {ColorsDefault, FontFamilyDefault} from '/assets';
import images from '/assets/images';
import {FULL_WIDTH_DEVICE, WIDTH_DEVICE} from '/common';
import {Routes} from '/navigations/Routes';
import {RateStar, TextDetail} from '/components/common';
import {s, ScaledSheet} from 'react-native-size-matters';

const dataFake = {
  hieght: '90cm',
  wieght: '10kg',
  ageMonth: '4',
  suggest: 'Lưu ý: Cho bé uống từ từ không bị sặc sữa',
  milks: '120ml/lần',
  allMilks: '1200ml',
  averageOfDay: '8-12 lần',
  averageOfMilks: '35-60ml',
  list: [
    {
      id: 1,
      name: 'Bỉm merries',
      content:
        'Chất liệu mềm mịn, độ dán của bỉm cũng khá chắc không bị lỏng lẻo sau mỗi lần tháo.',
      isLike: true,
      numberLike: 10,
      numberComment: 2,
      image: images.diapers1,
    },
    {
      id: 2,
      name: 'Bỉm Moony',
      content:
        'Chất liệu mềm mịn, độ dán của bỉm cũng khá chắc không bị lỏng lẻo sau mỗi lần tháo.',
      isLike: false,
      numberLike: 10,
      numberComment: 2,
      image: images.diapers1,
    },
  ],
};

const Milks = () => {
  const navigation = useNavigation<any>();

  const [listMilk, setListMilk] = useState<any[]>(dataFake.list);

  const onSelectItem = (item: any) => {
    const list = listMilk.map((value: any) =>
      value.id === item.id ? {...value, isSelected: !value?.isSelected} : value,
    );
    setListMilk(list);
  };

  const renderHeader = () => {
    return (
      <Block>
        <TextDetail
          title={
            'AI dựa theo chiều cao, cân nặng, tháng tuổi, loại sữa và nhu cầu của bé hàng ngày theo chuẩn WHO, calories để đưa ra gợi ý. '
          }
        />
        <ImageBackground
          imageStyle={{borderRadius: 12}}
          source={images.backgroundMilk}
          style={styles.bgInfoBaby}>
          <Block middle>
            <Text type={'h3'} color={ColorsDefault.orange2} fontWeight={'bold'}>
              Thông tin bé
            </Text>
            <Block
              width={140}
              height={0}
              borderWidth={1}
              borderColor={ColorsDefault.orange2}
              borderStyle="dashed"></Block>
          </Block>
          <Block row middle justifyBetween mt={16}>
            <Block row middle>
              <Image width={36} height={36} source={images.heightBaby}></Image>
              <Text ml={8} type={'h4'}>
                Chiều cao
              </Text>
            </Block>
            <Text type={'h4'} fontWeight={'medium'}>
              {dataFake.hieght}
            </Text>
          </Block>
          <Block row middle justifyBetween mt={16}>
            <Block row middle>
              <Image width={36} height={36} source={images.weightBaby}></Image>
              <Text ml={8} type={'h4'}>
                Cân nặng
              </Text>
            </Block>
            <Text type={'h4'} fontWeight={'medium'}>
              {dataFake.wieght}
            </Text>
          </Block>
          <Block row middle justifyBetween mt={16}>
            <Block row middle>
              <Image width={36} height={36} source={images.calendar}></Image>
              <Text ml={8} type={'h4'}>
                Số tháng tuổi
              </Text>
            </Block>
            <Text type={'h4'} fontWeight={'medium'}>
              {dataFake.ageMonth}
            </Text>
          </Block>
          <Block row middle justifyBetween mt={16}>
            <Block row middle>
              <Image width={36} height={36} source={images.milk}></Image>
              <Text ml={8} type={'h4'}>
                Lượng sữa tối đa
              </Text>
            </Block>
            <Text type={'h4'} fontWeight={'medium'}>
              {dataFake.milks}
            </Text>
          </Block>
        </ImageBackground>

        <ImageBackground
          imageStyle={{borderTopLeftRadius: 12, borderTopRightRadius: 12}}
          source={images.backgroundMilk2}
          style={styles.bgInfoBaby}>
          <Text textCenter type={'h4'} fontWeight={'medium'}>
            AI gợi ý size bỉm cho bé
          </Text>
          <Block row justifyBetween center mt={16}>
            <Text type={'h4'}>{'Lượng sữa chuẩn'}</Text>
            <Text type={'h4'} fontWeight={'medium'}>
              {'1200ml'}
            </Text>
          </Block>
          <Block row justifyBetween center mt={16}>
            <Text type={'h4'}>{'Lượng sữa theo calo cho bé'}</Text>
            <Text type={'h4'} fontWeight={'medium'}>
              {'1200ml'}
            </Text>
          </Block>
        </ImageBackground>

        <Block bg={ColorsDefault.purple1} mh={s(16)} ph={s(12)} pb={16} mt={4}>
          <Block row justifyBetween center mt={16}>
            <Text type={'h4'}>Tổng lượng sữa trong ngày</Text>
            <Text type={'h4'} fontWeight={'medium'}>
              {dataFake.allMilks}
            </Text>
          </Block>
          <Block row justifyBetween center mt={16}>
            <Text type={'h4'}>Số lần uống trung bình/ngày</Text>
            <Text type={'h4'} fontWeight={'medium'}>
              {dataFake.averageOfDay}
            </Text>
          </Block>
          <Block row justifyBetween center mt={16}>
            <Text type={'h4'}>Lượng sữa trung bình/lần uống</Text>
            <Text type={'h4'} fontWeight={'medium'}>
              {dataFake.averageOfMilks}
            </Text>
          </Block>
          <Text type={'h4'} color={ColorsDefault.gray3} mt={16}>
            {dataFake.suggest}
          </Text>
        </Block>

        <Block
          borderBLRadius={12}
          borderBRRadius={12}
          row
          justifyBetween
          center
          mt={4}
          bg={ColorsDefault.purple1}
          ph={s(12)}
          mh={16}
          pv={16}>
          <Text type={'h4'}>{'Lượng sữa hôm qua bé uống'}</Text>
          <Text type={'h4'} fontWeight={'medium'}>
            {dataFake.averageOfDay}
          </Text>
        </Block>

        <Text mt={24} type={'h4'} fontWeight={'medium'} mh={16}>
          {'Chọn loại sữa cho bé'}
        </Text>
        <Text mt={8} type={'h5'} mh={16} color={ColorsDefault.gray3}>
          {
            'Chúng tôi sẽ dựa vào loại sữa bạn chọn để đưa ra gợi ý lượng sữa bé có thể uống trong ngày'
          }
        </Text>
      </Block>
    );
  };

  const renderItem = ({item}: any) => {
    return (
      <Block
        borderWidth={1}
        mt={16}
        pt={16}
        mh={16}
        borderRadius={6}
        borderColor={ColorsDefault.gray1}>
        <Touchable
          absolute
          zIndex={10}
          right={12}
          top={12}
          onPress={() => onSelectItem(item)}>
          <Image
            source={item?.isSelected ? images.checkboxActive : images.checkbox}
            width={20}
            height={20}
            resizeMode={'contain'}></Image>
        </Touchable>
        <Block row>
          <Image width={143} height={113} source={item.image}></Image>
          <Block ml={8} width={WIDTH_DEVICE - 32 - 16 - 143}>
            <Text mt={12} type={'h4'} fontWeight={'medium'}>
              {item.name}
            </Text>
            <Text type={'h5'} color={ColorsDefault.blue1}>
              {'295.000đ'}
            </Text>
            <Text mt={8} type={'h5'} color={ColorsDefault.gray3}>
              {item.content}
            </Text>
          </Block>
        </Block>
        <Block height={1} bg={ColorsDefault.gray1} mt={16} mh={16} />
        <Block row middle justifyBetween mh={16} pv={12}>
          <Text type={'h5'} color={ColorsDefault.black3} fontWeight={'medium'}>
            {'Đánh giá ngay'}
          </Text>
          <RateStar />
        </Block>
      </Block>
    );
  };

  return (
    <Body
      bg={ColorsDefault.white}
      loading={false}
      showsVerticalScrollIndicator={false}>
      <List
        data={listMilk}
        contentContainerStyle={{paddingBottom: 64}}
        ListHeaderComponent={renderHeader}
        renderItem={renderItem}
        showsVerticalScrollIndicator={false}
      />
    </Body>
  );
};

const styles = ScaledSheet.create({
  bgInfoBaby: {
    width: FULL_WIDTH_DEVICE - s(32),
    alignSelf: 'center',
    shadowColor: ColorsDefault.black,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3,
    marginTop: '16@vs',
    paddingHorizontal: '12@s',
    paddingVertical: '16@vs',
  },
});

export default Milks;
