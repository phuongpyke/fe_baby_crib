import React, {useEffect, useState} from 'react';
import {Image, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import RenderHtml, {defaultSystemFonts} from 'react-native-render-html';
import {SafeAreaView} from 'react-native-safe-area-context';
import {ScaledSheet} from 'react-native-size-matters';
import StaticSafeAreaInsets from 'react-native-static-safe-area-insets';
import reactotron from 'reactotron-react-native';
import {getVaccineDetails} from '/api/modules/vaccine';
import {ColorsDefault, FontFamilyDefault, FontSizeDefault} from '/assets';
import images from '/assets/images';
import {FULL_WIDTH_DEVICE} from '/common';
import {Header} from '/components/common';
import {navigate} from '/navigations';
import {Routes} from '/navigations/Routes';
import {EStatusInject} from '/utils/enum';
import {tagsStyles} from '/utils/helper';

const vaccineRequired: Record<number, string> = {
  0: 'Không',
  1: 'Có',
};

const VaccinDetail = ({route}: any) => {
  const {id, type, headerTitle, baby} = route.params ?? {};

  const [details, setDetails] = useState<any>({});

  const goUpdateVaccineSchedule = () =>
    navigate(Routes.UpdateVaccinSchedule, {
      babyId: baby.id,
      vaccineId: id,
      numberIndex: details.number,
    });

  useEffect(() => {
    const getData = async () => {
      try {
        const res: any = await getVaccineDetails(id);
        if (res?.success) {
          setDetails(res?.data);
        }
      } catch (err) {
        reactotron.warn!(err);
      }
    };
    getData();
  }, []);

  return (
    <SafeAreaView style={styles.container} edges={['right', 'left', 'top']}>
      <Header title={headerTitle} customStyle={styles.viewHeader} />
      <Image
        source={images.headerVaccin}
        style={styles.bgHeader}
        resizeMode={'stretch'}
      />
      <ScrollView
        contentContainerStyle={styles.contentScroll}
        showsVerticalScrollIndicator={false}>
        {type === EStatusInject.INJECTED && (
          <TouchableOpacity
            style={styles.btn}
            onPress={goUpdateVaccineSchedule}>
            <Text style={styles.textBtn}>{'Cập nhật ngay'}</Text>
          </TouchableOpacity>
        )}
        <View style={styles.viewHorizon}>
          <Text style={styles.textLeft}>{'Tiêm phòng'}</Text>
          <View style={styles.viewRight}>
            <Text style={styles.textRight}>{details?.title}</Text>
          </View>
        </View>
        <View style={styles.viewHorizon}>
          <Text style={styles.textLeft}>{'Bé'}</Text>
          <View style={styles.viewRight}>
            <Text style={styles.textRight}>{baby?.name}</Text>
          </View>
        </View>
        <View style={styles.viewHorizon}>
          <Text style={styles.textLeft}>{'Thời gian'}</Text>
          <View style={styles.viewRight}>
            <Text style={styles.textRight}>{details?.time_inject}</Text>
          </View>
        </View>
        <View style={styles.viewHorizon}>
          <Text style={styles.textLeft}>{'Trạng thái'}</Text>
          <View style={styles.viewRight}>
            <Text style={styles.textRight}>
              <Text style={[styles.textRight, {color: ColorsDefault.green2}]}>
                {'Đã tiêm'}
              </Text>{' '}
              {' (27/04/2022)'}
            </Text>
          </View>
        </View>
        <View style={styles.viewHorizon}>
          <Text style={styles.textLeft}>{'Đến lịch\ntiêm'}</Text>
          <View style={styles.viewRight}>
            <Text style={styles.textRight}>{'Mũi 1'}</Text>
          </View>
        </View>
        <View style={styles.viewHorizon}>
          <Text style={styles.textLeft}>{'Bệnh'}</Text>
          <View style={styles.viewRight}>
            <Text style={styles.textRight}>{details?.desc_disease}</Text>
          </View>
        </View>

        <View style={styles.viewHorizon}>
          <Text style={styles.textLeft}>{'Tên vacxin'}</Text>
          <View style={styles.viewRight}>
            <Text style={styles.textRight}>{details?.name}</Text>
          </View>
        </View>

        <View style={styles.viewHorizon}>
          <Text style={styles.textLeft}>{'Nội dung'}</Text>
          <View style={styles.viewRight}>
            {!!details?.schedule && (
              <RenderHtml
                contentWidth={FULL_WIDTH_DEVICE}
                source={{html: details?.schedule}}
                defaultTextProps={{
                  allowFontScaling: false,
                }}
                tagsStyles={tagsStyles}
                systemFonts={[...defaultSystemFonts]}
              />
            )}
          </View>
        </View>

        <View style={styles.viewHorizon}>
          <Text style={styles.textLeft}>{'Phản ứng phụ'}</Text>
          <View style={styles.viewRight}>
            <Text style={styles.textRight}>{details?.effect_more}</Text>
          </View>
        </View>

        <View style={styles.viewHorizon}>
          <Text style={styles.textLeft}>{'Bắt buộc?'}</Text>
          <View style={styles.viewRight}>
            <Text style={styles.textRight}>
              {vaccineRequired?.[details.require]}
            </Text>
          </View>
        </View>
        {type === EStatusInject.NOT_INJECTED && (
          <TouchableOpacity
            style={styles.btn}
            onPress={goUpdateVaccineSchedule}>
            <Text style={styles.textBtn}>{'Tiêm cho bé'}</Text>
          </TouchableOpacity>
        )}
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = ScaledSheet.create({
  container: {
    backgroundColor: ColorsDefault.white,
    flex: 1,
  },
  contentScroll: {
    paddingTop: '16@vs',
    paddingBottom: StaticSafeAreaInsets.safeAreaInsetsBottom,
    paddingHorizontal: '16@s',
  },
  bgHeader: {
    width: FULL_WIDTH_DEVICE,
    position: 'absolute',
    zIndex: -100,
  },
  viewHeader: {
    borderBottomWidth: 1,
    borderBottomColor: ColorsDefault.white,
  },
  viewHorizon: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginBottom: '16@vs',
  },
  btn: {
    backgroundColor: ColorsDefault.blue1,
    height: '52@s',
    width: '100%',
    borderRadius: '52@s',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: '16@vs',
  },
  textBtn: {
    fontSize: FontSizeDefault.FONT_16,
    fontFamily: FontFamilyDefault.primaryMedium,
    color: ColorsDefault.white,
  },
  textLeft: {
    fontSize: FontSizeDefault.FONT_16,
    fontFamily: FontFamilyDefault.primaryMedium,
    color: ColorsDefault.black3,
    width: '85@s',
    lineHeight: FontSizeDefault.FONT_25,
  },
  textRight: {
    fontSize: FontSizeDefault.FONT_16,
    fontFamily: FontFamilyDefault.primaryRegular,
    color: ColorsDefault.black3,
    lineHeight: FontSizeDefault.FONT_25,
  },
  viewRight: {
    flex: 1,
  },
});

export default VaccinDetail;
