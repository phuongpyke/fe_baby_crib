import React, {useEffect} from 'react';
import {ColorsDefault} from '/assets';
import {Body, Text, Input, Touchable} from '/components/base';
import {FormikProps, useFormik} from 'formik';
import {RouteProp, useNavigation} from '@react-navigation/native';
import {WIDTH_DEVICE} from '/common';
import {useDispatch, useSelector} from 'react-redux';

const DATE_FORMAT = 'DD/MM/YYYY';

interface Props {
  route: RouteProp<
    {
      params: {
        mode?: 'edit' | 'new';
      };
    },
    'params'
  >;
}

interface MyValues {
  name: string;
  expression: string;
  createAt: string;
  completeAt: string;
}

const validate = (values: any) => {
  const errors: any = {};
  if (!values.name) {
    errors.name = 'Hãy nhập tên bệnh';
  }
  if (!values.expression) {
    errors.expression = 'Hãy nhập triệu chứng';
  }
  if (!values.createAt) {
    errors.createAt = 'Hãy nhập ngày bị bệnh';
  }
  return errors;
};

const AddPatient = ({route}: Props) => {
  const navigation = useNavigation<any>();
  const dispatch = useDispatch();

  const formik: FormikProps<MyValues> = useFormik<MyValues>({
    initialValues: {
      name: '',
      expression: '',
      createAt: '',
      completeAt: '',
    },
    validate,
    onSubmit: () => {},
  });

  const onSave = async () => {};

  useEffect(() => {}, []);

  return (
    <Body
      scroll
      keyboardAvoid
      bg={ColorsDefault.white}
      showsVerticalScrollIndicator={false}
      loading={false}
      ph={16}>
      <Text mt={24} fontSize={14}>
        Tên bệnh
      </Text>
      <Input
        style={{
          borderWidth: 1,
          width: WIDTH_DEVICE - 32,
          borderRadius: 8,
          fontSize: 16,
          borderColor: ColorsDefault.gray1,
        }}
        mt={8}
        height={58}
        mb={5}
        value={formik.values.name}
        onChangeText={formik.handleChange('name')}
        placeholder="VD: Khóc dạ đề"
        error={formik.errors.name && formik.touched.name}
        errorMessage={formik.errors.name}
        onBlur={formik.handleBlur('name')}
      />
      <Text mt={16} fontSize={14}>
        Triệu chứng
      </Text>
      <Input
        style={{
          borderWidth: 1,
          width: WIDTH_DEVICE - 32,
          borderRadius: 8,
          fontSize: 16,
          borderColor: ColorsDefault.gray1,
        }}
        mt={8}
        height={58}
        mb={5}
        value={formik.values.expression}
        onChangeText={formik.handleChange('expression')}
        placeholder="VD: Ho, sốt"
        error={formik.errors.expression && formik.touched.expression}
        errorMessage={formik.errors.expression}
        onBlur={formik.handleBlur('expression')}
      />
      <Text mt={16} fontSize={14}>
        Ngày bị bệnh
      </Text>
      <Input
        style={{
          borderWidth: 1,
          width: WIDTH_DEVICE - 32,
          borderRadius: 8,
          fontSize: 16,
          borderColor: ColorsDefault.gray1,
        }}
        mt={8}
        height={58}
        mb={5}
        value={formik.values.createAt}
        onChangeText={formik.handleChange('createAt')}
        placeholder="VD: 03-02-2022"
        error={formik.errors.createAt && formik.touched.createAt}
        errorMessage={formik.errors.createAt}
        onBlur={formik.handleBlur('createAt')}
      />
      <Text mt={16} fontSize={14}>
        Ngày khỏi bệnh
      </Text>
      <Input
        style={{
          borderWidth: 1,
          width: WIDTH_DEVICE - 32,
          borderRadius: 8,
          fontSize: 16,
          borderColor: ColorsDefault.gray1,
        }}
        mt={8}
        height={58}
        mb={5}
        value={formik.values.completeAt}
        onChangeText={formik.handleChange('completeAt')}
        placeholder="VD: 03-02-2022"
        error={formik.errors.completeAt && formik.touched.completeAt}
        errorMessage={formik.errors.completeAt}
        onBlur={formik.handleBlur('completeAt')}
      />

      <Touchable
        onPress={onSave}
        middle
        mt={40}
        mb={64}
        disabled={
          !(
            !formik.errors.name &&
            !formik.errors.expression &&
            !formik.errors.createAt &&
            formik.values.name &&
            formik.values.expression &&
            formik.values.createAt
          )
        }
        bg={
          !(
            !formik.errors.name &&
            !formik.errors.expression &&
            !formik.errors.createAt &&
            formik.values.name &&
            formik.values.expression &&
            formik.values.createAt
          )
            ? ColorsDefault.gray2
            : ColorsDefault.blue1
        }
        center
        borderRadius={6}
        height={53}>
        <Text fontSize={16} color={ColorsDefault.white} fontWeight={'medium'}>
          Hoàn thành
        </Text>
      </Touchable>
    </Body>
  );
};

export default AddPatient;
