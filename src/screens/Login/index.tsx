import {useNavigation} from '@react-navigation/native';
import {FormikProps, useFormik} from 'formik';
import React, {useState} from 'react';
import {StyleSheet} from 'react-native';
import {getStatusBarHeight} from 'react-native-iphone-x-helper';
import StaticSafeAreaInsets from 'react-native-static-safe-area-insets';
import Toast from 'react-native-toast-message';
import {useDispatch, useSelector} from 'react-redux';
import {ColorsDefault, FontFamilyDefault} from '/assets';
import images from '/assets/images';
import {WIDTH_DEVICE} from '/common';
import {Block, Body, Image, Input, Text, Touchable} from '/components/base';
import {loadingLoginSelector} from '/modules/auth/selectors';
import {onLogin} from '/modules/auth/slice';
import {Routes} from '/navigations/Routes';

interface MyValues {
  phone: string;
  password: string;
}

const validate = (values: any) => {
  const errors: any = {};
  if (!values.phone) {
    errors.phone = 'Hãy nhập số điện thoại';
  }
  if (values.phone.length !== 10) {
    errors.phone = 'Số điện thoại không hợp lệ';
  }

  if (!values.password) {
    errors.password = 'Hãy nhập mật khẩu';
  }
  return errors;
};

const Login = () => {
  const navigation = useNavigation<any>();
  const [isShowPass, setIsShowPass] = useState(false);
  const dispatch = useDispatch();
  const loading = useSelector(loadingLoginSelector);

  const formik: FormikProps<MyValues> = useFormik<MyValues>({
    initialValues: {
      phone: __DEV__ ? '0813906645' : '',
      password: __DEV__ ? '12345678' : '',
    },
    validate,
    onSubmit: () => {},
  });

  const handleLogin = (response: any) => {
    if (response.babies.length === 0) {
      return navigation.navigate(Routes.MainStack, {
        screen: Routes.EditProfile,
        params: {mode: 'new'},
      });
    }
    return navigation.navigate(Routes.MainStack, {
      screen: Routes.Dashboard,
    });
  };

  const goHome = () => {
    dispatch(
      onLogin({
        data: {
          phone: formik.values.phone,
          password: formik.values.password,
        },
        onSuccess: (response: any) => {
          handleLogin(response);
        },
        onError: (error: any) => {
          Toast.show({
            type: 'custom',
            text1: `${error?.message}`,
            props: {
              type: 'error',
            },
          });
        },
      }),
    );
  };

  const goRegister = () => {
    navigation.navigate(Routes.Register);
  };

  const goForgotPassword = () => {
    navigation.navigate(Routes.ForgotPassword);
  };

  return (
    <Body
      scroll
      keyboardAvoid
      bg={ColorsDefault.white}
      showsVerticalScrollIndicator={false}
      loading={loading}>
      <Image
        source={images.headerLogin}
        style={styles.logo}
        mt={getStatusBarHeight()}
      />
      <Block mh={16}>
        <Text fontWeight={'bold'} type="h2" textCenter mt={16}>
          Chào mừng bạn!
        </Text>
        <Block center middle>
          <Input
            style={{
              borderWidth: 1,
              width: WIDTH_DEVICE - 32,
              borderRadius: 60,
              fontSize: 16,
              borderColor: ColorsDefault.gray1,
            }}
            mt={16}
            height={58}
            mb={5}
            value={formik.values.phone}
            onChangeText={formik.handleChange('phone')}
            placeholder="Số điện thoại"
            error={formik.errors.phone && formik.touched.phone}
            errorMessage={formik.errors.phone}
            onBlur={formik.handleBlur('phone')}
            keyboardType={'number-pad'}
          />
          <Input
            mt={15}
            value={formik.values.password}
            error={formik.errors.password && formik.touched.password}
            errorMessage={formik.errors.password}
            onChangeText={formik.handleChange('password')}
            style={{
              borderWidth: 1,
              width: WIDTH_DEVICE - 32,
              borderRadius: 60,
              fontSize: 16,
              borderColor: ColorsDefault.gray1,
              fontFamily: FontFamilyDefault.primaryRegular,
            }}
            onBlur={formik.handleBlur('password')}
            height={58}
            iconRightPress={() => {
              setIsShowPass(!isShowPass);
            }}
            iconRight={isShowPass ? images.show : images.hide}
            secureTextEntry={!isShowPass}
            placeholder="Mật khẩu"
          />
        </Block>
        <Touchable alignItemsEnd onPress={goForgotPassword} mt={8}>
          <Text fontWeight={'medium'} underline color={ColorsDefault.black3}>
            Quên mật khẩu?
          </Text>
        </Touchable>
        <Touchable
          onPress={goHome}
          disabled={
            !(
              !formik.errors.password &&
              !formik.errors.phone &&
              formik.values.phone &&
              formik.values.password
            )
          }
          middle
          mt={20}
          center
          borderRadius={60}
          bg={
            !(
              !formik.errors.password &&
              !formik.errors.phone &&
              formik.values.phone &&
              formik.values.password
            )
              ? ColorsDefault.gray2
              : ColorsDefault.blue1
          }
          height={53}>
          <Text type="p" color={ColorsDefault.white} fontWeight={'medium'}>
            Đăng nhập
          </Text>
        </Touchable>
        <Block
          row
          mt={120}
          mb={StaticSafeAreaInsets.safeAreaInsetsBottom + 20}
          middle
          center>
          <Text
            fontWeight={'medium'}
            type="h4"
            color={ColorsDefault.black3}
            onPress={goRegister}>
            Bạn chưa có tài khoản cho bé?{' '}
            <Text type="h4" color={ColorsDefault.blue1}>
              Đăng ký
            </Text>
          </Text>
        </Block>
      </Block>
    </Body>
  );
};

export default Login;

const styles = StyleSheet.create({
  logo: {
    width: WIDTH_DEVICE,
    height: 274,
  },
});
