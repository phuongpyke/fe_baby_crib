import React, {useState} from 'react';
import {LayoutAnimation, ScrollView} from 'react-native';
import StaticSafeAreaInsets from 'react-native-static-safe-area-insets';
import {ColorsDefault} from '/assets';
import images from '/assets/images';
import {WIDTH_DEVICE} from '/common';
import {Block, Image, Text, Touchable} from '/components/base';
import {TextDetail, WatchVideo} from '/components/common';
import {hitSlop} from '/utils/helper';

const dataFake = [
  {
    id: 0,
    title: 'Phát triển vận động',
    answers: [
      {
        id: 0,
        name: 'Bé đã biết giữ vững được cổ',
        detail:
          'Theo các chuyên gia nhi khoa, mức độ cứng cổ của trẻ phụ thuộc vào từng giai đoạn phát triển của cơ cổ. Mỗi em bé sẽ có cơ cổ khác nhau và tốc độ phát triển khác nhau nên các bố mẹ không nên so sánh con mình với người khác.',
      },
      {
        id: 1,
        name: 'Đưa mắt nhìn mẹ, mỉm cười',
        detail:
          'Nụ cười theo phản xạ sẽ biến mất trước khi bé được 2 tháng tuổi và nụ cười thực sự sẽ xuất hiện khi bé được khoảng 1,5 đến 3 tháng (từ 6 đến 12 tuần).',
      },
    ],
  },
  {
    id: 1,
    title: 'Phát triển ngôn ngữ',
    answers: [
      {
        id: 0,
        name: 'Phát ra âm oo, a, oh khi hóng chuyện, reo cười thành tiếng',
        detail:
          'Trò chuyện với bé thường xuyên còn mang đến nhiều lợi ích về sự phát triển ngôn ngữ của bé sau này.',
      },
      {
        id: 1,
        name: 'Cười đáp lại, mỉm cười tự nhiên, hóng chuyện, nhăn mặt chăm chú',
        detail:
          'Trò chuyện với bé thường xuyên còn mang đến nhiều lợi ích về sự phát triển ngôn ngữ của bé sau này.',
      },
    ],
  },
];

const videoData = [
  {
    title: 'Tắm cho bé như thế nào là đúng?',
    id: '0ihlsYD5AZA',
  },
  {
    title: 'Môi trường sống ảnh hưởng đến sự phát triển của bé như thế nào?',
    id: '-vvgeNbL2x0',
  },
  {
    title: 'Những trò chơi kích thích sự phát triển của bé',
    id: 'IvUu87UhnUI',
  },
];

const BabyCare = () => {
  const [listQuestion, setListQuestion] = useState<any[]>(dataFake);

  const onChangeValue = (questionId: number, answerId: number) => {
    const list = listQuestion.map((question: any) => {
      if (question.id === questionId) {
        const answers = question.answers?.map((value: any) => {
          if (value.id === answerId) {
            return {...value, isSelected: !value?.isSelected};
          }
          return value;
        });

        return {...question, answers};
      }
      return question;
    });
    setListQuestion(list);
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
  };

  const renderItem = (item: any) => {
    return (
      <Block mt={32} mh={16} key={item.id}>
        <Text type={'h4'} fontWeight={'medium'}>
          {item.title}
        </Text>
        {item?.answers?.map((answers: any, index: number) => {
          return (
            <Block row alignItemsStart mt={12} key={answers.id}>
              <Touchable
                hitSlop={hitSlop(15)}
                onPress={() => onChangeValue(item?.id, answers.id)}
                height={16}
                width={16}
                mt={3}
                middle
                center
                bg={
                  answers?.isSelected
                    ? ColorsDefault.blue1
                    : ColorsDefault.white
                }
                borderWidth={1}
                borderRadius={2}
                borderColor={ColorsDefault.blue1}>
                <Image source={images.check} width={12} height={12} />
              </Touchable>
              <Block flex={1} ml={8}>
                <Text fontSize={16} color={ColorsDefault.black3}>
                  {answers?.name}
                </Text>
                {!!answers?.isSelected && (
                  <Text fontSize={14} color={ColorsDefault.black3}>
                    {answers?.detail}
                  </Text>
                )}
              </Block>
            </Block>
          );
        })}
      </Block>
    );
  };

  return (
    <ScrollView
      style={{
        flex: 1,
        backgroundColor: ColorsDefault.white,
      }}
      contentContainerStyle={{
        paddingBottom: StaticSafeAreaInsets.safeAreaInsetsBottom,
      }}>
      <TextDetail title="AI dựa theo tháng tuổi để đưa ra gợi ý về các mốc đánh dấu sự phát triển của bé. " />
      <Block mh={16}>
        <Text fontSize={16} color={ColorsDefault.blue1} mt={32}>
          Bé được 2 tháng 10 ngày
        </Text>
        <Block row mt={32} width={WIDTH_DEVICE - 32} flex={1} justifyBetween>
          <Block row middle justifyAround flex={1}>
            <Block>
              <Text fontSize={16}>0</Text>
              <Block height={46} center>
                <Block
                  width={12}
                  height={12}
                  borderRadius={12}
                  bg={ColorsDefault.blue1}></Block>
              </Block>
            </Block>
            <Block
              width={'40%'}
              mt={16}
              height={1}
              bg={ColorsDefault.gray}></Block>
            <Block center middle>
              <Text fontSize={16}>1</Text>
              <Block
                width={46}
                height={46}
                borderRadius={46}
                borderWidth={1}
                center
                middle>
                <Image source={images.babyBoy1} width={32} height={32} />
              </Block>
            </Block>
          </Block>

          <Block row middle flex={1} justifyBetween>
            <Block
              width={'55%'}
              mt={16}
              height={1}
              bg={ColorsDefault.gray}></Block>
            <Block center middle>
              <Text fontSize={16}>3</Text>
              <Block
                width={46}
                height={46}
                borderRadius={46}
                borderWidth={1}
                center
                middle>
                <Image source={images.babyBoy2} width={32} height={32} />
              </Block>
            </Block>
          </Block>

          <Block row middle flex={1} justifyBetween>
            <Block
              width={'55%'}
              mt={16}
              height={1}
              bg={ColorsDefault.gray}></Block>
            <Block center middle>
              <Text fontSize={16}>8</Text>
              <Block
                width={46}
                height={46}
                borderRadius={46}
                borderWidth={1}
                center
                middle>
                <Image source={images.babyBoy1} width={32} height={32} />
              </Block>
            </Block>
          </Block>

          <Block row middle width={54} justifyBetween>
            <Block
              width={'80%'}
              mt={16}
              height={1}
              bg={ColorsDefault.gray}></Block>
            <Block center middle>
              <Text fontSize={16}>8</Text>
              <Block height={46} center>
                <Block
                  width={12}
                  height={12}
                  borderRadius={12}
                  bg={ColorsDefault.blue1}></Block>
              </Block>
            </Block>
          </Block>
        </Block>
      </Block>
      {listQuestion.map((item, index) => renderItem(item))}
      <Block
        borderRadius={12}
        bg={ColorsDefault.pink2}
        mh={16}
        pv={12}
        ph={8}
        mt={24}>
        <Text color={ColorsDefault.orange2} type={'h4'} fontWeight={'medium'}>
          {'Chú ý'}
        </Text>
        <Text type={'h5'} color={ColorsDefault.black3}>
          {
            'Tips chăm sóc bé (nếu bé bị bệnh thì hiển thị những cái tránh cho bé)'
          }
        </Text>
      </Block>
      <Text
        type={'h4'}
        fontWeight={'medium'}
        color={ColorsDefault.black3}
        mh={16}
        mb={-12}
        mt={24}>
        {'Bài viết liên quan'}
      </Text>
      {videoData.map((item, index) => (
        <WatchVideo key={index} videoId={item.id} title={item.title} />
      ))}
    </ScrollView>
  );
};

export default BabyCare;
