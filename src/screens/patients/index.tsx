import React, { useLayoutEffect, useState } from 'react';
import { StyleSheet } from 'react-native';
import Toast from 'react-native-toast-message';
import { useNavigation } from '@react-navigation/native';
import {
    Body,
    Image,
    Text,
    Block,
    Input,
    Touchable,
    List,
} from '/components/base';
import { ColorsDefault } from '/assets';
import { WIDTH_DEVICE } from '/common';
import { Routes } from '/navigations/Routes';
import images from "/assets/images";


const list = [
    {
        id: 1,
        name: 'Bé bị bệnh khóc dạ đề',
        time: '22.04.2022',
    },
    {
        id: 2,
        name: 'Bé đi tiêm chủng ',
        time: '22.04.2022',
    },
]

const Patients = () => {
    const navigation = useNavigation<any>();
    const goPatients = () => {
        navigation.navigate(Routes.PatientDetail)
    }
    const goAddPatients = () => {
        navigation.navigate(Routes.AddPatient)
    }
    useLayoutEffect(() => {
        navigation.setOptions({
            headerRight: () =>
                <Touchable onPress={goAddPatients}>
                    <Image source={images.calendar2} width={24} height={24}></Image>
                </Touchable>
        })
    }, [navigation])

    const renderHeader = () => {
        return (
            <>
                <Block row justifyBetween middle pv={16}>
                    <Text fontSize={16} fontWeight={'medium'}>Thời gian</Text>
                    <Text fontSize={16} fontWeight={'medium'}>Nội dung</Text>
                    <Block width={'33%'}></Block>
                </Block>
                <Block height={1} bg={ColorsDefault.gray1}></Block>
            </>
        );
    };

    const renderItem = ({ item }: any) => {
        return (
            <>
                <Touchable onPress={goPatients} pv={16} row justifyBetween >
                    <Block width={'30%'}>
                        <Text fontSize={16}>{item.time}</Text>
                    </Block>
                    <Block width={'50%'}  >
                        <Text fontSize={16} fontWeight={'medium'} >{item.name}</Text>
                    </Block>
                    <Block width={'20%'} alignItemsEnd>
                        <Text fontSize={14} color={ColorsDefault.blue1}>Chi tiết</Text>
                    </Block>
                </Touchable>
                <Block height={1} bg={ColorsDefault.gray1}></Block>
            </>
        );
    };

    return (
        <Body
            bg={ColorsDefault.white}
            loading={false}
            showsVerticalScrollIndicator={false}>
            <List
                data={list}
                contentContainerStyle={{ paddingHorizontal: 16, paddingBottom: 64 }}
                ListHeaderComponent={renderHeader}
                renderItem={renderItem}
            />
        </Body>
    );
};

export default Patients;
