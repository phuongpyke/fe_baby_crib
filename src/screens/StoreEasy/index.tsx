import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {Image, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {ScaledSheet, vs} from 'react-native-size-matters';
import StaticSafeAreaInsets from 'react-native-static-safe-area-insets';
import {ColorsDefault, FontFamilyDefault, FontSizeDefault} from '/assets';
import images from '/assets/images';
import {FULL_WIDTH_DEVICE} from '/common';
import {TitleDash} from '/components/common';
import {Routes} from '/navigations/Routes';

const easyMom = [
  {
    id: 0,
    title: 'E.A.S.Y - 1',
    sub_title: 'Từ 0-2 tuần',
  },
  {
    id: 1,
    title: 'E.A.S.Y - 2',
    sub_title: 'Từ 2-4 tuần',
  },
  {
    id: 2,
    title: 'E.A.S.Y - 3',
    sub_title: 'Từ 4-6 tuần',
  },
];

const easyPopular = [
  {
    id: 0,
    title: 'E.A.S.Y. ',
    version: '2.5.v1',
    sub_title: 'Từ 0-2 tuần',
  },
  {
    id: 1,
    title: 'E.A.S.Y. ',
    version: '2.5.v2',
    sub_title: 'Từ 2-4 tuần',
  },
  {
    id: 2,
    title: 'E.A.S.Y. ',
    version: '3',
    sub_title: 'Từ 4-8 tuần',
  },
  {
    id: 3,
    title: 'E.A.S.Y. ',
    version: '4',
    sub_title: 'Từ 8-19 tuần',
  },
  {
    id: 4,
    title: 'E.A.S.Y. ',
    version: '2-3-4',
    sub_title: 'Từ 19-46 tuần',
  },
  {
    id: 5,
    title: 'E.A.S.Y. ',
    version: '5-6',
    sub_title: 'Từ 46 tuần trở đi',
  },
];

const StoreEasy = () => {
  const navigation = useNavigation();

  const goBack = () => {
    navigation.goBack();
  };

  const goEasyDetail = () => {
    navigation.navigate(Routes.EasyDetail);
  };

  const renderItem = (item: any, index: number) => (
    <TouchableOpacity style={styles.btnItem} key={index} onPress={goEasyDetail}>
      <Text style={styles.textLeftItem}>
        {item.title}
        {!!item.version && (
          <Text style={{color: ColorsDefault.orange2}}>{item?.version}</Text>
        )}
      </Text>
      <View style={styles.viewRightItem}>
        <Text style={styles.textRightItem}>{item.sub_title}</Text>
        <Image source={images.rightArrow} style={{width: 16, height: 16}} />
      </View>
    </TouchableOpacity>
  );

  return (
    <ScrollView
      style={styles.container}
      contentContainerStyle={{
        paddingBottom: StaticSafeAreaInsets.safeAreaInsetsBottom,
      }}
      bounces={false}
      showsVerticalScrollIndicator={false}>
      <View style={styles.wrapHeader}>
        <Image source={images.backgroundStoreEasy} style={styles.bgHeader} />
        <View style={styles.viewHeader}>
          <TouchableOpacity style={styles.btnBack} onPress={goBack}>
            <Image source={images.backLeft} style={styles.icBackHeader} />
          </TouchableOpacity>
          <Text style={styles.textHeader}>{'Giai đoạn'}</Text>
        </View>
        <Text style={styles.textSubTitle}>{'( Từ 6 - 8 tuần)'}</Text>
        <Text style={styles.textEasy}>
          {'E.A.S.Y.'} <Text style={{color: ColorsDefault.yellow5}}>{'3'}</Text>
        </Text>
        <TouchableOpacity style={styles.btnShowDetail}>
          <Text style={styles.textDetail}>{'Chi tiết'}</Text>
        </TouchableOpacity>
      </View>
      <TitleDash title={'KHO EASY CỦA MẸ'} customStyle={{marginTop: vs(24)}} />
      {easyMom.map((item, index) => renderItem(item, index))}
      <TouchableOpacity style={styles.btnSeeMore}>
        <Text style={styles.textSeeMore}>{'Xem thêm'}</Text>
        <Image source={images.bottomArrow} style={{height: 16, width: 16}} />
      </TouchableOpacity>
      <TitleDash title={'EASY PHỔ BIẾN'} customStyle={{marginTop: vs(24)}} />
      {easyPopular.map((item, index) => renderItem(item, index))}
    </ScrollView>
  );
};

const styles = ScaledSheet.create({
  container: {
    backgroundColor: ColorsDefault.white,
    flex: 1,
  },
  wrapHeader: {
    backgroundColor: ColorsDefault.blue1,
    paddingBottom: '20@vs',
    borderBottomRightRadius: 36,
    borderBottomLeftRadius: 36,
  },
  bgHeader: {
    width: FULL_WIDTH_DEVICE,
    position: 'absolute',
    top: StaticSafeAreaInsets.safeAreaInsetsTop,
  },
  viewHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: StaticSafeAreaInsets.safeAreaInsetsTop,
    height: '60@vs',
  },
  textHeader: {
    fontSize: FontSizeDefault.FONT_16,
    fontFamily: FontFamilyDefault.primaryBold,
    color: ColorsDefault.white,
  },
  icBackHeader: {
    height: 24,
    width: 24,
    tintColor: ColorsDefault.white,
  },
  btnBack: {
    position: 'absolute',
    left: '10@s',
  },
  textSubTitle: {
    fontSize: FontSizeDefault.FONT_16,
    fontFamily: FontFamilyDefault.primaryRegular,
    color: ColorsDefault.white,
    textAlign: 'center',
  },
  textEasy: {
    fontSize: FontSizeDefault.FONT_44,
    fontFamily: FontFamilyDefault.primarySemiBold,
    color: ColorsDefault.white,
    textAlign: 'center',
  },
  btnShowDetail: {
    alignSelf: 'center',
    marginTop: '5@vs',
  },
  textDetail: {
    fontSize: FontSizeDefault.FONT_16,
    fontFamily: FontFamilyDefault.primaryMedium,
    color: ColorsDefault.white,
    textDecorationLine: 'underline',
  },
  btnItem: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: '16@s',
    paddingHorizontal: '12@s',
    borderRadius: 12,
    height: '64@s',
    backgroundColor: ColorsDefault.white,
    marginTop: '16@vs',
    shadowColor: ColorsDefault.black2,
    shadowOffset: {
      width: 0.5,
      height: 1,
    },
    shadowOpacity: 0.2,
  },
  viewRightItem: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textRightItem: {
    fontSize: FontSizeDefault.FONT_16,
    fontFamily: FontFamilyDefault.primaryRegular,
    color: ColorsDefault.black3,
    marginRight: '10@s',
  },
  textLeftItem: {
    fontSize: FontSizeDefault.FONT_20,
    fontFamily: FontFamilyDefault.primaryBold,
    color: ColorsDefault.blue1,
  },
  btnSeeMore: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: '16@vs',
  },
  textSeeMore: {
    fontSize: FontSizeDefault.FONT_16,
    fontFamily: FontFamilyDefault.primaryRegular,
    color: ColorsDefault.black3,
    marginRight: '4@s',
  },
});

export default StoreEasy;
