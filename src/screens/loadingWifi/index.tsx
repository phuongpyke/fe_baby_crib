import React, {useEffect, useState} from 'react';
import {Image, Text} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {ColorsDefault, FontFamilyDefault, FontSizeDefault} from '/assets';
import images from '/assets/images';
import {FULL_HEIGHT_DEVICE, FULL_WIDTH_DEVICE} from '/common';
import {Loading} from '/components';
import {reset} from '/navigations';
import {Routes} from '/navigations/Routes';
import BackgroundTimer from 'react-native-background-timer';
import {s} from 'react-native-size-matters';

const LoadingWifi = () => {
  const [secondsLeft, setSecondsLeft] = useState<number>(180000);

  const startTimer = () => {
    BackgroundTimer.runBackgroundTimer(() => {
      setSecondsLeft(secs => {
        if (secs > 0) {
          return secs - 1;
        }
        return 0;
      });
    }, 1000);
  };

  // Runs when timerOn value changes to start or stop timer
  useEffect(() => {
    startTimer();

    return () => {
      BackgroundTimer.stopBackgroundTimer();
    };
  }, []);

  // Checks if secondsLeft = 0 and stop timer if so
  useEffect(() => {
    if (secondsLeft === 0) {
      BackgroundTimer.stopBackgroundTimer();
      reset([{name: Routes.Dashboard}]);
    }
  }, [secondsLeft]);

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: ColorsDefault.white}}>
      <Image
        source={images.logo}
        resizeMode={'contain'}
        style={{
          width: s(114),
          height: s(128),
          marginTop: FULL_HEIGHT_DEVICE * 0.1,
          alignSelf: 'center',
        }}
      />
      <Loading mt={16} ml={8} width={34} height={34} />
      <Text
        style={{
          fontSize: FontSizeDefault.FONT_16,
          fontFamily: FontFamilyDefault.primaryRegular,
          color: ColorsDefault.black3,
          marginHorizontal: 30,
          marginTop: 30,
          textAlign: 'center',
        }}>
        {'Hãy đợi 3 phút để '}
        <Text
          style={{
            fontFamily: FontFamilyDefault.primarySemiBold,
          }}>
          {'Babyai'}
        </Text>
        {' khởi động lại và kết nối với wifi của bạn.'}
      </Text>
      <Image
        source={images.footerRegister}
        style={{
          position: 'absolute',
          bottom: 0,
          right: 0,
          left: 0,
          zIndex: -10,
          width: FULL_WIDTH_DEVICE,
          height: FULL_WIDTH_DEVICE * 0.7,
        }}
      />
    </SafeAreaView>
  );
};

export default LoadingWifi;
