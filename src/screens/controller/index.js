import {useNavigation} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import {Platform, View} from 'react-native';
import BleManager from 'react-native-ble-manager';
import {getStatusBarHeight} from 'react-native-iphone-x-helper';
import SegmentedControlTab from 'react-native-segmented-control-tab';
import {useSelector} from 'react-redux';
import reactotron from 'reactotron-react-native';
import CribSettings from './crib';
import FollowInDay from './management';
import {styles} from './style';
import {ColorsDefault} from '/assets';
import {WIDTH_DEVICE} from '/common';
import {Block, Body, Text, Touchable} from '/components';
import {read} from '/config/BleController';
import {blueToothIdSelector, cribQrSelector} from '/modules/user/selectors';
import {Routes} from '/navigations/Routes';
import {Constants} from '/utils';

const iPwifiConnectCharactis = '00000011-373F-11EC-8D3D-0242AC130003';
const ipWifiService = '00000001-373F-11EC-8D3D-0242AC130003';

const wifiCharacteristic = '00000010-373F-11EC-8D3D-0242AC130003';
const wifiService = '00000001-373F-11EC-8D3D-0242AC130003';

// Components
const RenderQrView = () => {
  const navigation = useNavigation();

  const goQrScreen = () => {
    navigation.navigate(Routes.QrScreen);
  };

  return (
    <Block bg={ColorsDefault.white}>
      <Text
        mt={Platform.OS === 'android' ? 16 : getStatusBarHeight() + 16}
        textCenter
        fontWeight={'medium'}
        fontSize={20}
        lineHeight={28}>
        Nôi của bé
      </Text>
      <Block height={1} mt={16} bg={ColorsDefault.gray2} width={WIDTH_DEVICE} />
      <Text textCenter fontSize={16} lineHeight={22} mt={48}>
        Quét QR để active nôi của bé
      </Text>
      <Touchable
        onPress={goQrScreen}
        center
        middle
        height={53}
        bg={ColorsDefault.blue1}
        mh={16}
        borderRadius={6}
        mt={36}>
        <Text fontSize={16} color={ColorsDefault.white}>
          Quét mã QR
        </Text>
      </Touchable>
    </Block>
  );
};

const Crib = () => {
  const [selectedTab, setSelectedTab] = useState(0);
  const cribQr = useSelector(cribQrSelector);
  const bluetoothId = useSelector(blueToothIdSelector);
  const [isLoading, setIsLoading] = useState(true);
  const navigation = useNavigation();

  const handleTabChange = index => {
    setSelectedTab(index);
  };

  useEffect(() => {
    setIsLoading(true);
    if (bluetoothId) {
      BleManager.start({showAlert: false}).then(() => {
        setTimeout(() => {
          getStatusServices();
        }, 1000);
      });
    } else {
      setIsLoading(false);
      cribQr && navigation.navigate(Routes.ScanDevices);
    }
  }, []);

  const getStatusServices = () => {
    BleManager.connect(bluetoothId)
      .then(() => {
        setTimeout(() => {
          getIpWifi();
        }, 1000);
      })
      .catch(error => {
        reactotron.log({error});
      });
  };

  const getIpWifi = () => {
    let listWifi;
    BleManager.retrieveServices(bluetoothId).then(peripheralData => {
      peripheralData.characteristics.forEach(async item => {
        if (item.characteristic.toUpperCase() === wifiCharacteristic) {
          let stringWifi = await read(
            bluetoothId,
            wifiService,
            wifiCharacteristic,
          );
          reactotron.log({stringWifi});
          listWifi = stringWifi.split('_');
        }
        if (item.characteristic === iPwifiConnectCharactis) {
          const ipWifi = await read(
            bluetoothId,
            ipWifiService,
            iPwifiConnectCharactis,
          );
          reactotron.log({ipWifi});
          if (ipWifi) {
            setIsLoading(false);
          } else {
            setTimeout(() => {
              setIsLoading(false);
              reactotron.log({listWifi});
              navigation.navigate(Routes.Wifi, {
                listWifi: listWifi,
                peripheralId: bluetoothId,
              });
            }, 1000);
          }
        }
      });
    });
  };

  if (!cribQr) {
    return <RenderQrView />;
  }

  return (
    <Body style={styles.container} bg={ColorsDefault.white} loading={isLoading}>
      <Text
        textCenter
        fontSize={20}
        fontWeight={'medium'}
        lineHeight={26}
        mt={Platform.OS === 'android' ? 16 : getStatusBarHeight() + 16}>
        Nôi của bé
      </Text>
      <Block height={1} bg={ColorsDefault.gray1} mt={16}></Block>
      <View>
        <SegmentedControlTab
          values={[Constants.CribSettings, Constants.FollowInDay]}
          selectedIndex={selectedTab}
          onTabPress={handleTabChange}
          borderRadius={Constants.BorderRadius}
          tabStyle={styles.tabsContainer}
          tabTextStyle={styles.txtTab}
          activeTabStyle={{
            borderBottomColor: '#3E64FF',
            borderBottomWidth: 2,
            backgroundColor: ColorsDefault.white,
          }}
          activeTabTextStyle={{
            fontSize: Constants.txtSize,
            fontWeight: Constants.fontWeight,
            color: ColorsDefault.blue1,
          }}
        />
      </View>
      {selectedTab === 0 ? <CribSettings /> : <FollowInDay />}
    </Body>
  );
};

export default Crib;
