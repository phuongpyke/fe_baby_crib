import {StyleSheet, Dimensions} from 'react-native';
import {scale, verticalScale} from 'react-native-size-matters';
import {Color, Constants} from '../../utils';
import {ColorsDefault} from '/assets';
import {WIDTH_DEVICE} from '/common';

const {height, width} = Dimensions.get('window');
const rectDimensions = width * 0.65;
const scanBarWidth = width * 0.48;
const scanBarHeight = width * 0.0025;

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: ColorsDefault.white,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 81,
  },
  tabsWrapper: {
    padding: 16,
  },
  tabsContainer: {
    height: 48,
    borderBottomWidth: 1,
    borderColor: 'transparent',
    borderBottomColor: ColorsDefault.gray1,
  },
  txtTab: {
    fontSize: 16,
    color: ColorsDefault.black,
    fontWeight: '600',
  },
  // Crib Settings
  resultContainer: {
    width: '100%',
    height: verticalScale(80),
    borderRadius: Constants.borderRadius,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 16,
    paddingRight: 26,
  },
  status: {
    fontSize: 17,
    color: ColorsDefault.white,
    fontWeight: 'bold',
    paddingLeft: 12,
  },
  txtTemperature: {
    fontSize: 10,
    paddingBottom: 10,
  },
  textSm: {
    fontSize: 12,
    color: 'gray',
  },
  flexBetween: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  itemRow: {
    paddingLeft: 16,
    paddingRight: 11,
    flexDirection: 'row',
  },
  titleRemote: {
    fontSize: 24,
    fontWeight: 'bold',
    paddingVertical: 5,
  },
  img: {
    width: scale(150),
    height: verticalScale(150),
    marginTop: '5%',
    marginBottom: '10%',
  },
  btnAddCrib: {
    width: '50%',
    alignSelf: 'center',
    marginTop: 8,
  },
  // Follow The Day
  event: {
    marginBottom: 6,
    marginLeft: 0,
    position: 'relative',
    bottom: 30,
  },
  eventDetails: {
    flexDirection: 'row',
    padding: 8,
    borderRadius: Constants.borderRadius,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  eventDetails2: {
    paddingVertical: 14,
    paddingHorizontal: 16,
    borderRadius: Constants.borderRadius,
  },
  tabWrapper: {
    flexDirection: 'row',
    height: 28,
    marginBottom: 20,
    justifyContent: 'space-between',
  },
  chosenTab: {
    borderBottomWidth: 1,
    borderBottomColor: ColorsDefault.black,
  },
  txtBlack: {
    fontWeight: '600',
    fontSize: 16,
    color: ColorsDefault.black,
  },
  txtUnselectedTab: {
    fontSize: 16,
    color: '#828282',
  },

  // Modal
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#00000033',
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 15,
    paddingTop: 27,
    paddingBottom: 23,
    position: 'relative',
  },
  modalTitle: {
    fontSize: 20,
    fontWeight: 'bold',
    paddingLeft: 16,
  },
  close: {
    position: 'absolute',
    top: 20,
    right: 20,
  },
  modalItem: {
    padding: 16,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
  },
  // Qr code
  qrContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: ColorsDefault.transparent,
  },
  rectangle: {
    height: rectDimensions,
    width: rectDimensions,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: ColorsDefault.transparent,
  },
  topOverlay: {
    flex: 1,
    height: height,
    width: width,
  },
  appbar: {
    flexDirection: 'row',
    paddingTop: verticalScale(50),
    paddingHorizontal: scale(20),
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: verticalScale(30),
  },
  icon: {
    position: 'absolute',
    left: 0,
    top: verticalScale(28),
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    color: ColorsDefault.white,
  },
  txtDes: {
    fontSize: 16,
    color: ColorsDefault.white,
    alignSelf: 'center',
  },
  bottomOverlay: {
    flex: 1,
    height: width,
    width: width,
    paddingBottom: width * 0.25,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: scale(60),
  },
  leftAndRightOverlay: {
    height: width * 0.65,
    width: width,
  },
  scanBar: {
    width: scanBarWidth,
    height: scanBarHeight,
    backgroundColor: ColorsDefault.red,
  },
  bottomItem: {
    paddingTop: verticalScale(80),
  },
  txtDes2: {
    color: ColorsDefault.white,
    fontSize: 16,
    paddingTop: verticalScale(8),
  },
  errText: {
    color: '#EB5757',
    paddingTop: 6,
    paddingLeft: 15,
    alignSelf: 'flex-start',
  },
  registerAction: {
    alignSelf: 'flex-end',
    marginTop: 16,
    flexDirection: 'row',
  },
  txtRegister: {
    fontWeight: Constants.fontWeight,
    color: ColorsDefault.blue1,
    paddingLeft: 2,
  },
});
