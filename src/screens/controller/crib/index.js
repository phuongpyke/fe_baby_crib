import {useIsFocused, useNavigation} from '@react-navigation/native';
import {stringToBytes} from 'convert-string';
import React, {useState} from 'react';
import {Image, View} from 'react-native';
import BleManager from 'react-native-ble-manager';
import {Block, Switch, Text} from '../../../components';
import {getObjData} from '../../../config/AsyncStorage';
import {read, write} from '../../../config/BleController';
import {Constants} from '../../../utils';
import {styles} from '../style';
import images from '/assets/images';

const CribSettings = () => {
  const [auto, setAuto] = useState(false);
  const [vibrate, setVibrate] = useState(false);
  const [light, setLight] = useState(false);
  const [music, setMusic] = useState(false);

  const [vibrateSer, setVibrateSer] = useState(null);
  const [lightSer, setLightSer] = useState(null);
  const [musicSer, setMusicSer] = useState(null);

  const [cribID, setCribID] = useState('');
  const [wifi, setWifi] = useState([]);
  const isFocused = useIsFocused();
  const navigation = useNavigation();

  // useEffect(() => {
  //   if (isFocused) {
  //     BleManager.start();
  //     getStatusServices();
  //   }
  // }, [isFocused]);

  // Functions
  const getStatusServices = async () => {
    const device = await getObjData('crib');
    setCribID(device.id);
    if (device) {
      BleManager.connect(device.id)
        .then(() => {
          setTimeout(() => {
            getRetrieveServices(device.id);
          }, 500);
        })
        .catch(() => {
          getStatusServices();
        });
    }
  };

  const getRetrieveServices = id => {
    BleManager.retrieveServices(id)
      .then(peripheralData => {
        console.log('Retrieved peripheral services', peripheralData);
        peripheralData.characteristics.forEach(async item => {
          if (item.characteristic === '00000002-373F-11EC-8D3D-0242AC130003') {
            var status = await read(cribID, item.service, item.characteristic);
            setVibrate(status === 'on' ? true : false);
          }
          if (item.characteristic === '00000004-373F-11EC-8D3D-0242AC130003') {
            var status = await read(cribID, item.service, item.characteristic);
            setLight(status === 'on' ? true : false);
          }
          if (item.characteristic === '00000006-373F-11EC-8D3D-0242AC130003') {
            var status = await read(cribID, item.service, item.characteristic);
            setMusic(status === 'on' ? true : false);
          }
          if (
            item.characteristic ===
            '00000003-373f-11ec-8d3d-0242ac130003'.toUpperCase()
          ) {
            setVibrateSer(item);
          }
          if (
            item.characteristic ===
            '00000005-373f-11ec-8d3d-0242ac130003'.toUpperCase()
          ) {
            setLightSer(item);
          }
          if (
            item.characteristic ===
            '00000007-373f-11ec-8d3d-0242ac130003'.toUpperCase()
          ) {
            setMusicSer(item);
          }
        });
      })
      .catch(error => {
        console.log(error);
      });
  };

  const toggleRemote = type => {
    if (type === 'auto') {
      setAuto(!auto);
    } else if (type === 'vibrate') {
      if (vibrate) {
        write(
          cribID,
          vibrateSer.service,
          vibrateSer.characteristic,
          stringToBytes('off'),
        );
        setVibrate(false);
      } else {
        write(
          cribID,
          vibrateSer.service,
          vibrateSer.characteristic,
          stringToBytes('on'),
        );
        setVibrate(true);
      }
    } else if (type === 'music') {
      if (music) {
        write(
          cribID,
          musicSer.service,
          musicSer.characteristic,
          stringToBytes('off'),
        );
        setMusic(false);
      } else {
        write(
          cribID,
          musicSer.service,
          musicSer.characteristic,
          stringToBytes('on'),
        );
        setMusic(true);
      }
    } else if (type === 'light') {
      if (light) {
        write(
          cribID,
          lightSer.service,
          lightSer.characteristic,
          stringToBytes('off'),
        );
        setLight(false);
      } else {
        write(
          cribID,
          lightSer.service,
          lightSer.characteristic,
          stringToBytes('on'),
        );
        setLight(true);
      }
    }
  };

  // Components
  const ResultAI = () => {
    return (
      <View style={[styles.resultContainer, {backgroundColor: 'red'}]}>
        <View style={styles.row}>
          <Image source={images.crying} />
          <Text style={styles.status}>{Constants.Crying}</Text>
        </View>
        <View style={styles.row}>
          <Image source={images.thermometer} style={{paddingRight: 4}} />
          <Text>36</Text>
          <Text style={styles.txtTemperature}>o</Text>
          <Text>C</Text>
        </View>
      </View>
    );
  };

  const AutoRemote = () => {
    return (
      <View style={styles.autoRemote}>
        <View>
          <Text style={[styles.text, {paddingBottom: 5}]}>
            {Constants.AutoTurnOn}
          </Text>
          <Text style={styles.textSm}>{Constants.AutoTurnOn1}</Text>
          <Text style={styles.textSm}>{Constants.AutoTurnOn2}</Text>
        </View>
        <Switch value={auto} onChange={() => toggleRemote('auto')} />
      </View>
    );
  };

  const ItemRemote = props => {
    return (
      <View style={styles.itemRemote}>
        <View style={styles.flexBetween}>
          {props.children}
          <Switch value={props.switch} onChange={props.handleSwitch} />
        </View>
        <Text style={styles.titleRemote}>{props.title}</Text>
      </View>
    );
  };

  return (
    <Block>
      <Block row center middle mt={12}>
        <Text textCenter fontSize={14}>
          Tình trạng kết nối nôi:
        </Text>
        <Image source={images.connect} style={{marginHorizontal: 5}} />
        <Text style={{color: '#02BD63'}}>{Constants.Connected}</Text>
      </Block>
      <View style={{padding: 16}}>
        <ResultAI />
        {/* <AutoRemote /> */}
      </View>
      {/* <View style={styles.itemRow}>
        <ItemRemote
          title={Constants.Vibrate}
          switch={vibrate}
          handleSwitch={() => toggleRemote('vibrate')}>
          <Vibrate />
        </ItemRemote>
        <ItemRemote
          title={Constants.Music}
          switch={music}
          handleSwitch={() => toggleRemote('music')}>
          <Music />
        </ItemRemote>
      </View>
      <View style={[styles.itemRow, {alignSelf: 'center'}]}>
        <ItemRemote
          title={Constants.Light}
          switch={light}
          handleSwitch={() => toggleRemote('light')}>
          <Vibrate />
        </ItemRemote>
      </View> */}
      {/* <Button
        title={Constants.AddCrib}
        style={styles.btnAddCrib}
        onPress={navigateScanScreen}
      /> */}
    </Block>
  );
};

export default CribSettings;
