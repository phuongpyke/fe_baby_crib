import React from 'react';
import { Modal, View, Text, TouchableOpacity, Image } from 'react-native';
import md5 from 'md5';
import { styles } from '../style';

const ModalWifi = (props) => {

    const Item = (props) => (
        <TouchableOpacity
            key={md5(props.name)}
            onPress={props.handleWifi}
            style={styles.modalItem}
        >
            <Text>{props.name}</Text>
        </TouchableOpacity >
    )

    return (
        props.data ?
            <Modal
                animationType="fade"
                transparent={true}
                visible={props.visible}
                style={{ flex: 1 }}
            >
                <View activeOpacity={1} style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <Text style={styles.modalTitle}>Danh sách wifi</Text>

                        {props.data.map((item) => {
                            if (item) {
                                return <Item name={item} handleWifi={props.handleWifi} />
                            }
                        })}

                        <TouchableOpacity style={styles.close} onPress={props.closeModal}>
                            <Image source={require('../../../assets/icons/icon-close.png')} style={{ width: 18, height: 18 }} />
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
            : null
    )
}

export default ModalWifi;