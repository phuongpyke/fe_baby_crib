import React, {useState} from 'react';
import {View, Text} from 'react-native';
import Timeline from 'react-native-timeline-flatlist';
import {Color, Constants} from '../../../utils';
import {styles} from '../style';
import {ColorsDefault} from '/assets';

const TimelineCry = () => {
  const [dataCry, setDataCry] = useState([
    {time: '09:00', title: '1', description: 'Bé ngoan'},
    {time: '10:45', title: '1', description: 'Bé ngoan'},
    {time: '12:00', title: '1', description: 'Bé ngoan'},
    {time: '14:00', title: '1', description: 'Bé ngoan'},
    {time: '16:30', title: '2', description: 'Bé có biểu hiện quấy khóc'},
    {time: '16:30', title: '1', description: 'Bé ngoan'},
    {time: '16:30', title: '3', description: 'Bé bị sốt'},
    {time: '16:30', title: '1', description: 'Bé ngoan'},
    {time: '16:30', title: '2', description: 'Bé có biểu hiện quấy khóc'},
  ]);

  const renderCry = rowData => {
    return (
      <View
        style={[
          styles.eventDetails2,
          {
            backgroundColor:
              rowData.title === '3'
                ? '#EB5757'
                : rowData.title === '2'
                ? '#FFE0C5'
                : '#DAFFE0',
          },
        ]}>
        <Text style={[styles.txtBlack, {fontWeight: '400'}]}>
          {rowData.description}
        </Text>
      </View>
    );
  };

  return (
    <Timeline
      style={{flex: 1}}
      data={dataCry}
      circleColor={ColorsDefault.transparent}
      lineWidth={0}
      timeContainerStyle={{minWidth: 40, marginTop: -7}}
      timeStyle={styles.txtBlack}
      options={{style: {paddingTop: 20, marginBottom: 20}}}
      eventContainerStyle={styles.event}
      renderDetail={renderCry}
    />
  );
};

export default TimelineCry;
