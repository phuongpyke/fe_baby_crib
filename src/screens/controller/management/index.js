import React, {useState} from 'react';
import {View, Image} from 'react-native';
import Timeline from 'react-native-timeline-flatlist';
import {Constants} from '../../../utils';
import CryTimeline from './CryTimeline';
import {styles} from '../style';
import {Touchable, Text} from '/components';
import {WIDTH_DEVICE} from '/common';
import images from '/assets/images';
import {ColorsDefault} from '/assets';

const FollowInDay = () => {
  const [temperature, setTemperature] = useState([
    {time: '09:00', title: '36', description: 'Trạng thái bình thường'},
    {time: '10:45', title: '37', description: 'Trạng thái bình thường'},
    {time: '12:00', title: '36', description: 'Trạng thái bình thường'},
    {time: '14:00', title: '36.5', description: 'Trạng thái bình thường'},
    {time: '16:30', title: '38.1', description: 'Có biểu hiện của sốt'},
    {time: '16:30', title: '36.4', description: 'Trạng thái bình thường'},
    {time: '16:30', title: '39.5', description: 'Bé bị sốt'},
    {time: '16:30', title: '36.2', description: 'Trạng thái bình thường'},
    {time: '16:30', title: '39', description: 'Có biểu hiện của sốt'},
  ]);
  const [isChosenTab, setIsChosenTab] = useState({
    tab1: true,
    tab2: false,
    tab3: false,
  });

  const handleTab = type => {
    if (type === 'tab1') {
      setIsChosenTab({...isChosenTab, tab1: true, tab2: false, tab3: false});
    } else if (type === 'tab2') {
      setIsChosenTab({...isChosenTab, tab1: false, tab2: true, tab3: false});
    } else if (type === 'tab3') {
      setIsChosenTab({...isChosenTab, tab1: false, tab2: false, tab3: true});
    }
  };

  const renderTemperature = rowData => {
    return (
      <View
        style={[
          styles.eventDetails,
          {
            backgroundColor:
              rowData.title > 39
                ? '#EB5757'
                : rowData.title > 38
                ? '#FFE0C5'
                : '#DAFFE0',
          },
        ]}>
        <View style={styles.row}>
          <Image source={images.thermometer} />
          <Text style={[styles.txtBlack, {paddingLeft: 4}]}>
            {rowData.title}
          </Text>
          <Text style={[styles.txtTemperature, {color: ColorsDefault.black}]}>
            o
          </Text>
          <Text style={styles.txtBlack}>C</Text>
        </View>
        <Text style={[styles.txtBlack, {fontWeight: '400'}]}>
          {rowData.description}
        </Text>
      </View>
    );
  };

  const Tabs = () => {
    return (
      <View style={styles.tabWrapper}>
        <Touchable
          width={(WIDTH_DEVICE - 32) / 3}
          activeOpacity={1}
          center
          middle
          onPress={() => handleTab('tab1')}
          style={isChosenTab.tab1 ? styles.chosenTab : null}>
          <Text
            style={
              isChosenTab.tab1 ? styles.txtBlack : styles.txtUnselectedTab
            }>
            {Constants.Temperature}
          </Text>
        </Touchable>
        <Touchable
          width={(WIDTH_DEVICE - 32) / 3}
          center
          middle
          activeOpacity={1}
          onPress={() => handleTab('tab2')}
          style={isChosenTab.tab2 ? styles.chosenTab : null}>
          <Text
            style={
              isChosenTab.tab2 ? styles.txtBlack : styles.txtUnselectedTab
            }>
            {Constants.Cry}
          </Text>
        </Touchable>
        <Touchable
          width={(WIDTH_DEVICE - 32) / 3}
          center
          middle
          activeOpacity={1}
          onPress={() => handleTab('tab3')}
          style={isChosenTab.tab3 ? styles.chosenTab : null}>
          <Text
            style={
              isChosenTab.tab3 ? styles.txtBlack : styles.txtUnselectedTab
            }>
            Ho
          </Text>
        </Touchable>
      </View>
    );
  };

  return (
    <View style={{padding: 16, flex: 1}}>
      <Tabs />
      {isChosenTab.tab1 ? (
        <Timeline
          style={{flex: 1}}
          data={temperature}
          circleColor={ColorsDefault.transparent}
          lineWidth={0}
          timeContainerStyle={{minWidth: 40, marginTop: -7}}
          timeStyle={styles.txtBlack}
          options={{style: {paddingTop: 20, marginBottom: 20}}}
          eventContainerStyle={styles.event}
          renderDetail={renderTemperature}
        />
      ) : (
        <CryTimeline />
      )}
    </View>
  );
};

export default FollowInDay;
