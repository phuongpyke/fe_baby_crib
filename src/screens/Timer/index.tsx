import {useIsFocused} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import isEqual from 'react-fast-compare';
import {FlatList} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {vs} from 'react-native-size-matters';
import StaticSafeAreaInsets from 'react-native-static-safe-area-insets';
import {ColorsDefault} from '/assets';
import images from '/assets/images';
import {Block, Image, LoadingOverlay, Text, Touchable} from '/components';
import {Header} from '/components/common';
import {navigate} from '/navigations';
import {Routes} from '/navigations/Routes';
import {useAppSelector} from '/redux/hooks';
import {getScheduleTodayWithBaby} from '/services/api';
import {getOptionSelected} from '/utils/helper';

const Timer = () => {
  const {babyList, labels} = useAppSelector(
    ({user: {babyList}, schedule: {labels}}) => ({babyList, labels}),
    isEqual,
  );

  const isFocused = useIsFocused();

  const [listSchedule, setListSchedule] = useState<any[]>([]);
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const goTimerDetail = (scheduleId: string | number) =>
    navigate(Routes.TimerDetail, {
      scheduleId,
      babyId: babyList?.[0]?.id,
    });

  const goAddSchedule = () => navigate(Routes.AddSchedule);

  const goCreateSchedule = () => navigate(Routes.CreateSchedule);

  useEffect(() => {
    const getData = async () => {
      try {
        setIsLoading(true);
        const babyId = babyList?.[0]?.id;
        const response: any = await getScheduleTodayWithBaby({babyId});
        if (response.success && response.data?.length) {
          setListSchedule(response.data);
        }
      } catch (err) {
      } finally {
        setIsLoading(false);
      }
    };
    if (isFocused) {
      getData();
    }
  }, [isFocused]);

  const renderItem = ({item}: any) => {
    return (
      <Touchable
        row
        middle
        mt={12}
        p={8}
        borderRadius={16}
        borderWidth={1}
        borderColor={ColorsDefault.gray1}
        mh={12}
        onPress={() => goTimerDetail(item?.id)}>
        <Block>
          <Block
            borderRadius={12}
            height={96}
            width={96}
            middle
            justifyEnd
            bg={ColorsDefault.purple1}>
            <Text fontSize={14}>
              {getOptionSelected(labels, Number(item?.label), {
                label: 'category_name',
                value: 'id',
              })}
            </Text>
          </Block>
          <Block
            mt={6}
            height={20}
            width={80}
            bg={ColorsDefault.gray1}
            borderRadius={6}
            alignSelfCenter
            middle
            center>
            <Text fontSize={12}>{item?.time || item?.time_string}</Text>
          </Block>
        </Block>
        <Block flex={1} height={'100%'} ml={12}>
          <Text type="h5">{item.content}</Text>
        </Block>
      </Touchable>
    );
  };

  const renderAction = () => (
    <Image source={images.editSchedule} width={16} height={16} />
  );

  return (
    <SafeAreaView
      style={{flex: 1, backgroundColor: ColorsDefault.white}}
      edges={['right', 'left', 'top']}>
      <LoadingOverlay loading={isLoading} />
      <Header
        title="Tổng hợp lịch chăm bé"
        renderAction={renderAction}
        onPressAction={goAddSchedule}
        actionStyle={{
          height: 36,
          width: 36,
          borderRadius: 36,
          backgroundColor: ColorsDefault.white,
          borderWidth: 1,
          borderColor: ColorsDefault.gray1,
        }}
      />
      <Block
        circle={170}
        bg={ColorsDefault.pink3}
        absolute
        zIndex={-5}
        right={-60}
        top={-60}
      />
      <FlatList
        style={{flex: 1}}
        data={listSchedule}
        contentContainerStyle={{
          paddingBottom: StaticSafeAreaInsets.safeAreaInsetsBottom + vs(20),
        }}
        renderItem={renderItem}
        keyExtractor={(item: any) => `${item.id}`}
      />
      <Touchable
        onPress={goCreateSchedule}
        absolute
        bg={ColorsDefault.blue1}
        circle={60}
        middle
        center
        zIndex={10}
        bottom={StaticSafeAreaInsets.safeAreaInsetsBottom + vs(40)}
        right={16}>
        <Image source={images.addSchedule} width={24} height={24} />
      </Touchable>
    </SafeAreaView>
  );
};

export default Timer;
