import auth from '@react-native-firebase/auth';
import messaging from '@react-native-firebase/messaging';
import {useNavigation} from '@react-navigation/native';
import {FormikProps, useFormik} from 'formik';
import React, {useState} from 'react';
import {StyleSheet} from 'react-native';
import Toast from 'react-native-toast-message';
import {checkPhoneExist} from '/api/modules/auth';
import {ColorsDefault, FontFamilyDefault} from '/assets';
import images from '/assets/images';
import {WIDTH_DEVICE} from '/common';
import {Block, Body, Image, Input, Text, Touchable} from '/components/base';
import {Routes} from '/navigations/Routes';
import {registerAPI} from '/services/api';
interface MyValues {
  phone: string;
  password: string;
  rePassword: string;
}

const validate = (values: any) => {
  const errors: any = {};
  if (!values.phone) {
    errors.phone = 'Hãy nhập số điện thoại';
  }
  if (values.phone.length !== 10) {
    errors.phone = 'Số điện thoại không hợp lệ';
  }

  if (!values.password) {
    errors.password = 'Hãy nhập mật khẩu';
  }

  if (!values.rePassword) {
    errors.rePassword = 'Hãy nhập lại mật khẩu';
  }
  if (values.rePassword && values.rePassword !== values.password) {
    errors.rePassword = 'Mật khẩu chưa trùng khớp';
  }
  return errors;
};

const Register = () => {
  const navigation = useNavigation<any>();
  const [isShowPass, setIsShowPass] = useState(false);
  const [isShowRePass, setIsShowRePass] = useState(false);

  const [isLoading, setIsLoading] = useState<boolean>(false);

  const formik: FormikProps<MyValues> = useFormik<MyValues>({
    initialValues: {phone: '', password: '', rePassword: ''},
    validate,
    onSubmit: () => {},
  });

  const isValidForm = !(
    !formik.errors.password &&
    !formik.errors.phone &&
    !formik.errors.rePassword &&
    formik.values.phone &&
    formik.values.password &&
    formik.values.rePassword
  );

  const onRegister = async (cb?: any) => {
    try {
      const response: any = await registerAPI({
        password: formik.values?.password,
        phone: formik.values?.phone,
      });
      if (response.success) {
        Toast.show({
          type: 'custom',
          text1: 'Bạn đã đăng ký tài khoản thành công',
          props: {
            type: 'success',
          },
        });
        cb && cb();
      } else {
        Toast.show({
          type: 'custom',
          text1: `${response.message || 'Có lỗi xảy ra. Vui lòng thử lại!'}`,
          props: {
            type: 'error',
          },
        });
      }
    } catch (error: any) {}
  };

  const goOtp = async () => {
    try {
      setIsLoading(true);
      const phone = formik.values.phone;
      const checkPhone: any = await checkPhoneExist({phone});
      if (checkPhone?.success) {
        const formatPhone = `+84 ${phone.substring(1, 3)}-${phone.substring(
          3,
          6,
        )}-${phone.substring(6, 10)}`;
        await messaging().getToken();
        const confirmation = await auth().signInWithPhoneNumber(formatPhone);
        navigation.navigate(Routes.Otp, {
          phone,
          onRegister,
          confirmation,
        });
      }
    } catch (err: any) {
      if (err?.message) {
        Toast.show({
          type: 'custom',
          text1: err?.message,
          props: {
            type: 'error',
          },
        });
      }
    } finally {
      setIsLoading(false);
    }
  };

  const goLogin = () => {
    navigation.navigate(Routes.Login);
  };

  return (
    <Body
      scroll
      keyboardAvoid
      bg={ColorsDefault.white}
      loading={isLoading}
      showsVerticalScrollIndicator={false}>
      <Image
        source={images.headerRegister}
        style={styles.logo}
        resizeMode={'stretch'}>
        <Touchable onPress={() => navigation.goBack()}>
          <Image
            source={images.backLeft}
            tintColor={ColorsDefault.black}
            mt={58}
            ml={16}
            width={24}
            height={24}
          />
        </Touchable>
      </Image>
      <Block ph={16}>
        <Text
          textCenter
          type="h4"
          color={ColorsDefault.black3}
          fontWeight={'medium'}
          ph={32}
          mt={42}>
          Hãy để chúng tôi giúp bạn theo dõi sức khoẻ của bé nhé.
        </Text>
        <Block center middle>
          <Input
            style={{
              borderWidth: 1,
              width: WIDTH_DEVICE - 32,
              borderRadius: 60,
              borderColor: ColorsDefault.gray1,
            }}
            mt={12}
            height={58}
            mb={5}
            value={formik.values.phone}
            onChangeText={formik.handleChange('phone')}
            placeholder="Số điện thoại"
            error={formik.errors.phone && formik.touched.phone}
            errorMessage={formik.errors.phone}
            onBlur={formik.handleBlur('phone')}
            keyboardType={'number-pad'}
          />
          <Input
            mt={16}
            value={formik.values.password}
            error={formik.errors.password && formik.touched.password}
            errorMessage={formik.errors.password}
            onChangeText={formik.handleChange('password')}
            style={{
              borderWidth: 1,
              width: WIDTH_DEVICE - 32,
              borderRadius: 60,
              borderColor: ColorsDefault.gray1,
              fontFamily: FontFamilyDefault.primaryRegular,
            }}
            onBlur={formik.handleBlur('password')}
            height={58}
            mb={5}
            iconRightPress={() => {
              setIsShowPass(!isShowPass);
            }}
            iconRight={isShowPass ? images.show : images.hide}
            secureTextEntry={!isShowPass}
            placeholder="Mật khẩu"
          />
          <Input
            mt={16}
            value={formik.values.rePassword}
            error={formik.errors.rePassword && formik.touched.rePassword}
            errorMessage={formik.errors.rePassword}
            onChangeText={formik.handleChange('rePassword')}
            style={{
              borderWidth: 1,
              width: WIDTH_DEVICE - 32,
              borderRadius: 60,
              borderColor: ColorsDefault.gray1,
              fontFamily: FontFamilyDefault.primaryRegular,
            }}
            onBlur={formik.handleBlur('rePassword')}
            height={58}
            mb={5}
            iconRightPress={() => {
              setIsShowRePass(!isShowRePass);
            }}
            iconRight={isShowRePass ? images.show : images.hide}
            secureTextEntry={!isShowRePass}
            placeholder="Nhập lại mật khẩu"
          />
        </Block>
        <Touchable
          onPress={goOtp}
          disabled={isValidForm}
          middle
          mt={16}
          center
          borderRadius={60}
          bg={isValidForm ? ColorsDefault.gray2 : ColorsDefault.blue1}
          height={52}>
          <Text type="h4" color={ColorsDefault.white} fontWeight={'bold'}>
            Đăng ký
          </Text>
        </Touchable>
      </Block>
      <Image
        style={{
          bottom: 0,
          right: 0,
          left: 0,
          zIndex: -10,
        }}
        resizeMode={'stretch'}
        center
        middle
        absolute
        source={images.footerRegister}
        width={WIDTH_DEVICE}
        height={WIDTH_DEVICE * 0.7}>
        <Touchable mt={64} onPress={goLogin}>
          <Text type="h4" underline color={ColorsDefault.black3}>
            Bạn đã có tài khoản cho bé.{' '}
            <Text type="h4" color={ColorsDefault.blue1}>
              Đăng nhập
            </Text>
          </Text>
        </Touchable>
      </Image>
    </Body>
  );
};

export default Register;

const styles = StyleSheet.create({
  logo: {
    width: WIDTH_DEVICE,
    height: WIDTH_DEVICE * 0.48,
    alignSelf: 'center',
  },
});
