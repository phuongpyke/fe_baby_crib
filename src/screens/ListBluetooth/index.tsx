import {Buffer} from 'buffer';
import {isArray} from 'lodash';
import LottieView from 'lottie-react-native';
import React, {FC, useEffect, useState} from 'react';
import {
  FlatList,
  NativeEventEmitter,
  NativeModules,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import BleManager, {Peripheral} from 'react-native-ble-manager';
import {ms, s, ScaledSheet} from 'react-native-size-matters';
import {useDispatch} from 'react-redux';
import {ColorsDefault, FontFamilyDefault, FontSizeDefault} from '/assets';
import {BOTTOM_PADDING, FULL_WIDTH_DEVICE} from '/common';
import {Loading} from '/components';
import {onSetBlueToothId} from '/modules/user/slice';
import {navigate} from '/navigations';
import {Routes} from '/navigations/Routes';
import {showToastError} from '/utils/helper';

const BleManagerModule = NativeModules.BleManager;
const BleManagerEmitter = new NativeEventEmitter(BleManagerModule);

//
const addressMacCharacteristic = '00000009-373F-11EC-8D3D-0242AC130003';

const wifiCharacteristic = '00000010-373F-11EC-8D3D-0242AC130003';
const wifiService = '00000001-373F-11EC-8D3D-0242AC130003';

const ListBluetooth: FC = () => {
  const dispatch = useDispatch();

  const peripherals = new Map();
  const [isScanning, setIsScanning] = useState<boolean>(false);
  const [isConnected, setIsConnected] = useState<boolean>(false);
  const [listBle, setListBluetooth] = useState<any[]>([]);
  const [listWifi, setListWifi] = useState([]);
  const [peripheralId, setPeripheralId] = useState(null);

  const goScanWifi = () => {
    // navigate(Routes.Wifi, {
    //   listWifi,
    //   peripheralId,
    // });
    stopScan();
    navigate(Routes.ListWifi, {
      listWifi,
      peripheralId,
    });
  };

  const handleDiscoverPeripheral = (peripheral: Peripheral) => {
    if (peripheral?.name) {
      peripherals.set(peripheral.id, peripheral);
      setListBluetooth(Array.from(peripherals.values()));
    }
  };

  const handleStopScan = (event: any) => {
    console.log('Scan is stopped', event);
    setIsScanning(false);
  };

  const handleDisconnectedPeripheral = (data: any) => {
    let peripheral = peripherals.get(data.peripheral);
    if (peripheral) {
      peripheral.connected = false;
      peripherals.set(peripheral.id, peripheral);
      setListBluetooth(Array.from(peripherals.values()));
    }
  };

  const handleUpdateValueForCharacteristic = (event: any) => {
    console.log('handleUpdateValueForCharacteristic', event);
  };

  const onSelectBluetooth = async (peripheral: any) => {
    try {
      console.log('onSelectBluetooth: ', peripheral);
      if (peripheral?.name === 'raspberrypi') {
        const isConnected = await BleManager.isPeripheralConnected(
          peripheral.id,
          [],
        );
        if (isConnected) {
          await BleManager.disconnect(peripheral.id);
          console.log('disconnect');
          return showToastError(
            'Thiết bị đã ngắt kết nối. Vui lòng kết nối lại',
          );
        }
        await BleManager.connect(peripheral.id);
        let p = peripherals.get(peripheral.id);
        if (p) {
          p.connected = true;
          peripherals.set(peripheral.id, p);
          setListBluetooth(Array.from(peripherals.values()));
        }
        setPeripheralId(peripheral.id);
        const retrieveData = await BleManager.retrieveServices(peripheral.id);
        // console.log('retrieveData', retrieveData);
        if (!isArray(retrieveData?.characteristics)) {
          return showToastError('Đã xảy ra lỗi. Vui lòng kết nối lại');
        }
        const findAddressMac = retrieveData.characteristics?.find(
          item =>
            item.characteristic.toUpperCase() === addressMacCharacteristic,
        );
        if (findAddressMac) {
          let readData = await BleManager.read(
            peripheral.id,
            findAddressMac?.service,
            findAddressMac?.characteristic,
          );
          readData = Buffer.from(readData).toString();
          // console.log('readData: ', readData);
          if (readData) {
            // dispatch(onSetBlueToothId({data: peripheral.id}));
            setIsConnected(true);
          }
        }
        // const findWifi = retrieveData.characteristics?.find(
        //   item => item.characteristic.toUpperCase() === wifiCharacteristic,
        // );
        // if (findWifi) {
        //   let stringWifi = await BleManager.read(
        //     peripheral.id,
        //     wifiService,
        //     wifiCharacteristic,
        //   );
        //   stringWifi = Buffer.from(stringWifi).toString();
        //   if (stringWifi) {
        //     console.log('stringWifi', stringWifi);
        //     setListWifi(stringWifi.split('_'));
        //     setIsConnected(true);
        //   }
        // }
      }
    } catch (err) {
      console.log('err', err);
      showToastError('Đã xảy ra lỗi');
    }
  };

  const startScan = () => {
    BleManager.scan([], 10, false)
      .then(() => {
        console.log('Scanning...');
        setIsScanning(true);
      })
      .catch(err => {
        console.log('Scanning', err);
      });
  };

  const stopScan = () => {
    BleManager.stopScan().then(() => {
      console.log('Scan stopped');
    });
  };

  useEffect(() => {
    if (!isScanning) {
      startScan();
    }
  }, [isScanning]);

  useEffect(() => {
    BleManager.start({showAlert: false});

    const discoverPeripheral = BleManagerEmitter.addListener(
      'BleManagerDiscoverPeripheral',
      handleDiscoverPeripheral,
    );
    const stopScan = BleManagerEmitter.addListener(
      'BleManagerStopScan',
      handleStopScan,
    );
    const disconnectedPeripheral = BleManagerEmitter.addListener(
      'BleManagerDisconnectPeripheral',
      handleDisconnectedPeripheral,
    );
    const updateValue = BleManagerEmitter.addListener(
      'BleManagerDidUpdateValueForCharacteristic',
      handleUpdateValueForCharacteristic,
    );

    return () => {
      discoverPeripheral.remove();
      stopScan.remove();
      disconnectedPeripheral.remove();
      updateValue.remove();
    };
  }, []);

  const renderItem = ({item}: any) => (
    <TouchableOpacity
      onPress={() => onSelectBluetooth(item)}
      style={{
        paddingVertical: ms(16),
        borderBottomWidth: 1,
        borderBottomColor: ColorsDefault.gray1,
      }}>
      <Text
        style={{
          fontSize: FontSizeDefault.FONT_16,
          fontFamily: FontFamilyDefault.primarySemiBold,
          color: item.connected ? ColorsDefault.green : ColorsDefault.black3,
        }}>
        {item.name}
      </Text>
      <Text
        style={{
          fontSize: FontSizeDefault.FONT_14,
          fontFamily: FontFamilyDefault.primaryRegular,
          color: item.connected
            ? ColorsDefault.green
            : ColorsDefault.textPlaceHolder,
          marginTop: ms(4),
        }}>
        {item.id}
      </Text>
    </TouchableOpacity>
  );

  const renderHeader = () => (
    <View
      style={{flexDirection: 'row', alignItems: 'center', marginTop: ms(25)}}>
      <Text
        style={{
          fontSize: FontSizeDefault.FONT_16,
          fontFamily: FontFamilyDefault.primaryRegular,
          color: ColorsDefault.gray3,
        }}>
        {'CÁC THIẾT BỊ'}
      </Text>
      <Loading ml={8} />
    </View>
  );

  const renderNoData = () => (
    <View
      style={{
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Text
        style={{
          fontSize: FontSizeDefault.FONT_18,
          fontFamily: FontFamilyDefault.primaryRegular,
          color: ColorsDefault.black3,
        }}>
        {'Không tìm thấy thiết bị nào'}
      </Text>
    </View>
  );

  return (
    <View style={styles.container}>
      {isConnected ? (
        <View style={{flex: 1}}>
          <LottieView
            source={require('../../assets/loader/connected.json')}
            autoPlay
            loop
            style={{
              width: FULL_WIDTH_DEVICE - 50,
              height: FULL_WIDTH_DEVICE - 50,
              marginTop: 20,
              alignSelf: 'center',
            }}
          />
          <TouchableOpacity
            onPress={goScanWifi}
            style={{
              height: 52,
              width: FULL_WIDTH_DEVICE - 32,
              backgroundColor: ColorsDefault.blue1,
              borderRadius: 6,
              justifyContent: 'center',
              alignItems: 'center',
              marginHorizontal: 16,
              position: 'absolute',
              bottom: BOTTOM_PADDING,
            }}>
            <Text
              style={{
                fontSize: FontSizeDefault.FONT_16,
                fontFamily: FontFamilyDefault.primaryMedium,
                color: ColorsDefault.white,
              }}>
              {'Tiếp theo'}
            </Text>
          </TouchableOpacity>
        </View>
      ) : (
        <FlatList
          style={{flex: 1}}
          contentContainerStyle={{
            paddingHorizontal: s(16),
            flexGrow: 1,
            paddingBottom: BOTTOM_PADDING,
          }}
          data={listBle}
          renderItem={renderItem}
          ListHeaderComponent={renderHeader}
          ListEmptyComponent={renderNoData}
          keyExtractor={item => item.id}
        />
      )}
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: ColorsDefault.white,
  },
});

export default ListBluetooth;
