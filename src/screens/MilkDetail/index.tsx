import React, {useState} from 'react';
import {KeyboardAvoidingView, StyleSheet} from 'react-native';
import Toast from 'react-native-toast-message';
import {useNavigation} from '@react-navigation/native';
import {
  Body,
  Image,
  Text,
  Block,
  Input,
  Touchable,
  List,
} from '/components/base';
import {ColorsDefault} from '/assets';
import images from '/assets/images';
import {WIDTH_DEVICE} from '/common';
// import {useKeyboardShowTranslation} from '/hook/useKeyboardShowTranslation';
import Animated from 'react-native-reanimated';
import {getBottomSpace} from 'react-native-iphone-x-helper';

const dataFake = {
  id: 1,
  name: 'Bỉm merries',
  price: '295.000đ lon 350g và 625.000đ lon 800g',
  content:
    'Đạm whey, axit béo omega-3 và omega 6/DHA/ALA, các vitamin và khoáng chất như vitamin A, C, D, canxi, sắt, kẽm, photpho,… lợi khuẩn HMP (lợi khuẩn được phân lập gốc từ sữa mẹ) và chất xơ GOS',
  isLike: true,
  numberLike: 10,
  numberComment: 2,
  image: images.diapers1,
  list: [
    {
      id: 1,
      name: 'Nguyễn Hoàng',
      comment: 'Bỉm này tốt lắm',
      timeCreated: '3 ngày ',
      avatar: images.avatarCommet,
    },
    {
      id: 2,
      name: 'Nguyễn Hoàng',
      comment: 'Bỉm này tốt lắm',
      timeCreated: '3 ngày ',
      avatar: images.avatarCommet,
    },
  ],
};

const MilkDetail = () => {
  const [text, setText] = useState('');
  const navigation = useNavigation<any>();
  const {name, price, content, isLike, numberLike, numberComment, image} =
    dataFake;
  // const {animatedStyle} = useKeyboardShowTranslation();

  const renderFooter = () => {
    return (
      <Block
        borderTop
        absolute
        bg={ColorsDefault.white}
        bottom={0}
        right={0}
        left={0}
        middle
        center>
        <Input
          value={text}
          style={{
            borderRadius: 60,
            width: WIDTH_DEVICE - 32,
            marginTop: 16,
            marginBottom: getBottomSpace(),
          }}
          mh={16}
          height={36}
          onChangeText={setText}
          placeholder={'Viết bình luận...'}></Input>
      </Block>
    );
  };

  const renderHeader = () => {
    return (
      <Block mt={16} pt={16} borderRadius={6}>
        <Block row>
          <Image width={143} height={113} source={image}></Image>
          <Block ml={8} width={WIDTH_DEVICE - 32 - 16 - 143}>
            <Text fontSize={16} fontWeight={'medium'}>
              {name}
            </Text>
            <Text mt={8} fontSize={14} color={ColorsDefault.blue1}>
              {price}
            </Text>
          </Block>
        </Block>
        <Text mt={15} mh={16} fontSize={14} fontWeight={'medium'}>
          Thành phần: <Text fontSize={14}>{content}</Text>
        </Text>
        <Block height={1} bg={ColorsDefault.gray} mt={16} />
        <Block row>
          <Touchable
            middle
            center
            row
            width={(WIDTH_DEVICE - 32) / 2}
            pv={16}
            borderRight>
            <Image
              source={images.favorite}
              tintColor={isLike ? ColorsDefault.red1 : ColorsDefault.gray3}
              width={18}
              height={16}
              mr={8}
            />
            <Text
              fontSize={14}
              color={isLike ? ColorsDefault.red1 : ColorsDefault.gray3}>
              Thích({numberLike})
            </Text>
          </Touchable>
          <Touchable middle center row width={(WIDTH_DEVICE - 32) / 2}>
            <Image source={images.comment} width={18} height={18} mr={8} />
            <Text fontSize={14} color={ColorsDefault.gray3}>
              Bình luận({numberComment})
            </Text>
          </Touchable>
        </Block>
        <Block height={1} bg={ColorsDefault.gray} />
      </Block>
    );
  };

  const renderItem = ({item}: any) => {
    return (
      <Block row mt={16} mh={16}>
        <Image source={item.avatar} width={36} height={36} />
        <Block>
          <Block
            ml={16}
            bg={ColorsDefault.gray1}
            ph={16}
            pv={12}
            borderRadius={16}>
            <Text fontSize={16} fontWeight={'medium'}>
              {item.name}
            </Text>
            <Text fontSize={14} mt={4}>
              {item.comment}
            </Text>
          </Block>
          <Text color={ColorsDefault.gray3} ml={16} fontSize={12}>
            {item.timeCreated}
          </Text>
        </Block>
      </Block>
    );
  };

  return (
    <>
      <Animated.View style={[styles.container]}>
        <List
          data={dataFake.list}
          contentContainerStyle={{paddingBottom: 64}}
          ListHeaderComponent={renderHeader}
          renderItem={renderItem}
        />
        {renderFooter()}
      </Animated.View>
    </>
  );
};

export default MilkDetail;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: ColorsDefault.white,
  },
  contentContainerStyle: {
    paddingBottom: 16,
  },
});
