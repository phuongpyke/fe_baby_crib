import {useIsFocused, useNavigation} from '@react-navigation/native';
import React, {useEffect, useRef, useState} from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import {RNCamera} from 'react-native-camera';
import QRCodeScanner from 'react-native-qrcode-scanner';
import Animated, {
  Easing,
  useAnimatedStyle,
  useSharedValue,
  withRepeat,
  withTiming,
} from 'react-native-reanimated';
import StaticSafeAreaInsets from 'react-native-static-safe-area-insets';
import {useDispatch} from 'react-redux';
import {ColorsDefault} from '/assets';
import images from '/assets/images';
import {FULL_HEIGHT_DEVICE, FULL_WIDTH_DEVICE, HEIGHT_DEVICE} from '/common';
import {Image, Text} from '/components';
import {onSetCrib} from '/modules/user/slice';
import {Routes} from '/navigations/Routes';
import {hitSlop} from '/utils/helper';

const squareEdge = FULL_WIDTH_DEVICE * 0.65;
const heightRectangle = 3;

const QrScreen = () => {
  const navigation: any = useNavigation();
  const dispatch = useDispatch();
  const [flashMode, setFlashMode] = useState<any>(
    RNCamera.Constants.FlashMode.off,
  );
  const [hasPermission, setHasPermission] = useState<boolean>(false);
  const scannerRef = useRef<QRCodeScanner>(null);
  const isFocused = useIsFocused();

  const translateY = useSharedValue(0);

  const rectangleStyle = useAnimatedStyle(() => {
    return {
      transform: [
        {
          translateY: translateY.value,
        },
      ],
    };
  });

  const handleScan = (e: any) => {
    if (e?.data) {
      dispatch(onSetCrib({data: e?.data}));
      scannerRef.current?.disable();
      // navigation.navigate(Routes.ScanDevices);
      navigation.navigate(Routes.ListBluetooth);
    }
  };

  const onPressFlash = () => {
    const mode =
      flashMode === RNCamera.Constants.FlashMode.off
        ? RNCamera.Constants.FlashMode.torch
        : RNCamera.Constants.FlashMode.off;
    setFlashMode(mode);
  };

  useEffect(() => {
    if (isFocused) {
      scannerRef.current?.enable();
    }
  }, [isFocused]);

  useEffect(() => {
    if (hasPermission) {
      const distance = squareEdge - heightRectangle;
      translateY.value = withRepeat(
        withTiming(distance, {
          duration: 1800,
          easing: Easing.bezier(0.25, 0.1, 0.25, 1),
        }),
        -1,
        true,
      );
    }
  }, [hasPermission]);

  const renderMarker = (
    <View style={styles.wrapMarker}>
      <TouchableOpacity
        hitSlop={hitSlop(20)}
        style={styles.btnClose}
        onPress={() => navigation.goBack()}>
        <Image
          source={images.close}
          width={24}
          height={24}
          tintColor={ColorsDefault.white}
        />
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.btnFlash}
        onPress={onPressFlash}
        hitSlop={hitSlop(20)}>
        <Image
          source={
            flashMode === RNCamera.Constants.FlashMode.off
              ? images.flashOn
              : images.flashOff
          }
          height={28}
          width={28}
        />
      </TouchableOpacity>
      <View style={styles.topAndBottomOverlay}>
        <Text fontSize={24} color={ColorsDefault.white} fontWeight={'bold'}>
          {'Quét mã'}
        </Text>
        <Text textCenter fontSize={16} color={ColorsDefault.white}>
          {'Hướng khung camera vào mã QR để quét'}
        </Text>
      </View>
      <View style={styles.viewCenter}>
        <View style={styles.leftAndRightOverlay} />
        <Animated.View style={styles.viewMiddle} />
        <View style={styles.leftAndRightOverlay} />
      </View>
      <View style={styles.topAndBottomOverlay} />
      <Animated.Image
        source={images.imgScanBar}
        style={styles.imgScanBar}
        resizeMode={'stretch'}
      />
      <Animated.View style={[styles.rectangle, rectangleStyle]} />
    </View>
  );

  return (
    <QRCodeScanner
      onRead={handleScan}
      ref={scannerRef}
      showMarker
      reactivate
      reactivateTimeout={2000}
      cameraProps={{
        type: RNCamera.Constants.Type.back,
      }}
      cameraStyle={{height: HEIGHT_DEVICE}}
      customMarker={renderMarker}
    />
  );
};

export default QrScreen;

const styles = StyleSheet.create({
  wrapMarker: {
    flex: 1,
    justifyContent: 'center',
  },
  topAndBottomOverlay: {
    height: (FULL_HEIGHT_DEVICE - squareEdge) / 2,
    width: FULL_WIDTH_DEVICE,
    backgroundColor: 'rgba(0,0,0,0.5)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  viewBottom: {
    height: FULL_HEIGHT_DEVICE * 0.2,
    width: FULL_WIDTH_DEVICE,
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  leftAndRightOverlay: {
    height: squareEdge,
    width: (FULL_WIDTH_DEVICE - squareEdge) / 2,
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  rectangle: {
    width: squareEdge,
    backgroundColor: ColorsDefault.blue1,
    height: heightRectangle,
    position: 'absolute',
    zIndex: 100,
    top: (FULL_HEIGHT_DEVICE - squareEdge) / 2,
    alignSelf: 'center',
  },
  viewCenter: {
    flexDirection: 'row',
  },
  viewMiddle: {
    flex: 1,
  },
  btnClose: {
    position: 'absolute',
    top: StaticSafeAreaInsets.safeAreaInsetsTop + 20,
    left: 24,
    zIndex: 10,
  },
  btnFlash: {
    position: 'absolute',
    top: StaticSafeAreaInsets.safeAreaInsetsTop + 20,
    right: 24,
    zIndex: 10,
  },
  imgScanBar: {
    height: squareEdge + 15,
    width: squareEdge + 15,
    zIndex: 10,
    position: 'absolute',
    alignSelf: 'center',
    tintColor: ColorsDefault.blue1,
  },
});
