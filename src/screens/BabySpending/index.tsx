import {useNavigation} from '@react-navigation/native';
import numeral from 'numeral';
import React, {useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  ImageBackground,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {s, ScaledSheet, vs} from 'react-native-size-matters';
import StaticSafeAreaInsets from 'react-native-static-safe-area-insets';
import {ColorsDefault, FontFamilyDefault, FontSizeDefault} from '/assets';
import images from '/assets/images';
import {BOTTOM_PADDING, FULL_WIDTH_DEVICE} from '/common';

const data = [
  {
    id: 0,
    title: 'Bỉm merries',
    subTitle: 'Bỉm bé dùng',
    price: '295.000đ / 46 miếng dán',
    total_price: 770000,
    image: images.diapers1,
  },
  {
    id: 1,
    title: 'Sữa bột công thức hữu cơ HiPP ORGANIC COMBIOTIC® số 1',
    subTitle: 'Sữa bé uống',
    price: '295.000đ lon 350g',
    total_price: 770000,
    image: images.diapers1,
  },
  {
    id: 2,
    title: 'Bỉm merries',
    subTitle: 'Bỉm bé dùng',
    price: '295.000đ / 46 miếng dán',
    total_price: 770000,
    image: images.diapers1,
  },
];

const BabySpending = () => {
  const [listSpending, setListSpending] = useState<any[]>(data);

  const onSelectCheckBox = (id: number) => {
    let list = [...listSpending];
    list = list.map(item =>
      item.id === id ? {...item, isSelected: !item?.isSelected} : item,
    );
    setListSpending(list);
  };

  const onBuyNow = () => {};

  const renderItem = ({item}: any) => (
    <View style={styles.wrapItem}>
      <View style={styles.viewHeaderItem}>
        <Text style={styles.textSubTitle}>{item.subTitle}</Text>
        <TouchableOpacity>
          <Text style={styles.textChange}>{'Thay đổi'}</Text>
        </TouchableOpacity>
      </View>
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          borderRadius: 12,
          borderWidth: 1,
          borderColor: ColorsDefault.gray1,
          padding: 12,
        }}>
        <View style={styles.viewLeftItem}>
          <Image source={item.image} style={styles.itemImage} />
          <View style={styles.viewRateStar}>
            <Image
              source={images.rateStar}
              style={{height: 14, width: 14, tintColor: ColorsDefault.yellow2}}
            />
            <Text style={styles.textStar}>{'3.5'}</Text>
          </View>
        </View>
        <View style={styles.viewRightItem}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text
              style={{
                flex: 1,
                fontSize: FontSizeDefault.FONT_16,
                fontFamily: FontFamilyDefault.primaryMedium,
                color: ColorsDefault.black3,
              }}>
              {item.title}
            </Text>
            <TouchableOpacity
              style={styles.btnCheckBox}
              onPress={() => onSelectCheckBox(item.id)}>
              <Image
                source={
                  item?.isSelected
                    ? images.checkboxActive
                    : images.checkboxInactive
                }
                style={{width: s(24), height: s(24)}}
              />
            </TouchableOpacity>
          </View>
          <Text
            style={{
              fontSize: FontSizeDefault.FONT_14,
              fontFamily: FontFamilyDefault.primaryRegular,
              color: ColorsDefault.blue1,
            }}>
            {item.price}
          </Text>
          <View style={styles.viewLineItem} />
          <View style={styles.viewHeaderItem}>
            <Text
              style={{
                fontSize: FontSizeDefault.FONT_14,
                fontFamily: FontFamilyDefault.primaryRegular,
                color: ColorsDefault.black3,
              }}>
              {'Số lần/ngày'}
            </Text>
            <Text
              style={{
                fontSize: FontSizeDefault.FONT_14,
                fontFamily: FontFamilyDefault.primaryMedium,
                color: ColorsDefault.black3,
              }}>
              {4}
            </Text>
          </View>
          <View style={styles.viewHorizonItem}>
            <Text
              style={{
                fontSize: FontSizeDefault.FONT_14,
                fontFamily: FontFamilyDefault.primaryRegular,
                color: ColorsDefault.black3,
              }}>
              {'Tổng tiền'}
            </Text>
            <Text
              style={{
                fontSize: FontSizeDefault.FONT_14,
                fontFamily: FontFamilyDefault.primaryMedium,
                color: ColorsDefault.orange2,
              }}>{`${numeral(item.total_price).format()}đ`}</Text>
          </View>
        </View>
      </View>
    </View>
  );

  const renderHeader = () => (
    <ImageBackground
      imageStyle={{borderRadius: 12}}
      source={images.backgroundMilk3}
      style={styles.wrapFooter}>
      <Text style={styles.textTotalSpend}>{'Tổng chi tiêu/tháng'}</Text>
      <View style={styles.viewDash} />
      <Text style={styles.textSpend}>{`${numeral(1540000).format()}đ`}</Text>
    </ImageBackground>
  );

  return (
    <View style={styles.container}>
      <FlatList
        data={listSpending}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: vs(24)}}
        renderItem={renderItem}
        ListHeaderComponent={renderHeader}
      />
      <TouchableOpacity
        onPress={onBuyNow}
        style={styles.btnBuy}
        activeOpacity={0.8}>
        <Text style={styles.textBuy}>{'Mua ngay'}</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    backgroundColor: ColorsDefault.white,
    flex: 1,
  },
  wrapFooter: {
    width: FULL_WIDTH_DEVICE - s(32),
    alignSelf: 'center',
    shadowColor: ColorsDefault.black,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3,
    marginTop: '16@vs',
    alignItems: 'center',
  },
  textTotalSpend: {
    fontSize: FontSizeDefault.FONT_16,
    color: ColorsDefault.black2,
    fontFamily: FontFamilyDefault.primaryBold,
    marginTop: '16@vs',
  },
  textSpend: {
    fontSize: FontSizeDefault.FONT_24,
    color: ColorsDefault.orange2,
    fontFamily: FontFamilyDefault.primaryBold,
    marginTop: '16@vs',
    marginBottom: '24@vs',
  },
  viewDash: {
    width: '140@s',
    height: 0,
    borderWidth: 1,
    borderColor: ColorsDefault.orange2,
    borderStyle: 'dashed',
  },
  wrapItem: {
    marginHorizontal: '16@s',
    marginTop: '24@vs',
  },
  viewHeaderItem: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: '12@vs',
  },
  textSubTitle: {
    fontSize: FontSizeDefault.FONT_16,
    color: ColorsDefault.black3,
    fontFamily: FontFamilyDefault.primaryMedium,
  },
  textChange: {
    fontSize: FontSizeDefault.FONT_16,
    color: ColorsDefault.blue1,
    fontFamily: FontFamilyDefault.primaryRegular,
  },
  viewRateStar: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  itemImage: {
    height: 96,
    width: 96,
  },
  textStar: {
    fontSize: FontSizeDefault.FONT_12,
    fontFamily: FontFamilyDefault.primaryRegular,
    color: ColorsDefault.black3,
    marginLeft: '4@s',
  },
  viewRightItem: {
    flex: 1,
    marginLeft: '8@s',
  },
  viewLeftItem: {},
  viewLineItem: {
    height: 1,
    width: '100%',
    backgroundColor: ColorsDefault.gray1,
    marginVertical: '12@vs',
  },
  viewHorizonItem: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  btnBuy: {
    height: '52@s',
    borderRadius: '52@s',
    width: FULL_WIDTH_DEVICE - s(32),
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: ColorsDefault.blue1,
    marginBottom: BOTTOM_PADDING,
  },
  textBuy: {
    fontSize: FontSizeDefault.FONT_16,
    fontFamily: FontFamilyDefault.primaryMedium,
    color: ColorsDefault.white,
  },
  btnCheckBox: {
    width: '26@s',
    height: '26@s',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default BabySpending;
