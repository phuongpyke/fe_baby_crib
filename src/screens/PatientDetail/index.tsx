import React, { useState } from 'react';
import { StyleSheet } from 'react-native';
import Toast from 'react-native-toast-message';
import { useNavigation } from '@react-navigation/native';
import {
    Body,
    Image,
    Text,
    Block,
    Input,
    Touchable,
    List,
} from '/components/base';
import { ColorsDefault } from '/assets';
import { WIDTH_DEVICE } from '/common';
import { Routes } from '/navigations/Routes';
import images from "/assets/images";

const data = {
    name: 'Khóc dạ đề',
    symptom: 'Bé co đầu gối lên gần phía ngực Khi khóc, mắt bé mở to hoặc nhắm thật chặt',
    createAt: '22.04.2022'

}

const PatientDetail = () => {
    const [showInput, setShowInput] = useState(false)
    const [text, setText] = useState('')
    return (
        <Body
            bg={ColorsDefault.white}
            loading={false}
            showsVerticalScrollIndicator={false}
            ph={16}>
            <Block row mt={32}>
                <Text fontSize={16}>Tên bệnh:</Text>
                <Text fontSize={16} fontWeight={'medium'}> {data.name}</Text>
            </Block>
            <Block mt={24} >
                <Text fontSize={16}>Triệu chứng:</Text>
                <Text lineHeight={22} fontSize={16} fontWeight={'medium'}>{data.symptom}</Text>
            </Block>
            <Block row mt={24} >
                <Text fontSize={16}>Ngày bị bệnh:</Text>
                <Text fontSize={16} fontWeight={'medium'}> {data.createAt}</Text>
            </Block>
            <Block row mt={24} middle>
                <Block row center>
                    <Text fontSize={16}>Ngày khỏi bệnh:</Text>
                    {!showInput && <Text fontSize={16} fontWeight={'medium'}> {data.createAt}</Text>}
                </Block>
                <Touchable onPress={() => setShowInput(true)}>
                    {
                        !showInput && <Image ml={16} mt={-6} source={images.edit} width={24} height={24}></Image>
                    }
                </Touchable>
            </Block>
            {
                showInput &&
                <Block>
                    <Input style={{ borderRadius: 8 }} value={text} onChangeText={setText} height={58} width={WIDTH_DEVICE - 32} placeholder={"VD : 22.04.2022"} mt={16}></Input>
                    <Touchable mt={32} center middle height={53} bg={ColorsDefault.blue1} borderRadius={8}>
                        <Text fontSize={16} color={ColorsDefault.white} fontWeight={'medium'}>Hoàn thành</Text>
                    </Touchable>
                    <Touchable onPress={() => setShowInput(false)} borderWidth={1} borderColor={ColorsDefault.gray} mt={16} center middle height={53} bg={ColorsDefault.white} borderRadius={8}>
                        <Text fontSize={16} color={ColorsDefault.gray3} fontWeight={'medium'}>Quay lại</Text>
                    </Touchable>
                </Block>
            }
        </Body>
    );
};

export default PatientDetail;
