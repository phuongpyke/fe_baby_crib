import {yupResolver} from '@hookform/resolvers/yup';
import {useNavigation} from '@react-navigation/native';
import React, {useLayoutEffect, useRef, useState} from 'react';
import isEqual from 'react-fast-compare';
import {Controller, useForm} from 'react-hook-form';
import {Image, Text, TextInput, TouchableOpacity, View} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {s, ScaledSheet} from 'react-native-size-matters';
import * as yup from 'yup';
import {getScheduleSettingTime} from '/api/modules/schedule';
import {ColorsDefault, FontFamilyDefault, FontSizeDefault} from '/assets';
import images from '/assets/images';
import {FULL_WIDTH_DEVICE} from '/common';
import {SelectTimeSchedule} from '/components/common';
import {goBack} from '/navigations';
import {useAppSelector} from '/redux/hooks';
import {showToastSuccess} from '/utils/helper';

const AddSchedule = () => {
  const {babyList} = useAppSelector(
    ({user: {babyList}}) => ({babyList}),
    isEqual,
  );
  const navigation: any = useNavigation();

  const [currentBtn, setCurrentBtn] = useState<number>(0);
  const selectTimeRef = useRef<any>(null);

  const yupSchema = yup.object().shape({
    calendar: yup.string().required('Lịch chăm bé không hợp lệ'),
  });
  const {
    handleSubmit,
    control,
    formState: {errors, isValid},
  } = useForm({
    mode: 'onChange',
    resolver: yupResolver(yupSchema),
    reValidateMode: 'onChange',
    criteriaMode: 'firstError',
  });

  const onIncrease = () => {
    setCurrentBtn(1);
    selectTimeRef.current?.add(30);
  };

  const onDecrease = () => {
    setCurrentBtn(2);
    selectTimeRef.current?.subtract(30);
  };

  const onSubmit = async (data: any) => {
    try {
      const res: any = await getScheduleSettingTime({
        baby_id: babyList?.[0]?.id,
        increase: 1,
        value: '00:30',
      });
      if (res?.success) {
        showToastSuccess(res?.message);
        goBack();
      }
    } catch (err) {
    } finally {
    }
  };

  useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <TouchableOpacity style={styles.btnHeader} onPress={goBack}>
          <Text style={styles.textLeftHeader}>{'Hủy'}</Text>
        </TouchableOpacity>
      ),
      headerRight: () => (
        <TouchableOpacity
          style={styles.btnHeader}
          onPress={handleSubmit(onSubmit)}
          disabled={!isValid}>
          <Text
            style={[
              styles.textRightHeader,
              isValid && {
                color: ColorsDefault.blue1,
              },
            ]}>
            {'Lưu'}
          </Text>
        </TouchableOpacity>
      ),
    });
  }, [isValid]);

  return (
    <KeyboardAwareScrollView
      style={styles.container}
      keyboardShouldPersistTaps="handled"
      showsVerticalScrollIndicator={false}
      enableResetScrollToCoords={true}>
      <Text style={styles.textSubTitle}>
        {'Bạn muốn tăng hay giảm khung giờ?'}
      </Text>
      <View style={styles.viewBtn}>
        <TouchableOpacity
          style={[styles.btn, currentBtn === 1 && styles.btnFocus]}
          onPress={onIncrease}>
          <Text style={styles.textBtn}>{'Tăng'}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.btn, currentBtn === 2 && styles.btnFocus]}
          onPress={onDecrease}>
          <Text style={styles.textBtn}>{'Giảm'}</Text>
        </TouchableOpacity>
      </View>
      <Text style={styles.textSubTitle}>{'Bạn muốn tăng/giảm bao lâu?'}</Text>
      <SelectTimeSchedule ref={selectTimeRef} boxStyle={styles.selectTime} />
      <Text style={styles.textSubTitle}>{'Tên lịch chăm bé'}</Text>
      <Controller
        control={control}
        render={({field: {onChange, onBlur, value}}) => (
          <TextInput
            style={[
              styles.textInput,
              errors?.calendar && styles.textInputError,
            ]}
            autoCorrect={false}
            placeholder="VD: E.A.S.Y - 1"
            placeholderTextColor={ColorsDefault.gray3}
            onChangeText={value => onChange(value)}
            onBlur={onBlur}
            value={value}
          />
        )}
        name="calendar"
        rules={{required: true}}
      />
      {!!errors?.calendar?.message && (
        <Text style={styles.textError}>{`${errors?.calendar?.message}`}</Text>
      )}
      <Image source={images.footerRegister} style={styles.imgFooter} />
    </KeyboardAwareScrollView>
  );
};

const styles = ScaledSheet.create({
  container: {
    backgroundColor: ColorsDefault.white,
    flex: 1,
  },
  btnHeader: {
    padding: 8,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textLeftHeader: {
    fontSize: FontSizeDefault.FONT_16,
    fontFamily: FontFamilyDefault.primaryRegular,
    color: ColorsDefault.blue1,
  },
  textRightHeader: {
    fontSize: FontSizeDefault.FONT_16,
    fontFamily: FontFamilyDefault.primaryBold,
    color: ColorsDefault.gray3,
  },
  textSubTitle: {
    fontSize: FontSizeDefault.FONT_18,
    fontFamily: FontFamilyDefault.primaryMedium,
    color: ColorsDefault.black3,
    marginHorizontal: '16@s',
    marginTop: '36@vs',
    marginBottom: '12@vs',
  },
  viewBtn: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: '16@s',
  },
  btn: {
    height: '58@s',
    borderRadius: '58@s',
    justifyContent: 'center',
    alignItems: 'center',
    width: (FULL_WIDTH_DEVICE - s(48)) / 2,
    backgroundColor: ColorsDefault.gray1,
  },
  btnFocus: {
    backgroundColor: ColorsDefault.purple1,
    borderColor: ColorsDefault.blue1,
    borderWidth: 1,
  },
  textBtn: {
    fontSize: FontSizeDefault.FONT_16,
    fontFamily: FontFamilyDefault.primaryRegular,
    color: ColorsDefault.black3,
  },
  textInput: {
    height: '58@s',
    borderRadius: '58@s',
    backgroundColor: ColorsDefault.gray1,
    width: FULL_WIDTH_DEVICE - s(32),
    alignSelf: 'center',
    paddingHorizontal: '16@s',
    fontFamily: FontFamilyDefault.primaryRegular,
    fontSize: FontSizeDefault.FONT_18,
    color: ColorsDefault.black3,
  },
  textInputError: {
    borderWidth: 1,
    borderColor: ColorsDefault.textRed,
  },
  selectTime: {
    backgroundColor: ColorsDefault.white,
    borderWidth: 1,
    borderColor: ColorsDefault.gray1,
  },
  textError: {
    fontSize: FontSizeDefault.FONT_12,
    fontFamily: FontFamilyDefault.primaryRegular,
    color: ColorsDefault.textRed,
    marginTop: '4@vs',
    marginHorizontal: '16@s',
  },
  imgFooter: {
    bottom: 0,
    right: 0,
    left: 0,
    zIndex: -10,
    width: FULL_WIDTH_DEVICE,
    height: FULL_WIDTH_DEVICE * 0.7,
  },
});

export default AddSchedule;
