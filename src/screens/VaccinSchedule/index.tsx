import {useNavigation} from '@react-navigation/native';
import React, {useEffect, useLayoutEffect, useState} from 'react';
import isEqual from 'react-fast-compare';
import {FlatList, Text, TouchableOpacity, View} from 'react-native';
import RenderHtml, {defaultSystemFonts} from 'react-native-render-html';
import {s, ScaledSheet} from 'react-native-size-matters';
import StaticSafeAreaInsets from 'react-native-static-safe-area-insets';
import reactotron from 'reactotron-react-native';
import {getVaccineSchedule} from '/api/modules/vaccine';
import {ColorsDefault, FontFamilyDefault, FontSizeDefault} from '/assets';
import {BOTTOM_PADDING, FULL_WIDTH_DEVICE} from '/common';
import {navigate} from '/navigations';
import {Routes} from '/navigations/Routes';
import {useAppSelector} from '/redux/hooks';
import {tagsStyles} from '/utils/helper';

const VaccinSchedule = () => {
  const navigation: any = useNavigation();
  const {babyList} = useAppSelector(
    ({user: {babyList}}) => ({babyList}),
    isEqual,
  );
  const [listVaccine, setListVaccine] = useState<any[]>([]);

  useEffect(() => {
    const getData = async () => {
      try {
        const res: any = await getVaccineSchedule();
        if (res?.success) {
          setListVaccine(res?.data);
        }
      } catch (err) {
        reactotron.warn!(err);
      }
    };
    getData();
  }, []);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <TouchableOpacity
          style={styles.btnHeader}
          onPress={() => navigate(Routes.ListVaccination)}>
          <Text style={[styles.textRightHeader]}>{'Các mũi tiêm'}</Text>
        </TouchableOpacity>
      ),
    });
  }, []);

  const renderItem = ({item}: any) => (
    <View style={styles.wrapItem}>
      <View style={styles.viewLeftItem}>
        <Text style={styles.textTime}>{item.time}</Text>
      </View>
      <View style={styles.viewRightItem}>
        <Text style={styles.textTitle}>{item.title}</Text>
        {!!item?.desc && (
          <RenderHtml
            contentWidth={FULL_WIDTH_DEVICE * 0.66}
            source={{html: item?.desc}}
            defaultTextProps={{
              allowFontScaling: false,
            }}
            tagsStyles={tagsStyles}
            systemFonts={[...defaultSystemFonts]}
          />
        )}
      </View>
    </View>
  );

  return (
    <View style={styles.container}>
      <FlatList
        contentContainerStyle={styles.contentScroll}
        showsVerticalScrollIndicator={false}
        data={listVaccine}
        renderItem={renderItem}
      />
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    backgroundColor: ColorsDefault.white,
    flex: 1,
    paddingBottom: BOTTOM_PADDING,
  },
  contentScroll: {
    paddingBottom: StaticSafeAreaInsets.safeAreaInsetsBottom,
  },
  btnHeader: {
    padding: 8,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textRightHeader: {
    fontSize: FontSizeDefault.FONT_16,
    fontFamily: FontFamilyDefault.primaryRegular,
    color: ColorsDefault.black3,
  },
  wrapItem: {
    width: FULL_WIDTH_DEVICE - s(32),
    backgroundColor: ColorsDefault.blue7,
    alignSelf: 'center',
    borderRadius: 12,
    marginTop: '12@vs',
    paddingVertical: '16@vs',
    paddingHorizontal: '12@s',
    flexDirection: 'row',
  },
  viewLeftItem: {
    flex: 1,
  },
  textTime: {
    fontSize: FontSizeDefault.FONT_16,
    fontFamily: FontFamilyDefault.primaryRegular,
    color: ColorsDefault.black3,
  },
  viewRightItem: {
    width: FULL_WIDTH_DEVICE * 0.66,
  },
  textTitle: {
    fontSize: FontSizeDefault.FONT_16,
    fontFamily: FontFamilyDefault.primaryBold,
    color: ColorsDefault.black3,
  },
  textSubTitle: {
    fontSize: FontSizeDefault.FONT_14,
    fontFamily: FontFamilyDefault.primaryRegular,
    color: ColorsDefault.black3,
  },
  textSchedule: {
    fontSize: FontSizeDefault.FONT_14,
    fontFamily: FontFamilyDefault.primaryRegular,
    color: ColorsDefault.black3,
    marginLeft: '5@s',
  },
});

export default VaccinSchedule;
