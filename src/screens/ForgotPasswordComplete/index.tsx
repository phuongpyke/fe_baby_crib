import React, {useState} from 'react';
import {ColorsDefault} from '/assets';
import {Block, Body, Text, Touchable, Input, Image} from '/components';
import images from '/assets/images';
import {forgotPasswordAPI} from '/services/api';
import Toast from 'react-native-toast-message';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import {FormikProps, useFormik} from 'formik';
import {WIDTH_DEVICE} from '/common';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import {RouteProp, useNavigation} from '@react-navigation/native';
import {Routes} from '/navigations/Routes';

interface Props {
  route: RouteProp<
    {
      params: {
        phone: string;
      };
    },
    'params'
  >;
}

interface MyValues {
  password: string;
  rePassword: string;
}

const validate = (values: any) => {
  const errors: any = {};
  if (!values.password.trim()) {
    errors.password = 'Hãy nhập số điện thoại';
  }

  if (!values.rePassword.trim()) {
    errors.rePassword = 'Hãy nhập lại mật khẩu';
  }
  if (values.rePassword !== values.password) {
    errors.rePassword = 'Mật khẩu chưa trùng khớp';
  }
  return errors;
};
export const ForgotPasswordComplete = (props: Props) => {
  const [loading, setLoading] = useState<boolean>(false);
  const navigation = useNavigation<any>();
  const [isShowPass, setIsShowPass] = useState(false);
  const [isShowRePass, setIsShowRePass] = useState(false);
  const {phone} = props.route.params;

  const formik: FormikProps<MyValues> = useFormik<MyValues>({
    initialValues: {password: '', rePassword: ''},
    validate,
    onSubmit: () => {},
  });

  const onSubmit = async () => {
    setLoading(true);
    try {
      await forgotPasswordAPI({
        phone,
        new_password: formik.values?.password,
      });
      Toast.show({
        type: 'custom',
        text1: 'Bạn đã đổi mật khẩu thành công',
        props: {
          type: 'success',
        },
      });
      navigation.navigate(Routes.Login);
    } catch (error: any) {
      Toast.show({
        type: 'custom',
        props: {
          type: 'error',
        },
        text1: `${error?.errorMessage || error}`,
      });
    }
    setLoading(false);
  };

  return (
    <Body scroll keyboardAvoid bg={ColorsDefault.white} loading={loading}>
      <Image
        source={images.headerForgotPassword}
        width={WIDTH_DEVICE}
        height={181}>
        <Block mt={58} pl={16} row justifyBetween middle>
          <Touchable onPress={() => navigation.goBack()}>
            <Image
              source={images.backLeft}
              tintColor={ColorsDefault.black}
              width={24}
              height={24}
            />
          </Touchable>
          <Text type="p" fontWeight="semiBold">
            Tạo mật khẩu mới
          </Text>
          <Block width={24}></Block>
        </Block>
      </Image>
      <Block flex={1} mh={16} mt={24}>
        <Text type="p" fontWeight="medium" textCenter>
          Mật khẩu dùng để đăng nhập.
        </Text>
        <Input
          mt={24}
          width={WIDTH_DEVICE - 32}
          style={{
            borderWidth: 1,
            borderRadius: 60,
            borderColor: ColorsDefault.gray1,
          }}
          height={58}
          value={formik.values?.password}
          onChangeText={formik.handleChange('password')}
          placeholder="Mật khẩu mới"
          error={formik.errors.password && formik.touched.password}
          errorMessage={formik.errors.password}
          onBlur={formik.handleBlur('password')}
          iconRightPress={() => {
            setIsShowPass(!isShowPass);
          }}
          iconRight={isShowPass ? images.show : images.hide}
          secureTextEntry={!isShowPass}
        />
        <Input
          mt={16}
          width={WIDTH_DEVICE - 32}
          style={{
            borderWidth: 1,
            borderRadius: 60,
            borderColor: ColorsDefault.gray1,
          }}
          height={58}
          value={formik.values?.rePassword}
          onChangeText={formik.handleChange('rePassword')}
          placeholder="Nhập lại mật khẩu mới"
          error={formik.errors.rePassword && formik.touched.rePassword}
          errorMessage={formik.errors.rePassword}
          onBlur={formik.handleBlur('rePassword')}
          iconRightPress={() => {
            setIsShowRePass(!isShowRePass);
          }}
          iconRight={isShowRePass ? images.show : images.hide}
          secureTextEntry={!isShowRePass}
        />
        <Touchable
          onPress={onSubmit}
          disabled={
            !(
              !formik.errors.password &&
              !formik.errors.rePassword &&
              formik.values.password &&
              formik.values.rePassword
            )
          }
          middle
          mt={16}
          center
          borderRadius={60}
          bg={
            !(
              !formik.errors.password &&
              !formik.errors.rePassword &&
              formik.values.password &&
              formik.values.rePassword
            )
              ? ColorsDefault.gray2
              : ColorsDefault.blue1
          }
          height={53}>
          <Text type="p" color={ColorsDefault.white} fontWeight={'bold'}>
            Hoàn thành
          </Text>
        </Touchable>
      </Block>
      <Block style={{position: 'absolute', bottom: 0, left: 0, width: 0}}>
        <Image
          center
          middle
          source={images.footerRegister}
          width={WIDTH_DEVICE}
          height={265}
        />
      </Block>
    </Body>
  );
};
