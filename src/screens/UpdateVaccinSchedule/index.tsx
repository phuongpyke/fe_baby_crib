import {yupResolver} from '@hookform/resolvers/yup';
import moment from 'moment';
import React, {useRef, useState} from 'react';
import {Controller, useForm} from 'react-hook-form';
import {Image, Text, TextInput, TouchableOpacity} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import Modal from 'react-native-modal';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import {s, ScaledSheet} from 'react-native-size-matters';
import * as yup from 'yup';
import {updateVaccine} from '/api/modules/vaccine';
import {ColorsDefault, FontFamilyDefault, FontSizeDefault} from '/assets';
import images from '/assets/images';
import {FULL_WIDTH_DEVICE} from '/common';
import {ModalStatusVaccin} from '/components/common';
import {goBack} from '/navigations';
import {showToastError, showToastSuccess} from '/utils/helper';

const statusData = [
  {
    label: 'Đã tiêm',
    value: 1,
    icon: images.check,
    color: ColorsDefault.green2,
  },
  {
    label: 'Chưa tiêm',
    value: 0,
    icon: images.close,
    color: ColorsDefault.orange2,
  },
];

const DEFAULT_CUSTOMER = {
  name: '',
  status: 0,
  date: new Date(),
};

const UpdateVaccinSchedule = ({route}: any) => {
  const {babyId, vaccineId, numberIndex} = route.params ?? {};

  const statusVaccinRef = useRef<any>(null);
  const [isVisible, setIsVisible] = useState<boolean>(false);

  const yupSchema = yup.object().shape({
    name: yup.string().required('Tên mũi tiêm không hợp lệ'),
    status: yup.string().required(''),
    date: yup.string().required(''),
  });
  const {
    getValues,
    setValue,
    handleSubmit,
    control,
    formState: {errors, isValid},
  } = useForm({
    defaultValues: DEFAULT_CUSTOMER,
    mode: 'onChange',
    resolver: yupResolver(yupSchema),
    reValidateMode: 'onChange',
    criteriaMode: 'firstError',
  });

  const onConfirmDate = (date: Date) => {
    setValue('date', date, {
      shouldValidate: true,
    });
    hideDatePicker();
  };

  const onCancelDate = () => {
    hideDatePicker();
  };

  const showDatePicker = () => {
    setIsVisible(true);
  };

  const hideDatePicker = () => {
    setIsVisible(false);
  };

  const onChangeStatus = (data: any) => {
    setValue('status', data?.value, {
      shouldValidate: true,
    });
  };

  const onSubmit = async (form: any) => {
    try {
      const date = moment(form?.date).format('YYYY-MM-DD');
      const res: any = await updateVaccine({
        baby_id: babyId,
        vaccine_id: vaccineId,
        number_index: numberIndex,
        is_inject: form?.status,
        date,
      });
      if (res.success) {
        showToastSuccess(res.message);
        goBack();
      }
    } catch (err: any) {
      showToastError(err?.message);
    } finally {
    }
  };

  const getStatusVaccin = () => {
    const status = getValues('status');
    const findIndex = statusData.find((item: any) => item.value === status);
    return findIndex?.label;
  };

  return (
    <KeyboardAwareScrollView
      style={styles.container}
      contentContainerStyle={styles.contentScroll}
      keyboardShouldPersistTaps="handled"
      showsVerticalScrollIndicator={false}
      enableResetScrollToCoords={true}>
      <Text style={styles.textSubTitle}>{'Tiêm phòng'}</Text>
      <Controller
        control={control}
        render={({field: {onChange, onBlur, value}}) => (
          <TextInput
            style={[styles.textInput, errors?.name && styles.textInputError]}
            autoCorrect={false}
            placeholder=""
            placeholderTextColor={ColorsDefault.gray3}
            onChangeText={value => onChange(value)}
            onBlur={onBlur}
            value={value}
          />
        )}
        name="name"
        rules={{required: true}}
      />
      {!!errors?.name?.message && (
        <Text style={styles.textError}>{errors?.name?.message}</Text>
      )}
      <Text style={styles.textSubTitle}>{'Trạng thái'}</Text>
      <TouchableOpacity
        style={styles.btnStatusVaccin}
        onPress={() => {
          statusVaccinRef.current?.show();
        }}>
        <Text style={styles.textVaccinDate}>{getStatusVaccin()}</Text>
        <Image source={images.bottomArrow} style={{height: 24, width: 24}} />
      </TouchableOpacity>
      <Text style={styles.textSubTitle}>{'Ngày tiêm'}</Text>
      <TouchableOpacity style={styles.btnStatusVaccin} onPress={showDatePicker}>
        <Text style={styles.textVaccinDate}>
          {moment(getValues('date')).format('DD/MM/YYYY')}
        </Text>
        <Image source={images.calendar3} style={{height: 24, width: 24}} />
      </TouchableOpacity>
      <TouchableOpacity
        disabled={!isValid}
        style={[
          styles.btnSubmit,
          !isValid && {backgroundColor: ColorsDefault.gray2},
        ]}
        onPress={handleSubmit(onSubmit)}>
        <Text style={styles.textSubmit}>{'Thêm'}</Text>
      </TouchableOpacity>
      <Modal
        isVisible={isVisible}
        onBackdropPress={hideDatePicker}
        backdropColor={ColorsDefault.black2}>
        <DateTimePickerModal
          isVisible={isVisible}
          date={getValues('date')}
          mode="date"
          locale="en_GB"
          onConfirm={onConfirmDate}
          onCancel={onCancelDate}
          confirmTextIOS={'Xác nhận'}
          cancelTextIOS={'Hủy'}
          maximumDate={new Date()}
        />
      </Modal>
      <ModalStatusVaccin
        ref={statusVaccinRef}
        options={statusData}
        onChangeValue={onChangeStatus}
      />
    </KeyboardAwareScrollView>
  );
};

const styles = ScaledSheet.create({
  container: {
    backgroundColor: ColorsDefault.white,
    flex: 1,
  },
  contentScroll: {},
  textSubTitle: {
    fontSize: FontSizeDefault.FONT_18,
    fontFamily: FontFamilyDefault.primaryMedium,
    color: ColorsDefault.black3,
    marginHorizontal: '16@s',
    marginBottom: '12@vs',
    marginTop: '24@vs',
  },
  textInput: {
    height: '58@s',
    borderRadius: '58@s',
    backgroundColor: ColorsDefault.gray1,
    width: FULL_WIDTH_DEVICE - s(32),
    alignSelf: 'center',
    paddingHorizontal: '16@s',
    fontFamily: FontFamilyDefault.primaryRegular,
    fontSize: FontSizeDefault.FONT_18,
    color: ColorsDefault.black3,
  },
  textInputError: {
    borderWidth: 1,
    borderColor: ColorsDefault.textRed,
  },
  selectTime: {
    backgroundColor: ColorsDefault.white,
    borderWidth: 1,
    borderColor: ColorsDefault.gray1,
  },
  textError: {
    fontSize: FontSizeDefault.FONT_12,
    fontFamily: FontFamilyDefault.primaryRegular,
    color: ColorsDefault.textRed,
    marginTop: '4@vs',
    marginHorizontal: '16@s',
  },
  btnStatusVaccin: {
    height: '58@s',
    borderRadius: '58@s',
    backgroundColor: ColorsDefault.gray1,
    width: FULL_WIDTH_DEVICE - s(32),
    alignSelf: 'center',
    paddingHorizontal: '16@s',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  btnSubmit: {
    height: '52@s',
    borderRadius: '58@s',
    width: FULL_WIDTH_DEVICE - s(32),
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: ColorsDefault.blue1,
    marginTop: '45@vs',
  },
  textSubmit: {
    fontSize: FontSizeDefault.FONT_16,
    fontFamily: FontFamilyDefault.primaryBold,
    color: ColorsDefault.white,
  },
  textVaccinDate: {
    fontSize: FontSizeDefault.FONT_18,
    fontFamily: FontFamilyDefault.primaryRegular,
    color: ColorsDefault.black3,
  },
});

export default UpdateVaccinSchedule;
