import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {Alert, ScrollView, StyleSheet} from 'react-native';
import OneSignal from 'react-native-onesignal';
import {useDispatch, useSelector} from 'react-redux';
import {getAge} from '../../utils';
import {ColorsDefault} from '/assets';
import images from '/assets/images';
import {BOTTOM_PADDING, WIDTH_DEVICE} from '/common';
import {Block, Body, Image, Text, Touchable} from '/components';
import {logout} from '/modules/auth/slice';
import {babyListSelector} from '/modules/user/selectors';
import {Routes} from '/navigations/Routes';

const Settings = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation<any>();
  const listBaby = useSelector(babyListSelector);
  const babyInfo = listBaby[0];

  const Item = ({title, onPress, textRight}: any) => {
    return (
      <Touchable
        mt={12}
        ph={16}
        shadow
        middle
        bg={ColorsDefault.white}
        activeOpacity={0.1}
        mh={16}
        onPress={onPress}
        style={styles.item}>
        <Text fontSize={16} fontWeight={'medium'} lineHeight={22}>
          {title}
        </Text>

        <Block row middle>
          {textRight && (
            <Text
              color={ColorsDefault.blue1}
              fontSize={16}
              fontWeight={'medium'}>
              {textRight}
            </Text>
          )}
          <Image source={images.rightArrow} height={16} width={16} />
        </Block>
      </Touchable>
    );
  };

  const onClickLogout = async () => {
    Alert.alert('', 'Bạn có muốn đăng xuất không？', [
      {
        text: 'Không',
        style: 'cancel',
      },
      {
        text: 'Có',
        onPress: onLogOut,
      },
    ]);
  };

  const onLogOut = async () => {
    dispatch(logout());
    OneSignal.removeExternalUserId();
    navigation.navigate(Routes.AuthStack, {
      screen: Routes.Login,
    });
  };

  const goEditBabyInfo = (data: any) => {
    navigation.navigate(Routes.EditProfile, {mode: 'edit', userInfo: data});
  };

  const goAddNewBaby = () => {
    navigation.navigate(Routes.EditProfile, {mode: 'new'});
  };

  const goAddCrib = () => {
    navigation.navigate(Routes.AddCrib);
  };

  return (
    <ScrollView
      style={styles.container}
      contentContainerStyle={styles.contentScroll}
      showsVerticalScrollIndicator={false}>
      <Image
        source={images.headerSetting}
        width={WIDTH_DEVICE}
        height={259}
        middle>
        <Block
          mt={79}
          width={80}
          height={80}
          borderWidth={2}
          borderColor={ColorsDefault.gray}
          middle
          center
          borderRadius={40}
          bg={ColorsDefault.white}>
          <Image
            source={images.babyBoy1}
            style={{zIndex: 11111}}
            width={50}
            height={50}></Image>
        </Block>
        <Text
          fontWeight={'semiBold'}
          textCenter
          mt={12}
          type={'p'}
          color={ColorsDefault.white}>
          {babyInfo.name}
        </Text>
        <Text color={ColorsDefault.white} type="h4" mt={24}>
          {babyInfo.gender === 1 ? 'Nam' : 'Nữ'} - {getAge(babyInfo.dob)} ngày -
          {babyInfo.weight?.toString().replace('.', ',')} kg
        </Text>
      </Image>
      <Block shadow bg={ColorsDefault.white} mh={16} pv={16} mt={8}>
        <Text type="h4" fontWeight="medium" pl={16}>
          Hồ sơ con tôi
        </Text>
        {listBaby.map((item: any, index: number) => {
          return (
            <Block mh={16} row mt={8} middle justifyBetween key={index}>
              <Block row>
                <Block
                  width={48}
                  height={48}
                  borderRadius={48}
                  middle
                  center
                  borderWidth={1}
                  borderColor={ColorsDefault.blue1}>
                  <Image
                    width={30}
                    height={30}
                    source={images.babyBoy1}></Image>
                </Block>
                <Block ml={12} mt={8}>
                  <Text fontWeight="medium">{item.name}</Text>
                  <Text color={ColorsDefault.gray3}>{item.dob}</Text>
                </Block>
              </Block>
              <Touchable onPress={() => goEditBabyInfo(item)}>
                <Text type="h5" fontWeight="medium" color={ColorsDefault.blue1}>
                  Chỉnh sửa
                </Text>
              </Touchable>
            </Block>
          );
        })}
        <Block height={1} bg={ColorsDefault.gray} mt={16} mh={16}></Block>
        <Touchable mh={16} mt={16} row middle onPress={goAddNewBaby}>
          <Image source={images.addBaby} width={24} height={24}></Image>
          <Text type="h4" fontWeight="medium" mt={4} ml={12}>
            Thêm bé
          </Text>
        </Touchable>
      </Block>
      <Item title={'Thêm nôi'} onPress={goAddCrib} />
      <Item title={'Đổi mật khẩu'} />
      <Item title={'Thay đổi ngôn ngữ'} textRight={'Tiếng Việt'} />
      <Item title={'Đăng xuất'} onPress={onClickLogout} />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: ColorsDefault.white,
  },
  contentScroll: {
    flexGrow: 1,
    paddingBottom: 20,
  },
  item: {
    height: 68,
    borderBottomColor: '#F5F5F5',
    borderBottomWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});

export default Settings;
