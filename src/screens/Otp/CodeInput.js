import React, {useRef, useEffect} from 'react';
import Code from '../../assets/icons/code_none.png';
import {Image, View, TextInput} from 'react-native';
import {ColorsDefault} from '/assets';

const CodeInput = ({value, style, onChange, currentFocus, position}) => {
  const onChangeV = v => {
    console.log('v', v);
    onChange(v);
  };
  const ref = useRef(null);
  useEffect(() => {
    if (currentFocus === position) {
      ref.current.focus();
    } else {
      ref.current.blur();
    }
  }, [currentFocus, position]);

  return (
    <View
      style={{
        width: 56,
        height: 56,
        alignItems: 'center',
        justifyContent: 'center',
        ...style,
      }}>
      <Image
        source={Code}
        style={{width: 56, height: 56, position: 'absolute'}}
      />
      <TextInput
        style={{
          color: ColorsDefault.black,
          fontSize: 16,
          width: 50,
          textAlign: 'center',
          backgroundColor:
            value && value.length > 0
              ? ColorsDefault.white
              : ColorsDefault.transparent,
        }}
        value={value}
        onChangeText={onChangeV}
        maxLength={1}
        ref={ref}
        keyboardType="numeric"
      />
    </View>
  );
};

export default CodeInput;
