import {RouteProp, useNavigation} from '@react-navigation/native';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import React, {useEffect, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {Block, Body, Image, Text, Touchable} from '/components';
import {ColorsDefault} from '/assets';
import images from '/assets/images';
import {WIDTH_DEVICE} from '/common';
import {Routes} from '/navigations/Routes';
interface Props {
  route: RouteProp<
    {
      params: {
        code: string;
        phone: string;
        onRegister?: (cb?: any) => void;
        isForgotPassword?: boolean;
        confirmation?: any;
      };
    },
    'params'
  >;
}

const OTP_LENGTH = 6;
const OTP_SIZE = (WIDTH_DEVICE - 52) / 6;

const Otp = (props: Props) => {
  const navigation = useNavigation<any>();
  const {code, phone, onRegister, isForgotPassword, confirmation} =
    props.route.params;
  const [timerCount, setTimer] = useState(60);
  const [otp, setOtp] = useState('');
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);

  useEffect(() => {
    let interval = setInterval(() => {
      setTimer(lastTimerCount => {
        lastTimerCount <= 1 && clearInterval(interval);
        return lastTimerCount - 1;
      });
    }, 1000);
    return () => clearInterval(interval);
  }, []);

  const onSubmit = async () => {
    try {
      setLoading(true);
      await confirmation.confirm(otp);
      if (onRegister) {
        onRegister(() => {
          navigation.navigate(Routes.Login);
        });
      }
      if (isForgotPassword) {
        navigation.navigate(Routes.ForgotPasswordComplete, {
          phone,
        });
      }
    } catch (err) {
      setError(true);
    } finally {
      setLoading(false);
    }
  };

  return (
    <Body scroll keyboardAvoid bg={ColorsDefault.white} loading={loading}>
      <Image
        resizeMode={'stretch'}
        source={images.headerForgotPassword}
        width={WIDTH_DEVICE}
        height={181}>
        <Block mt={58} pl={16} row justifyBetween middle>
          <Touchable onPress={() => navigation.goBack()}>
            <Image
              source={images.backLeft}
              tintColor={ColorsDefault.black}
              width={24}
              height={24}
            />
          </Touchable>
          <Text type="p" fontWeight="semiBold">
            Xác minh OTP
          </Text>
          <Block width={24}></Block>
        </Block>
      </Image>
      <Text textCenter mt={24} type="p" ph={24} lineHeight={26}>
        Chúng tôi đã gửi mã xác nhận đến số điện thoại:
        <Text type="p" fontWeight={'bold'}>
          {'  '}
          {phone}
        </Text>
      </Text>
      <OTPInputView
        style={{
          width: WIDTH_DEVICE - 32,
          alignSelf: 'center',
          height: 80,
          marginTop: 24,
        }}
        pinCount={OTP_LENGTH}
        autoFocusOnLoad
        onCodeChanged={code => {
          if (code.length !== OTP_LENGTH) {
            setError(false);
          }
        }}
        codeInputFieldStyle={[
          styles.underlineStyleBase,
          {
            borderColor: error ? ColorsDefault.red : ColorsDefault.blue1,
            color: error ? ColorsDefault.red : ColorsDefault.black,
          },
        ]}
        codeInputHighlightStyle={styles.underlineStyleHighLighted}
        onCodeFilled={input => {
          setOtp(input);
          // if (`${code}` === `${input}`) {
          //   if (onRegister) {
          //     setLoading(true);
          //     onRegister(() => {
          //       setLoading(false);
          //       navigation.replace(Routes.Login);
          //     });
          //   }
          // }
        }}
      />
      {error && (
        <Text mt={32} textCenter type="h4" color={ColorsDefault.red}>
          {' '}
          Mã xác nhận chưa đúng
        </Text>
      )}
      <View style={styles.footer}>
        <Touchable onPress={() => {}} disabled={timerCount > 0}>
          <Text
            color={timerCount > 0 ? ColorsDefault.gray4 : ColorsDefault.black}
            fontSize={16}
            fontWeight={'semiBold'}>{`Gửi lại (${timerCount})`}</Text>
        </Touchable>
        <Touchable
          width={60}
          height={60}
          borderRadius={50}
          bg={ColorsDefault.blue1}
          center
          middle
          onPress={onSubmit}>
          <Image
            source={images.rightArrow}
            height={20}
            width={20}
            tintColor={ColorsDefault.white}
          />
        </Touchable>
      </View>
    </Body>
  );
};

export default Otp;

const styles = StyleSheet.create({
  footer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    paddingBottom: 20,
    paddingHorizontal: 20,
  },
  underlineStyleBase: {
    width: OTP_SIZE,
    height: OTP_SIZE,
    borderWidth: 1,
    borderRadius: OTP_SIZE / 2,
    fontSize: 24,
  },
  underlineStyleHighLighted: {
    width: OTP_SIZE,
    height: OTP_SIZE,
    borderWidth: 1,
    borderRadius: OTP_SIZE / 2,
    fontSize: 24,
  },
});
