import {Buffer} from 'buffer';
import {stringToBytes} from 'convert-string';
import {concat, includes} from 'lodash';
import React, {FC, useEffect, useRef, useState} from 'react';
import isEqual from 'react-fast-compare';
import {FlatList, Image, Text, TouchableOpacity, View} from 'react-native';
import BleManager from 'react-native-ble-manager';
import {ms, s, ScaledSheet} from 'react-native-size-matters';
import {ColorsDefault, FontFamilyDefault, FontSizeDefault} from '/assets';
import images from '/assets/images';
import {BOTTOM_PADDING} from '/common';
import {Loading} from '/components';
import {ModalWifiSetting} from '/components/common';
import {setBleConnect} from '/modules/user/slice';
import {navigate} from '/navigations';
import {Routes} from '/navigations/Routes';
import {useAppDispatch, useAppSelector} from '/redux/hooks';
import {showToastError} from '/utils/helper';

const wifiCharacteristic = '00000010-373F-11EC-8D3D-0242AC130003';
const wifiService = '00000001-373F-11EC-8D3D-0242AC130003';
const writeWifi = '00000012-373F-11EC-8D3D-0242AC130003';

const ListWifi: FC = ({route}: any) => {
  const {peripheralId} = route.params ?? {};
  const dispatch = useAppDispatch();
  const {bleConnect} = useAppSelector(
    ({user: {bleConnect}}) => ({bleConnect}),
    isEqual,
  );

  const wifiModalRef = useRef<any>(null);

  const [listWifi, setListWifi] = useState([]);

  const goLoadingWifi = () => {
    navigate(Routes.LoadingWifi);
  };

  const onWifiSetting = (data: any) => {
    wifiModalRef.current?.show({data});
  };

  const onWifiConnect = async (name: string, pass: string) => {
    try {
      // console.log('name: ', name, 'pass: ', pass);

      const peripheralData = await BleManager.retrieveServices(peripheralId);
      // console.log('peripheralData', peripheralData);

      const findItem = peripheralData?.characteristics?.find(
        item => item.characteristic.toUpperCase() === writeWifi,
      );
      // console.log('findItem', findItem);

      if (findItem) {
        const data = stringToBytes(`${name}_${pass}`);
        await BleManager.writeWithoutResponse(
          peripheralId,
          findItem.service,
          writeWifi,
          data,
        );
        wifiModalRef.current?.hide();
        setTimeout(() => {
          if (!includes(bleConnect, peripheralId)) {
            dispatch(setBleConnect(concat(bleConnect, peripheralId)));
            goLoadingWifi();
          }
        }, 300);
      }
    } catch (err) {
      console.log(err);
      wifiModalRef.current?.hide();
      showToastError('Có lỗi xảy ra. Vui lòng thử lại!');
    }
  };

  useEffect(() => {
    const getData = async () => {
      try {
        await BleManager.connect(peripheralId);
        setTimeout(async () => {
          const retrieveData = await BleManager.retrieveServices(peripheralId);
          console.log('retrieveData', retrieveData);
          const findWifi = retrieveData.characteristics?.find(
            item => item.characteristic.toUpperCase() === wifiCharacteristic,
          );
          if (findWifi) {
            let stringWifi = await BleManager.read(
              peripheralId,
              wifiService,
              wifiCharacteristic,
            );
            stringWifi = Buffer.from(stringWifi).toString();
            if (stringWifi && stringWifi.split('_').length) {
              // console.log('stringWifi', stringWifi);
              setListWifi(stringWifi.split('_'));
            }
          }
        }, 3000);
      } catch (err) {
        console.log(err);
      }
    };
    getData();
  }, []);

  const renderItem = ({item}: any) => (
    <TouchableOpacity
      onPress={() => onWifiSetting(item)}
      style={{
        paddingVertical: ms(16),
        borderBottomWidth: 1,
        borderBottomColor: ColorsDefault.gray1,
        flexDirection: 'row',
        alignItems: 'center',
      }}>
      <View style={{flex: 1}}>
        <Text
          numberOfLines={1}
          style={{
            fontSize: FontSizeDefault.FONT_16,
            fontFamily: FontFamilyDefault.primarySemiBold,
            color: ColorsDefault.black3,
          }}>
          {item}
        </Text>
      </View>
      <View
        style={{flexDirection: 'row', alignItems: 'center', marginLeft: 12}}>
        <Image
          source={images.lock}
          style={{width: 16, height: 16, marginRight: 12}}
          resizeMode={'contain'}
        />
        <Image
          source={images.wifi}
          style={{width: 16, height: 16, marginRight: 12}}
          resizeMode={'contain'}
        />
        <Image
          source={images.wifiInfo}
          style={{width: 16, height: 16}}
          resizeMode={'contain'}
        />
      </View>
    </TouchableOpacity>
  );

  const renderHeader = () => (
    <View
      style={{flexDirection: 'row', alignItems: 'center', marginTop: ms(25)}}>
      <Text
        style={{
          fontSize: FontSizeDefault.FONT_16,
          fontFamily: FontFamilyDefault.primaryRegular,
          color: ColorsDefault.gray3,
        }}>
        {'MẠNG'}
      </Text>
      <Loading ml={8} />
    </View>
  );

  const renderNoData = () => (
    <View
      style={{
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Text
        style={{
          fontSize: FontSizeDefault.FONT_18,
          fontFamily: FontFamilyDefault.primaryRegular,
          color: ColorsDefault.black3,
        }}>
        {'Không tìm thấy mạng nào'}
      </Text>
    </View>
  );

  return (
    <View style={styles.container}>
      <FlatList
        style={{flex: 1}}
        contentContainerStyle={{
          paddingHorizontal: s(16),
          flexGrow: 1,
          paddingBottom: BOTTOM_PADDING,
        }}
        showsVerticalScrollIndicator={false}
        data={listWifi}
        renderItem={renderItem}
        ListHeaderComponent={renderHeader}
        ListEmptyComponent={renderNoData}
        keyExtractor={(_, index: number) => `${index}`}
      />
      <ModalWifiSetting ref={wifiModalRef} onSubmit={onWifiConnect} />
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: ColorsDefault.white,
  },
});

export default ListWifi;
