import React, {FC} from 'react';
import {Text, TouchableOpacity} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {ScaledSheet} from 'react-native-size-matters';
import {ColorsDefault, FontFamilyDefault, FontSizeDefault} from '/assets';
import {Routes} from '/navigations/Routes';

const AddCrib: FC = ({navigation}: any) => {
  const goQrScreen = () => {
    navigation.navigate(Routes.QrScreen);
  };

  return (
    <SafeAreaView style={styles.container}>
      <Text
        style={{
          fontSize: FontSizeDefault.FONT_14,
          fontFamily: FontFamilyDefault.primaryMedium,
          color: ColorsDefault.black3,
          textAlign: 'center',
        }}>
        {'Quét QR để active nôi của bé'}
      </Text>
      <TouchableOpacity
        onPress={goQrScreen}
        style={{
          backgroundColor: ColorsDefault.blue1,
          borderRadius: 6,
          height: 52,
          justifyContent: 'center',
          alignItems: 'center',
          marginHorizontal: 16,
          marginTop: 32,
        }}>
        <Text
          style={{
            fontSize: FontSizeDefault.FONT_16,
            fontFamily: FontFamilyDefault.primaryMedium,
            color: ColorsDefault.white,
          }}>
          {'Quét mã QR'}
        </Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: ColorsDefault.white,
  },
});

export default AddCrib;
