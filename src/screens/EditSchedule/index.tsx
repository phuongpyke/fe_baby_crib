import {useNavigation} from '@react-navigation/native';
import React, {useCallback, useLayoutEffect, useState} from 'react';
import isEqual from 'react-fast-compare';
import {ScrollView} from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import {vs} from 'react-native-size-matters';
import reactotron from 'reactotron-react-native';
import {updateSchedule} from '/api/modules/schedule';
import {ColorsDefault, FontFamilyDefault, FontSizeDefault} from '/assets';
import images from '/assets/images';
import {FULL_WIDTH_DEVICE} from '/common';
import {Block, Image, Text, Touchable} from '/components';
import {SelectTimeSchedule} from '/components/common';
import {goBack} from '/navigations';
import {Routes} from '/navigations/Routes';
import {useAppSelector} from '/redux/hooks';
import {showToastError, showToastSuccess} from '/utils/helper';

const EditSchedule = ({route}: any) => {
  const {details, scheduleId} = route.params ?? {};
  const navigation: any = useNavigation();
  const {labels} = useAppSelector(
    ({schedule: {labels}}) => ({labels}),
    isEqual,
  );

  const [valueAction, setValueAction] = useState<any>(Number(details?.label));
  const [listActions, setListActions] = useState<any[]>(labels);
  const [openModalAction, setModalAction] = useState<boolean>(false);

  const onSubmit = useCallback(async () => {
    try {
      const res: any = await updateSchedule(
        {
          ...details,
          label: valueAction,
        },
        scheduleId,
      );
      if (res?.success) {
        showToastSuccess(res?.message);
        navigation.navigate(Routes.Timer);
      } else {
        showToastError(res?.message);
      }
    } catch (err) {
      reactotron.log!(err);
    }
  }, [valueAction, scheduleId, details]);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <Touchable p={8} middle center onPress={goBack}>
          <Text type="h4" fontWeight={'medium'} color={ColorsDefault.blue1}>
            {'Hủy'}
          </Text>
        </Touchable>
      ),
      headerRight: () => (
        <Touchable p={8} middle center onPress={onSubmit}>
          <Text type="h4" fontWeight={'bold'} color={ColorsDefault.blue1}>
            {'Lưu'}
          </Text>
        </Touchable>
      ),
    });
  }, [onSubmit]);

  return (
    <ScrollView
      style={{flex: 1, backgroundColor: ColorsDefault.white}}
      contentContainerStyle={{flex: 1, backgroundColor: ColorsDefault.white}}>
      <SelectTimeSchedule customStyle={{marginTop: vs(60)}} />
      <Block
        row
        middle
        justifyBetween
        pb={24}
        pt={36}
        mh={16}
        borderBottom
        borderColor={ColorsDefault.gray1}>
        <Text type="h4" fontWeight={'medium'}>
          Nhãn
        </Text>
        <DropDownPicker
          open={openModalAction}
          value={valueAction}
          items={listActions}
          setOpen={setModalAction}
          setValue={setValueAction}
          setItems={setListActions}
          listMode={'SCROLLVIEW'}
          zIndex={10}
          style={{
            borderColor: ColorsDefault.gray1,
            borderRadius: 6,
          }}
          dropDownContainerStyle={{
            borderColor: ColorsDefault.gray1,
            borderRadius: 6,
          }}
          placeholderStyle={{
            color: ColorsDefault.black3,
            fontFamily: FontFamilyDefault.primaryRegular,
            fontSize: FontSizeDefault.FONT_16,
          }}
          containerStyle={{
            width: FULL_WIDTH_DEVICE / 3,
          }}
          schema={{
            label: 'category_name',
            value: 'id',
            testID: 'id',
          }}
        />
      </Block>
      <Block mh={16} zIndex={-1}>
        <Text mt={36} fontWeight={'medium'} type="h4">
          Nội dung
        </Text>
        <Text
          style={{
            marginTop: 12,
            fontFamily: FontFamilyDefault.primaryRegular,
            fontSize: FontSizeDefault.FONT_16,
            color: ColorsDefault.black2,
            lineHeight: FontSizeDefault.FONT_22,
          }}>
          {details?.content}
        </Text>
      </Block>
      <Image
        style={{
          bottom: 0,
          right: 0,
          left: 0,
          zIndex: -10,
        }}
        resizeMode={'stretch'}
        center
        middle
        absolute
        source={images.footerRegister}
        width={FULL_WIDTH_DEVICE}
        height={FULL_WIDTH_DEVICE * 0.7}
      />
    </ScrollView>
  );
};

export default EditSchedule;
