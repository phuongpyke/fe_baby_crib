import {useNavigation} from '@react-navigation/native';
import React, {useCallback, useEffect, useLayoutEffect, useState} from 'react';
import {ScrollView, TouchableOpacity, Image, Linking} from 'react-native';
import {vs} from 'react-native-size-matters';
import {getAllLabels} from '/api/modules/news';
import {getScheduleDetail} from '/api/modules/schedule';
import {ColorsDefault, FontFamilyDefault, FontSizeDefault} from '/assets';
import images from '/assets/images';
import {BOTTOM_PADDING, FULL_WIDTH_DEVICE} from '/common';
import {Block, Text, Touchable} from '/components';
import {SelectTimeSchedule} from '/components/common';
import {Routes} from '/navigations/Routes';

const TimerDetail = ({route}: any) => {
  const {scheduleId, babyId} = route.params ?? {};

  const navigation: any = useNavigation();

  const [listNews, setListNews] = useState<any[]>([]);
  const [listLabels, setListLabels] = useState<any[]>([]);
  const [scheduleDetail, setScheduleDetail] = useState<any>({});

  const goEditSchedule = () =>
    navigation.navigate(Routes.EditSchedule, {
      details: scheduleDetail,
      scheduleId,
    });

  const openLink = async (url: string) => {
    const supported = await Linking.canOpenURL(url);
    if (supported) {
      await Linking.openURL(url);
    }
  };

  useEffect(() => {
    const getData = async () => {
      try {
        const res: any = await Promise.all([
          getAllLabels(),
          getScheduleDetail({babyId, scheduleId}),
        ]);
        if (res[0]?.success) {
          setListLabels(res[0]?.data);
        }
        if (res[1]?.success) {
          setScheduleDetail(res[1]?.data?.schedule);
          setListNews(res[1]?.data?.blogs);
        }
      } catch (err) {
      } finally {
      }
    };
    getData();
  }, []);

  const goBack = () => navigation.goBack();

  const getLabel = useCallback(() => {
    if (listLabels?.length && scheduleDetail?.label) {
      const findItem = listLabels.find(
        item => item.id == scheduleDetail?.label,
      );
      return findItem?.category_name;
    }
    return null;
  }, [scheduleDetail, listLabels]);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <Touchable p={8} middle center onPress={goBack}>
          <Image source={images.backLeft} style={{height: 20, width: 20}} />
        </Touchable>
      ),
      headerRight: () => (
        <Touchable p={8} middle center onPress={goEditSchedule}>
          <Text type="h4" fontWeight={'bold'} color={ColorsDefault.blue1}>
            {'Sửa'}
          </Text>
        </Touchable>
      ),
    });
  }, [scheduleDetail]);

  return (
    <ScrollView
      showsVerticalScrollIndicator={false}
      style={{flex: 1, backgroundColor: ColorsDefault.white}}
      contentContainerStyle={{
        flexGrow: 1,
        backgroundColor: ColorsDefault.white,
        paddingBottom: BOTTOM_PADDING,
      }}>
      <SelectTimeSchedule customStyle={{marginTop: vs(60)}} />
      <Block
        row
        middle
        justifyBetween
        pb={24}
        pt={36}
        mh={16}
        borderBottom
        borderColor={ColorsDefault.gray1}>
        <Text type="h4" fontWeight={'medium'}>
          Nhãn
        </Text>
        <Text type="h4">{getLabel()}</Text>
      </Block>
      <Block mh={16}>
        <Text mt={36} fontWeight={'medium'} type="h4">
          Nội dung
        </Text>
        <Text
          style={{
            marginTop: 12,
            fontFamily: FontFamilyDefault.primaryRegular,
            fontSize: FontSizeDefault.FONT_16,
            color: ColorsDefault.black2,
            lineHeight: FontSizeDefault.FONT_22,
          }}>
          {scheduleDetail?.content}
        </Text>
      </Block>
      <Text mt={24} mh={16} fontWeight={'medium'} type="h4">
        Bài viết liên quan
      </Text>
      {listNews.map(item => (
        <TouchableOpacity
          onPress={() => openLink(item?.link)}
          activeOpacity={0.6}
          key={item.id}
          style={{marginHorizontal: 16, marginTop: 16}}>
          {!!item.banner && (
            <Image
              source={item.banner}
              style={{
                width: '100%',
                height: FULL_WIDTH_DEVICE * 0.5,
                borderRadius: 12,
                backgroundColor: ColorsDefault.blue4,
              }}
            />
          )}
          <Text mt={8} fontWeight={'medium'} type="h4">
            {item.title}
          </Text>
        </TouchableOpacity>
      ))}
    </ScrollView>
  );
};

export default TimerDetail;
