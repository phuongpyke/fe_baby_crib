import {useNavigation} from '@react-navigation/native';
import moment from 'moment';
import React, {useEffect, useRef, useState} from 'react';
import {ScrollView} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {ColorsDefault, FontSizeDefault} from '/assets';
import images from '/assets/images';
import {WIDTH_DEVICE} from '/common';
import {Block, Image, Text, Touchable} from '/components';
import {ModalAmountMilk, ModalConfirm, WatchVideo} from '/components/common';
import {babyListSelector} from '/modules/user/selectors';
import {getBaByList} from '/modules/user/slice';
import {Routes} from '/navigations/Routes';
import {ELabel} from '/utils/enum';

const data = [
  {
    id: 1,
    label: 'Dậy & Ăn',
    time: '06:00 -07:00',
    type: 1,
    image: images.babyCare1,
  },
  {
    id: 2,
    label: 'Hoạt động',
    time: '06:00 -07:00',
    type: 2,
    image: images.babyCare2,
  },
  {
    id: 3,
    label: 'Bé ngủ',
    time: '09:30 -10:00',
    type: 3,
    image: images.babyCare3,
  },
  {
    id: 4,
    label: 'Bé tắm',
    time: '10:30 -12:00',
    type: 3,
    image: images.babyCare4,
  },
  {
    id: 5,
    label: 'Bé thay tã',
    time: '09:30 -10:00',
    type: 3,
    image: images.babyCare5,
  },
  {
    id: 6,
    label: 'Bé ăn dặm',
    time: '10:30 -12:00',
    type: 3,
    image: images.babyCare6,
  },
];

const videoData = [
  {
    title: 'Tắm cho bé như thế nào là đúng?',
    id: '0ihlsYD5AZA',
  },
  {
    title: 'Môi trường sống ảnh hưởng đến sự phát triển của bé như thế nào?',
    id: '-vvgeNbL2x0',
  },
  {
    title: 'Những trò chơi kích thích sự phát triển của bé',
    id: 'IvUu87UhnUI',
  },
];

const Dashboard = () => {
  const navigation: any = useNavigation();
  const listBaby = useSelector(babyListSelector);
  const userInfo = listBaby[0];
  const start = moment(userInfo?.dob, 'YYYY-MM-DD');
  const end = moment(new Date(), 'YYYY-MM-DD');

  const [listSchedule, setListSchedule] = useState<any[]>(data);

  const dispatch = useDispatch();

  const amountMilkRef = useRef<any>(null);
  const diapersRef = useRef<any>(null);
  const colorRef = useRef<any>(null);

  const goSchedule = () => {
    navigation.navigate(Routes.Schedule);
  };

  const goVaccinSchedule = () => {
    navigation.navigate(Routes.VaccinSchedule);
  };

  const goPatients = () => {
    navigation.navigate(Routes.Patients);
  };

  const goDiapers = () => {
    navigation.navigate(Routes.Diapers);
  };

  const goMilks = () => {
    navigation.navigate(Routes.Milks);
  };

  const goBabyCare = () => {
    navigation.navigate(Routes.BabyCare);
  };

  const goBabySpending = () => {
    navigation.navigate(Routes.BabySpending);
  };

  const goDiagnosisByAI = () => {
    navigation.navigate(Routes.DiagnosisByAI);
  };

  const goStoreEasy = () => {
    navigation.navigate(Routes.StoreEasy);
  };

  const onSelectItem = (item: any) => {
    const list = listSchedule.map((value: any) =>
      value.id === item.id ? {...value, isSelected: !value?.isSelected} : value,
    );
    setListSchedule(list);
    if (item.type == ELabel.TYPE_1) {
      return amountMilkRef.current?.show();
    } else if (item.type == ELabel.TYPE_2) {
      diapersRef.current?.show();
    }
    return;
  };

  useEffect(() => {
    const payload = {
      onSuccess: (response: any) => {
        if (response.data?.length === 0) {
          navigation.navigate(Routes.EditProfile, {
            mode: 'new',
          });
        }
      },
    };
    dispatch(getBaByList(payload));
  }, []);

  const renderHeader = () => (
    <Image
      top={0}
      source={images.headerDashBoard}
      width={WIDTH_DEVICE}
      resizeMode={'cover'}
      height={285}>
      <Block mt={67} row>
        <Block
          ml={16}
          width={80}
          height={80}
          borderWidth={2}
          borderColor={ColorsDefault.gray}
          middle
          center
          borderRadius={40}
          bg={ColorsDefault.white}>
          <Image
            source={images.babyBoy1}
            style={{zIndex: 11111}}
            width={50}
            height={50}></Image>
        </Block>
        <Block ml={16} mt={12}>
          <Text type="h4" color={ColorsDefault.white}>
            Chào mẹ của bé
          </Text>
          <Text mt={12} color={ColorsDefault.white} type="h3" fontWeight="bold">
            {userInfo?.name}
          </Text>
        </Block>
      </Block>
      <Block
        style={{position: 'absolute', bottom: 0, left: 16, right: 16}}
        mt={12}
        row
        justifyBetween>
        <Image
          width={(WIDTH_DEVICE - 32) / 3}
          height={(WIDTH_DEVICE - 32) / 3}
          resizeMode="stretch"
          source={images.imageDate}
          middle>
          <Text fontWeight="medium" type="h4" mt={24}>
            Tuổi
          </Text>
          <Text
            mt={8}
            type="h3"
            color={ColorsDefault.blue1}
            fontWeight={'bold'}>
            {end.diff(start, 'days')} Ngày
          </Text>
        </Image>
        <Touchable onPress={goStoreEasy}>
          <Image
            width={(WIDTH_DEVICE - 32) / 3}
            height={(WIDTH_DEVICE - 32) / 3}
            source={images.imageStage}
            middle>
            <Text fontWeight="medium" mt={24} type="h4">
              Giai đoạn
            </Text>
            <Text
              mt={8}
              type="h3"
              color={ColorsDefault.blue1}
              fontWeight="bold">
              {`Easy ${userInfo?.breast_milk}`}
            </Text>
          </Image>
        </Touchable>
        <Image
          width={(WIDTH_DEVICE - 32) / 3}
          height={(WIDTH_DEVICE - 32) / 3}
          source={images.imageName}
          middle>
          <Text mt={24} fontWeight="medium" type="h4">
            Giới tính
          </Text>
          <Text mt={8} type="h3" color={ColorsDefault.blue1} fontWeight="bold">
            {userInfo?.gender === 1 ? 'Nam' : 'Nữ'}
          </Text>
        </Image>
      </Block>
    </Image>
  );

  const renderTakeCareBaby = () => {
    return (
      <Block
        mt={32}
        shadow
        bg={ColorsDefault.white}
        mh={16}
        borderRadius={8}
        center
        middle>
        <Image
          source={images.headerTakeCare}
          width={268}
          height={108}
          middle
          pt={16}>
          <Text type="h3" color={ColorsDefault.orange2} fontWeight="bold">
            Chăm sóc bé
          </Text>
          <Block
            width={140}
            height={0}
            borderWidth={1}
            borderColor={ColorsDefault.orange2}
            borderStyle="dashed"></Block>
        </Image>
        <Block row justifyBetween width={'100%'} ph={16} pb={16}>
          <Touchable flex={1} middle onPress={goBabyCare}>
            <Image
              width={56}
              height={56}
              source={images.dashBoard1}
              middle></Image>
            <Text textCenter mt={8} fontWeight="medium">
              Chăm sóc toàn diện bé
            </Text>
          </Touchable>
          <Touchable flex={1} middle onPress={goDiapers}>
            <Image
              width={56}
              height={56}
              borderRadius={8}
              bg={'#73D2E2'}
              source={images.dashBoard3}
              middle></Image>
            <Text textCenter mt={8} fontWeight="medium">
              Bỉm
            </Text>
          </Touchable>
          <Touchable flex={1} middle onPress={goMilks}>
            <Image
              width={56}
              height={56}
              source={images.dashBoard2}
              middle></Image>
            <Text textCenter mt={8} fontWeight="medium">
              Sữa
            </Text>
          </Touchable>
          <Touchable flex={1} middle onPress={goBabySpending}>
            <Image
              width={56}
              height={56}
              source={images.dashBoard7}
              middle></Image>
            <Text textCenter mt={8} fontWeight="medium">
              Chi tiêu
            </Text>
          </Touchable>
        </Block>
      </Block>
    );
  };

  const renderMedical = () => {
    return (
      <Block
        mt={32}
        shadow
        bg={ColorsDefault.white}
        mh={16}
        borderRadius={8}
        center
        middle>
        <Image
          source={images.headerTakeCare}
          width={268}
          height={108}
          middle
          pt={16}>
          <Text type="h3" color={ColorsDefault.orange2} fontWeight="bold">
            Y tế
          </Text>
          <Block
            width={140}
            height={0}
            borderWidth={1}
            borderColor={ColorsDefault.orange2}
            borderStyle="dashed"></Block>
        </Image>
        <Block row justifyBetween width={'100%'} ph={16} pb={16}>
          <Touchable width={96} onPress={goPatients}>
            <Image
              width={96}
              height={96}
              borderRadius={12}
              source={images.dashBoard6}
              middle></Image>
            <Text textCenter mt={8} fontWeight="medium">
              Bệnh án
            </Text>
          </Touchable>
          <Touchable width={96} onPress={goVaccinSchedule}>
            <Image
              width={96}
              height={96}
              borderRadius={12}
              source={images.dashBoard4}
              middle></Image>
            <Text textCenter mt={8} fontWeight="medium">
              Tiêm chủng
            </Text>
          </Touchable>
          <Touchable width={96} onPress={goDiagnosisByAI}>
            <Image
              width={96}
              height={96}
              borderRadius={12}
              source={images.dashBoard5}
              middle></Image>
            <Text textCenter mt={8} fontWeight="medium">
              Đoán bệnh
            </Text>
          </Touchable>
        </Block>
      </Block>
    );
  };

  return (
    <ScrollView
      style={{flex: 1, backgroundColor: ColorsDefault.white}}
      contentContainerStyle={{paddingBottom: 20}}
      showsVerticalScrollIndicator={false}>
      {renderHeader()}
      <Block mh={16} mt={8}>
        <Image source={images.poster} width={'100%'} height={44} middle center>
          <Text color={ColorsDefault.white} fontWeight={'medium'} type="h4">
            Bé phát triển bình thường{' '}
          </Text>
        </Image>
      </Block>
      {renderTakeCareBaby()}
      <Block bg={ColorsDefault.gray1} mt={24}>
        <Text
          mt={16}
          textCenter
          color={ColorsDefault.orange2}
          type="h3"
          fontWeight="bold">
          Lịch trình chăm bé
        </Text>
        <Block
          alignSelfCenter
          width={140}
          height={0}
          borderWidth={1}
          borderColor={ColorsDefault.orange2}
          borderStyle="dashed"></Block>
        <ScrollView
          horizontal
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={{
            paddingVertical: 16,
            paddingRight: 16,
          }}>
          {listSchedule.map((item, index) => (
            <Touchable key={index} ml={12} onPress={() => onSelectItem(item)}>
              <Image
                source={item.image}
                width={96}
                height={96}
                bg={ColorsDefault.purple1}
                middle
                borderRadius={12}
                justifyEnd>
                <Block
                  bg={
                    item?.isSelected ? ColorsDefault.blue1 : ColorsDefault.white
                  }
                  width={20}
                  height={20}
                  borderRadius={3}
                  borderWidth={1}
                  borderColor={ColorsDefault.blue1}
                  middle
                  center
                  absolute
                  top={8}
                  right={8}
                  zIndex={10}>
                  <Image source={images.check} width={12} height={8} />
                </Block>
                <Text
                  mb={-2}
                  color={ColorsDefault.black3}
                  fontSize={FontSizeDefault.FONT_14}
                  lineHeight={FontSizeDefault.FONT_22}
                  fontWeight="medium">
                  {item?.label}
                </Text>
              </Image>
              <Block
                bg={ColorsDefault.white}
                borderRadius={6}
                width={80}
                height={20}
                mt={10}
                alignSelfCenter
                middle
                center>
                <Text color={ColorsDefault.black3} type="c1">
                  {item?.time}
                </Text>
              </Block>
            </Touchable>
          ))}
        </ScrollView>

        <Touchable
          onPress={goSchedule}
          center
          width={150}
          height={40}
          borderRadius={20}
          middle
          mb={16}
          bg={ColorsDefault.blue1}
          alignSelfCenter>
          <Text fontWeight="medium" type="h4" color={ColorsDefault.white}>
            Xem tất cả
          </Text>
        </Touchable>
      </Block>
      {renderMedical()}
      <Text
        mt={16}
        textCenter
        type="h3"
        color={ColorsDefault.orange2}
        fontWeight="bold">
        Video chăm bé
      </Text>
      <Block
        alignSelfCenter
        width={140}
        height={0}
        borderWidth={1}
        borderColor={ColorsDefault.orange2}
        borderStyle="dashed"></Block>
      {videoData.map((item, index) => (
        <WatchVideo key={index} videoId={item.id} title={item.title} />
      ))}
      <ModalAmountMilk ref={amountMilkRef} />
      <ModalConfirm
        ref={diapersRef}
        title={'Bé có đi nặng không?'}
        textConfirm={'Có'}
        textCancel={'Không'}
        onSubmit={() => {
          setTimeout(() => {
            colorRef.current?.show();
          }, 500);
        }}
      />
      <ModalConfirm
        ref={colorRef}
        title={'Phân của bé có đổi màu không?'}
        textConfirm={'Có'}
        textCancel={'Không'}
      />
    </ScrollView>
  );
};

export default Dashboard;
