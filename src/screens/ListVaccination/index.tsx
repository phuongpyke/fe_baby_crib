import {useNavigation} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import isEqual from 'react-fast-compare';
import {FlatList, Image, Text, TouchableOpacity, View} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {s, ScaledSheet} from 'react-native-size-matters';
import reactotron from 'reactotron-react-native';
import {getAllVaccines} from '/api/modules/vaccine';
import {ColorsDefault, FontFamilyDefault, FontSizeDefault} from '/assets';
import images from '/assets/images';
import {BOTTOM_PADDING, FULL_WIDTH_DEVICE} from '/common';
import {Header} from '/components/common';
import {Routes} from '/navigations/Routes';
import {useAppSelector} from '/redux/hooks';
import {EStatusInject} from '/utils/enum';

const schedules2 = [
  {
    id: 0,
    name: 'Viêm gan B (HBV)',
    total_vaccination: 5,
    num_vaccination: 1,
  },
  {
    id: 1,
    name: 'Lao',
    total_vaccination: 3,
    num_vaccination: 0,
  },
  {
    id: 2,
    name: 'Rota',
    total_vaccination: 4,
    num_vaccination: 1,
  },
  {
    id: 3,
    name: '5in1 hoặc 6in1',
    total_vaccination: 3,
    num_vaccination: 2,
  },
  {
    id: 4,
    name: 'Phế cầu',
    total_vaccination: 5,
    num_vaccination: 1,
  },
  {
    id: 5,
    name: 'Cúm mùa',
    total_vaccination: 2,
    num_vaccination: 0,
  },
  {
    id: 6,
    name: 'Viêm màng nào BC',
    total_vaccination: 1,
    num_vaccination: 0,
  },
  {
    id: 7,
    name: 'Sởi-Quai bị-Rubella',
    total_vaccination: 5,
    num_vaccination: 0,
  },
  {
    id: 8,
    name: 'Viêm não Nhật Bản',
    total_vaccination: 3,
    num_vaccination: 2,
  },
  {
    id: 9,
    name: 'Thuỷ đậu',
    total_vaccination: 2,
    num_vaccination: 0,
  },
  {
    id: 10,
    name: 'Viêm gan A',
    total_vaccination: 1,
    num_vaccination: 0,
  },
  {
    id: 11,
    name: 'Viêm màng não AC',
    total_vaccination: 1,
    num_vaccination: 1,
  },
  {
    id: 12,
    name: 'Thương hàn',
    total_vaccination: 1,
    num_vaccination: 0,
  },
  {
    id: 13,
    name: 'Ung thư cổ tử cung, u nhú do HPV',
    total_vaccination: 1,
    num_vaccination: 1,
  },
];

const ListVaccination = () => {
  const navigation: any = useNavigation();
  const {babyList} = useAppSelector(
    ({user: {babyList}}) => ({babyList}),
    isEqual,
  );
  const [schedules, setSchedules] = useState<any[]>([]);

  const goUpdateVaccinSchedule = () =>
    navigation.navigate(Routes.UpdateVaccinSchedule);

  const goVaccinDetail = (data: any, check: boolean) =>
    navigation.navigate(Routes.VaccinDetail, {
      id: data.id,
      type: check ? EStatusInject.INJECTED : EStatusInject.NOT_INJECTED,
      headerTitle: data.title,
      baby: babyList?.[0],
    });

  useEffect(() => {
    const getData = async () => {
      try {
        const res: any = await getAllVaccines(babyList?.[0]?.id);
        if (res?.success) {
          setSchedules(res.data);
        }
      } catch (err) {
        reactotron.warn!(err);
      }
    };
    getData();
  }, []);

  const renderItem = ({item}: any) => (
    <View style={styles.wrapItem}>
      <View style={styles.viewLeftItem}>
        <Text style={styles.textName}>{item?.title}</Text>
      </View>
      <View style={styles.viewRightItem}>
        {Array(item?.number)
          .fill(null)
          .map((_, i) => (
            <TouchableOpacity
              onPress={() => goVaccinDetail(item, item?.number_injected > i)}
              key={i}
              style={[
                styles.btnTimes,
                item?.number_injected > i && styles.btnTimesSelected,
              ]}>
              {item?.number_injected > i ? (
                <Image source={images.check} style={{height: 24, width: 24}} />
              ) : (
                <Text style={styles.textTimes}>{i + 1}</Text>
              )}
            </TouchableOpacity>
          ))}
      </View>
    </View>
  );

  const renderAction = () => (
    <Image source={images.calendar3} style={{height: 20, width: 20}} />
  );

  return (
    <SafeAreaView style={styles.container} edges={['right', 'left', 'top']}>
      <Header
        title="Tiêm chủng"
        customStyle={styles.viewHeader}
        renderAction={renderAction}
        onPressAction={goUpdateVaccinSchedule}
        actionStyle={styles.btnHeader}
      />
      <Image
        source={images.headerVaccin}
        style={styles.bgHeader}
        resizeMode={'stretch'}
      />
      <FlatList
        contentContainerStyle={styles.contentScroll}
        showsVerticalScrollIndicator={false}
        data={schedules}
        renderItem={renderItem}
        keyExtractor={(item: any) => item.id}
      />
    </SafeAreaView>
  );
};

const styles = ScaledSheet.create({
  container: {
    backgroundColor: ColorsDefault.white,
    flex: 1,
  },
  contentScroll: {
    paddingBottom: BOTTOM_PADDING,
  },
  viewHeader: {
    borderBottomWidth: 1,
    borderBottomColor: ColorsDefault.white,
  },
  bgHeader: {width: FULL_WIDTH_DEVICE, position: 'absolute', zIndex: -100},
  btnHeader: {
    height: '36@s',
    width: '36@s',
    borderRadius: '36@s',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: ColorsDefault.gray1,
    borderWidth: 1,
    backgroundColor: ColorsDefault.white,
  },
  wrapItem: {
    width: FULL_WIDTH_DEVICE - s(32),
    backgroundColor: ColorsDefault.transparent,
    alignSelf: 'center',
    marginTop: '16@vs',
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: ColorsDefault.gray1,
    paddingBottom: '8@vs',
  },
  viewLeftItem: {
    width: '80@s',
  },
  textName: {
    fontSize: FontSizeDefault.FONT_16,
    fontFamily: FontFamilyDefault.primaryMedium,
    color: ColorsDefault.black3,
    lineHeight: FontSizeDefault.FONT_24,
  },
  viewRightItem: {
    flex: 1,
    marginLeft: '12@s',
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  btnTimes: {
    backgroundColor: ColorsDefault.gray1,
    height: '36@s',
    width: '36@s',
    borderRadius: '36@s',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: '8@s',
  },
  btnTimesSelected: {
    backgroundColor: ColorsDefault.green2,
  },
  textTimes: {
    fontSize: FontSizeDefault.FONT_16,
    fontFamily: FontFamilyDefault.primaryMedium,
    color: ColorsDefault.gray3,
  },
  textSubTitle: {
    fontSize: FontSizeDefault.FONT_14,
    fontFamily: FontFamilyDefault.primaryRegular,
    color: ColorsDefault.black3,
  },
  textSchedule: {
    fontSize: FontSizeDefault.FONT_14,
    fontFamily: FontFamilyDefault.primaryRegular,
    color: ColorsDefault.black3,
    marginLeft: '5@s',
  },
});

export default ListVaccination;
